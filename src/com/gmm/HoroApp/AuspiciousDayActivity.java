package com.gmm.HoroApp;

import com.gmm.HoroApp.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.gesture.GestureOverlayView;
import android.gesture.GestureOverlayView.OnGestureListener;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Display;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnTouchListener;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

public class AuspiciousDayActivity extends Activity implements Runnable,OnGestureListener{
	
	
	private ProgressDialog pd;

	TextView mText_header;
	WebView mWebView;
	
	String URL_webview; 
	//String Text_header;
	
	boolean fin;
	private GestureDetector gestureScanner;
	private long lastTouchTime = -1;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
		
		System.out.println("--- WebViewAcitivity");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.auspiciousday);
        
        fin=false;
        mWebView = (WebView)findViewById(R.id.webviewaus);
        mWebView.setOnTouchListener(new OnTouchListener() {
			
			public boolean onTouch(View v, MotionEvent event) {
				System.out.println("============ click ==========");
				   if (event.getAction() == MotionEvent.ACTION_DOWN) {

				      long thisTime = System.currentTimeMillis();
				      if (thisTime - lastTouchTime < 250) {

				         // Double click
				    	 Log.log("========== doubleclick ======");
				         lastTouchTime = -1;

				      } else {
				         // too slow
				    	  Log.log("========= singleclick ======");
				         lastTouchTime = thisTime;
				      }
				   }
				
				return false;
			}
		});
        
        //if(checkInternet()){
			
			pd = ProgressDialog.show( getParent() , "Loading..", "Please wait..",true,false);
	        
	        Thread thread = new Thread(this);
	        thread.start();
	        
	        /*
		}else{
			
			//alert 
    		String title = getString(R.string.title_internet_false);
            String message = getString(R.string.msg_internet_false);
            showNoticeDialogBox(title, message);
		}
        */
        
        
	}
	
	
	public void run() {
    	try {
    		
    		System.out.println("-run -- WebViewAcitivity");
    		
    		
    		mWebView.getSettings().setJavaScriptEnabled(true);
    		
    		//====== set disible zoom
    		mWebView.getSettings().setSupportZoom(false);
    		mWebView.getSettings().setBuiltInZoomControls(false);
    		
    		mWebView.getSettings().setLoadWithOverviewMode(true);
    		mWebView.getSettings().setUseWideViewPort(true);
    		mWebView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
    		mWebView.setPadding(0, 0, 0, 0);
    		mWebView.setInitialScale(getScale());
    		
//    		System.out.println("getScale() = "+getScale());
	        
	        mWebView.clearCache(true);
	        mWebView.clearView();
	        mWebView.clearHistory();
	        mWebView.clearFormData();
	        
//	        mWebView.loadUrl(URL_webview);
	        mWebView.setBackgroundColor(Color.BLACK);
	        mWebView.loadUrl("file:///android_asset/WVAuspicious/auspicious_day.html");
	        fin=true;
    		
    		
		} catch (Exception e) {
			e.printStackTrace();
		}
        
        handler.sendEmptyMessage(0);
    }
	
	private int getScale(){
	    Display display = ((WindowManager) getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay(); 
	    int width = display.getWidth(); 
	    System.out.println("width = "+width);
	    Double val = new Double(width)/new Double(/*640*/1280);
	    val = val * 100d;
	    System.out.println("val.intValue() = "+val.intValue());
	    return val.intValue();
	}
	
	private Handler handler = new Handler() {
	    @Override
	    public void handleMessage(Message msg) {
	    	try {
	    		
	            pd.dismiss();
	            
	    	}
	    	catch (Exception e) {
				e.printStackTrace();
			}
	    }
	};
	
	
	protected void onResume() {
		
		super.onResume();
		
		System.out.println("---------- onResume() ----webview------");
		
//		if(fin){
//			finish();
//		}
		
	}
	
	
	private class HelloWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }
    
    
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK) && mWebView.canGoBack()) {
        	
        	
        	System.out.println("======== onKeyDown");
        	
//            mWebView.goBack();
        	Thread thread = new Thread(this);
	        thread.start();
            
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
    
    
    private void showNoticeDialogBox(final String title, final String message) {
    	
    	System.out.println("---- showNoticeDialogBox");
        Builder setupAlert;
        setupAlert = new AlertDialog.Builder(AuspiciousDayActivity.this);    
        setupAlert.setTitle(title);
        setupAlert.setMessage(message);
        setupAlert.setPositiveButton("Ok",
            new DialogInterface.OnClickListener() {
              public void onClick(DialogInterface dialog, int whichButton) {
            	  
              }
            });
        setupAlert.show();
      }

    
    private boolean checkInternet() {
		
    	ConnectivityManager connec =  ( ConnectivityManager ) getSystemService( Context.CONNECTIVITY_SERVICE ); 
    	
    	if ( connec.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTED || connec.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTED )
    	{
    	     // connect 
    		return true;
    	}
    	else
    	{
    	     // not-connect 
    		return false;
    	}
    }

	public void onGesture(GestureOverlayView overlay, MotionEvent event) {
		// TODO Auto-generated method stub
		
	}

	public void onGestureCancelled(GestureOverlayView overlay, MotionEvent event) {
		// TODO Auto-generated method stub
		
	}

	public void onGestureEnded(GestureOverlayView overlay, MotionEvent event) {
		// TODO Auto-generated method stub
		
	}

	public void onGestureStarted(GestureOverlayView overlay, MotionEvent event) {
		// TODO Auto-generated method stub
		
	}

}
