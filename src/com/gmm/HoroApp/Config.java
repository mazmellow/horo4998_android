package com.gmm.HoroApp;

import android.graphics.Bitmap;

public class Config {
	
	
	public static String COL_bannerimg="bannerimg";
	public static String COL_homepageimg="homepageimg";
	public static String COL_howtoregisterimg="howtoregisterimg";
	public static String COL_howtouseimg="howtouseimg";
	public static String COL_profiletext="profiletext";
	
	
	private Bitmap bannerimg;
	private Bitmap homepageimg;
	private Bitmap howtoregisterimg;
	private Bitmap howtouseimg;
	private Bitmap profiletext;
	
	private String url_bannerimg;
	private String url_homepageimg;
	private String url_howtoregisterimg;
	private String url_howtouseimg;
	private String url_profiletext;
	
	
	public Bitmap getBannerImg() {
		return(bannerimg);
	}
	
	public void setBannerImg(Bitmap bannerimg) {
		this.bannerimg=bannerimg;
	}
	
	
	public Bitmap getHomePageImg() {
		return(homepageimg);
	}
	
	public void setHomePageImg(Bitmap homepageimg) {
		this.homepageimg=homepageimg;
	}

	
	public Bitmap getHowToRegisterImg() {
		return(howtoregisterimg);
	}
	
	public void setHowToRegisterImg(Bitmap howtoregisterimg) {
		this.howtoregisterimg=howtoregisterimg;
	}
	
	
	public Bitmap getHowToUseImg() {
		return(howtouseimg);
	}
	
	public void setHowToUseImg(Bitmap howtouseimg) {
		this.howtouseimg=howtouseimg;
	}
	
	
	public Bitmap getProfileText() {
		return(profiletext);
	}
	
	public void setProfileText(Bitmap profiletext) {
		this.profiletext=profiletext;
	}
	
	
	
	
	public String getBannerImg_URL() {
		return(url_bannerimg);
	}
	
	public void setBannerImg_URL(String url_bannerimg) {
		this.url_bannerimg=url_bannerimg;
	}
	
	
	public String getHomePageImg_URL() {
		return(url_homepageimg);
	}
	
	public void setHomePageImg_URL(String url_homepageimg) {
		this.url_homepageimg=url_homepageimg;
	}

	
	public String getHowToRegisterImg_URL() {
		return(url_howtoregisterimg);
	}
	
	public void setHowToRegister_URL(String url_howtoregisterimg) {
		this.url_howtoregisterimg=url_howtoregisterimg;
	}
	
	
	public String getHowToUseImg_URL() {
		return(url_howtouseimg);
	}
	
	public void setHowToUseImg_URL(String url_howtouseimg) {
		this.url_howtouseimg=url_howtouseimg;
	}
	
	
	public String getProfileText_URL() {
		return(url_profiletext);
	}
	
	public void setProfileText_URL(String url_profiletext) {
		this.url_profiletext=url_profiletext;
	}
	
	
}
