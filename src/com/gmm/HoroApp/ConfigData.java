package com.gmm.HoroApp;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;

public class ConfigData {
	
private Context mContext;
	
	public ConfigData(Context context){
		mContext = context;
	}


	public void insert(final Config config) {
		
		System.out.println(" ConfigData -- insert");
		
		ContentValues values = new ContentValues();	
		
		try{
			
			if(config.getBannerImg_URL().trim().length()>0){
				DefaultHttpClient mHttpClient = new DefaultHttpClient();
		    	HttpGet mHttpGet = new HttpGet( config.getBannerImg_URL() );		//Link
		    	HttpResponse mHttpResponse = mHttpClient.execute(mHttpGet);
		    	if (mHttpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
		    	  HttpEntity entity = mHttpResponse.getEntity();
		    	    if ( entity != null) {
		    	      values.put(Config.COL_bannerimg, EntityUtils.toByteArray(entity));
		    	    }
		    	}else{
		    		values.put(Config.COL_bannerimg, "");
		    	}
			}else{
				values.put(Config.COL_bannerimg, "");
			}
		}catch(Exception e) {
			System.out.println("error contect save COL_bannerimg in AlbumData");
			values.put(Config.COL_bannerimg, "");
		} 
		
		try{
			
			if(config.getHomePageImg_URL().trim().length()>0){
				DefaultHttpClient mHttpClient = new DefaultHttpClient();
		    	HttpGet mHttpGet = new HttpGet( config.getHomePageImg_URL() );		//Link
		    	HttpResponse mHttpResponse = mHttpClient.execute(mHttpGet);
		    	if (mHttpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
		    	  HttpEntity entity = mHttpResponse.getEntity();
		    	    if ( entity != null) {
		    	      values.put(Config.COL_homepageimg, EntityUtils.toByteArray(entity));
		    	    }
		    	}else{
		    		values.put(Config.COL_homepageimg, "");
		    	}
			}else{
				values.put(Config.COL_homepageimg, "");
			}
		}catch(Exception e) {
			System.out.println("error contect save COL_homepageimg in AlbumData");
			values.put(Config.COL_homepageimg, "");
		}
		
		try{
			
			if(config.getHowToRegisterImg_URL().trim().length()>0){
				DefaultHttpClient mHttpClient = new DefaultHttpClient();
		    	HttpGet mHttpGet = new HttpGet( config.getHowToRegisterImg_URL() );		//Link
		    	HttpResponse mHttpResponse = mHttpClient.execute(mHttpGet);
		    	if (mHttpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
		    	  HttpEntity entity = mHttpResponse.getEntity();
		    	    if ( entity != null) {
		    	      values.put(Config.COL_howtoregisterimg, EntityUtils.toByteArray(entity));
		    	    }
		    	}else{
		    		values.put(Config.COL_howtoregisterimg, "");
		    	}
			}else{
				values.put(Config.COL_howtoregisterimg, "");
			}
		}catch(Exception e) {
			System.out.println("error contect save COL_howtoregisterimg in AlbumData");
			values.put(Config.COL_howtoregisterimg, "");
		}
		
		try{
			
			if(config.getHowToUseImg_URL().trim().length()>0){
				DefaultHttpClient mHttpClient = new DefaultHttpClient();
		    	HttpGet mHttpGet = new HttpGet( config.getHowToUseImg_URL() );		//Link
		    	HttpResponse mHttpResponse = mHttpClient.execute(mHttpGet);
		    	if (mHttpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
		    	  HttpEntity entity = mHttpResponse.getEntity();
		    	    if ( entity != null) {
		    	      values.put(Config.COL_howtouseimg, EntityUtils.toByteArray(entity));
		    	    }
		    	}else{
		    		values.put(Config.COL_howtouseimg, "");
		    	}
			}else{
				values.put(Config.COL_howtouseimg, "");
			}
		}catch(Exception e) {
			System.out.println("error contect save COL_howtouseimg in AlbumData");
			values.put(Config.COL_howtouseimg, "");
		}
		
		
		
		try{
			
			if(config.getProfileText_URL().trim().length()>0){
				DefaultHttpClient mHttpClient = new DefaultHttpClient();
		    	HttpGet mHttpGet = new HttpGet( config.getProfileText_URL() );		//Link
		    	HttpResponse mHttpResponse = mHttpClient.execute(mHttpGet);
		    	if (mHttpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
		    	  HttpEntity entity = mHttpResponse.getEntity();
		    	    if ( entity != null) {
		    	      values.put(Config.COL_profiletext, EntityUtils.toByteArray(entity));
		    	    }
		    	}else{
		    		values.put(Config.COL_profiletext, "");
		    	}
			}else{
				values.put(Config.COL_profiletext, "");
			}
		}catch(Exception e) {
			System.out.println("error contect save COL_profiletext in AlbumData");
			values.put(Config.COL_profiletext, "");
		}
		
		
		
    	mContext.getContentResolver().insert(UsersConfigDB.AuthenticationInfo.CONTENT_URI, values);
    	
	}
	
	
	public void update(final Config config, String selection, final String selectionArgs) {
		
		System.out.println(" ConfigData -- update");
		
		//String selection = Content.COL_id + "=?";
		ContentValues values = new ContentValues();	
		
		try{
			
			if(config.getBannerImg_URL().trim().length()>0){
				DefaultHttpClient mHttpClient = new DefaultHttpClient();
		    	HttpGet mHttpGet = new HttpGet( config.getBannerImg_URL() );		//Link
		    	HttpResponse mHttpResponse = mHttpClient.execute(mHttpGet);
		    	if (mHttpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
		    	  HttpEntity entity = mHttpResponse.getEntity();
		    	    if ( entity != null) {
		    	      values.put(Config.COL_bannerimg, EntityUtils.toByteArray(entity));
		    	    }
		    	}else{
		    		values.put(Config.COL_bannerimg, "");
		    	}
			}else{
				values.put(Config.COL_bannerimg, "");
			}
		}catch(Exception e) {
			System.out.println("error contect save COL_bannerimg in AlbumData");
			values.put(Config.COL_bannerimg, "");
		} 
		
		try{
			
			if(config.getHomePageImg_URL().trim().length()>0){
				DefaultHttpClient mHttpClient = new DefaultHttpClient();
		    	HttpGet mHttpGet = new HttpGet( config.getHomePageImg_URL() );		//Link
		    	HttpResponse mHttpResponse = mHttpClient.execute(mHttpGet);
		    	if (mHttpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
		    	  HttpEntity entity = mHttpResponse.getEntity();
		    	    if ( entity != null) {
		    	      values.put(Config.COL_homepageimg, EntityUtils.toByteArray(entity));
		    	    }
		    	}else{
		    		values.put(Config.COL_homepageimg, "");
		    	}
			}else{
				values.put(Config.COL_homepageimg, "");
			}
		}catch(Exception e) {
			System.out.println("error contect save COL_homepageimg in AlbumData");
			values.put(Config.COL_homepageimg, "");
		}
		
		try{
			
			if(config.getHowToRegisterImg_URL().trim().length()>0){
				DefaultHttpClient mHttpClient = new DefaultHttpClient();
		    	HttpGet mHttpGet = new HttpGet( config.getHowToRegisterImg_URL() );		//Link
		    	HttpResponse mHttpResponse = mHttpClient.execute(mHttpGet);
		    	if (mHttpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
		    	  HttpEntity entity = mHttpResponse.getEntity();
		    	    if ( entity != null) {
		    	      values.put(Config.COL_howtoregisterimg, EntityUtils.toByteArray(entity));
		    	    }
		    	}else{
		    		values.put(Config.COL_howtoregisterimg, "");
		    	}
			}else{
				values.put(Config.COL_howtoregisterimg, "");
			}
		}catch(Exception e) {
			System.out.println("error contect save COL_howtoregisterimg in AlbumData");
			values.put(Config.COL_howtoregisterimg, "");
		}
		
		try{
			
			if(config.getHowToUseImg_URL().trim().length()>0){
				DefaultHttpClient mHttpClient = new DefaultHttpClient();
		    	HttpGet mHttpGet = new HttpGet( config.getHowToUseImg_URL() );		//Link
		    	HttpResponse mHttpResponse = mHttpClient.execute(mHttpGet);
		    	if (mHttpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
		    	  HttpEntity entity = mHttpResponse.getEntity();
		    	    if ( entity != null) {
		    	      values.put(Config.COL_howtouseimg, EntityUtils.toByteArray(entity));
		    	    }
		    	}else{
		    		values.put(Config.COL_howtouseimg, "");
		    	}
			}else{
				values.put(Config.COL_howtouseimg, "");
			}
		}catch(Exception e) {
			System.out.println("error contect save COL_howtouseimg in AlbumData");
			values.put(Config.COL_howtouseimg, "");
		}
		
		
		try{
			
			if(config.getProfileText_URL().trim().length()>0){
				DefaultHttpClient mHttpClient = new DefaultHttpClient();
		    	HttpGet mHttpGet = new HttpGet( config.getProfileText_URL() );		//Link
		    	HttpResponse mHttpResponse = mHttpClient.execute(mHttpGet);
		    	if (mHttpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
		    	  HttpEntity entity = mHttpResponse.getEntity();
		    	    if ( entity != null) {
		    	      values.put(Config.COL_profiletext, EntityUtils.toByteArray(entity));
		    	    }
		    	}else{
		    		values.put(Config.COL_profiletext, "");
		    	}
			}else{
				values.put(Config.COL_profiletext, "");
			}
		}catch(Exception e) {
			System.out.println("error contect save COL_profiletext in AlbumData");
			values.put(Config.COL_profiletext, "");
		}
		
		
		mContext.getContentResolver().update(UsersConfigDB.AuthenticationInfo.CONTENT_URI, 
		    values, 
		    selection, 
		    new String[]{selectionArgs} 
			);
	}
	
	
	
	
	
	public final List<Config> query( String selection, final String contentCodee) {
		
		System.out.println(" ConfigData -- query");
		
		// An array specifying which columns to return.
		String columns[] = new String[] { 
				Config.COL_bannerimg,
				Config.COL_homepageimg,
				Config.COL_howtoregisterimg,
				Config.COL_howtouseimg,
				Config.COL_profiletext }; //5

		//selection = Content.COL_contentcode + "=?";

		Uri myUri = UsersConfigDB.AuthenticationInfo.CONTENT_URI;
		Cursor cur = mContext.getContentResolver().query(myUri, columns, selection, new String[]{contentCodee}, null);
		
		List<Config> configListt = new ArrayList<Config>();

		// check if found any query result
		if (cur.getCount() > 0) {
			
			cur.moveToFirst();
			int numRows = cur.getCount();
			
			for (int i = 0; i < numRows; ++i) {
			
				Config config = new Config(); 
				
				byte[] bb = cur.getBlob(cur.getColumnIndex(Config.COL_bannerimg));
				config.setBannerImg(BitmapFactory.decodeByteArray(bb, 0, bb.length));		//Img
				
				byte[] bb2 = cur.getBlob(cur.getColumnIndex(Config.COL_homepageimg));
				config.setHomePageImg(BitmapFactory.decodeByteArray(bb2, 0, bb2.length));	
				
				byte[] bb3 = cur.getBlob(cur.getColumnIndex(Config.COL_howtoregisterimg));
				config.setHowToRegisterImg(BitmapFactory.decodeByteArray(bb3, 0, bb3.length));	
				
				byte[] bb4 = cur.getBlob(cur.getColumnIndex(Config.COL_howtouseimg));
				config.setHowToUseImg(BitmapFactory.decodeByteArray(bb4, 0, bb4.length));
				
				byte[] bb5 = cur.getBlob(cur.getColumnIndex(Config.COL_profiletext));
				config.setProfileText(BitmapFactory.decodeByteArray(bb5, 0, bb5.length));	
				
				
				configListt.add(config);
				
				/*
				if(configListt.get(i).getBannerImg()!=null){
					System.out.println("configListt "+i+" : getBannerImg != null");
				}
				
				if(configListt.get(i).getProfileText()!=null){
					System.out.println("configListt "+i+" : getProfileText != null");
				}
				*/
				
				cur.moveToNext();
				
			}
			
			return configListt;
		}
		
		// close cursor.
		cur.close();
		return null;
	}
	

	
	public void delete( final String selection, final String selectionArgs) {

		System.out.println(" ConfigData -- delete");
		
		//String selection = Content.COL_contentcode +"=?";
		
		Uri myUri = UsersConfigDB.AuthenticationInfo.CONTENT_URI;
		mContext.getContentResolver().delete(myUri, selection, new String[]{selectionArgs});
		
	}

}
