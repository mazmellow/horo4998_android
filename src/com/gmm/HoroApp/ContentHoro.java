package com.gmm.HoroApp;

public class ContentHoro {
	
	public static String COL_id="id";
	public static String COL_title="title";
	public static String COL_desc="desc";
	public static String COL_date="date";		//4 COL
	
	
	private String id="";
	private String title="";
	private String desc="";
	private String date="";
	
	private String ThumbImg="";
	private String Img="";
	private String Clip="";
	
	
	public String getID() {
		return(id);
	}
	
	public void setID(String id) {
		this.id=id;
	}
	
	
	public String getTitle() {
		return(title);
	}
	
	public void setTitle(String title) {
		this.title=title;
	}
	
	
	public String getDesc() {
		return(desc);
	}
	
	public void setDesc(String desc) {
		this.desc=desc;
	}
	
	
	public String getDate() {
		return(date);
	}
	
	public void setDate(String date) {
		this.date=date;
	}
	
	
	
	public String getThumbImg() {
		return(ThumbImg);
	}
	
	public void setThumbImg(String ThumbImg) {
		this.ThumbImg=ThumbImg;
	}
	
	
	public String getImg() {
		return(Img);
	}
	
	public void setImg(String Img) {
		this.Img=Img;
	}
	
	
	public String getClip() {
		return(Clip);
	}
	
	public void setClip(String Clip) {
		this.Clip=Clip;
	}

}
