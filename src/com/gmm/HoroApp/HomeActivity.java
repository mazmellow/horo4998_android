package com.gmm.HoroApp;

import java.util.ArrayList;
import java.util.List;

import com.gmm.HoroApp.R;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.telephony.TelephonyManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.ImageView;

public class HomeActivity extends Activity implements Runnable{
	
	private Button btnRegisStep, btnUseStep;
	
	public static Bitmap bitmapBGHome;
	public static Bitmap bitmapBanner;
	public static Bitmap bitProfileText;
	public static Bitmap bitmapHowToUse;
	public static Bitmap bitmapHowToRegister;
	
	private ProgressDialog pd;
	private ImageView imgHomePage;
	
	boolean update_config=false;
	boolean update_menulist=false;
	boolean update_menupluslist=false;
	boolean update_monthlyhoro=false;
	
	
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        System.out.println("----------- HomeActivity ----------"+checkInternet());
        
//        getCheckUpdate();	-->  copy to run
        
        btnRegisStep = (Button)findViewById(R.id.button1);
        btnUseStep = (Button)findViewById(R.id.button2);
        imgHomePage = (ImageView)findViewById(R.id.imageView2);
        
        //============ Query Config Database ========
        ConfigData configData = new ConfigData(HomeActivity.this);
        List<Config> config = new ArrayList<Config>();
        try {
			config = configData.query(null, null);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		if(config != null){
			if(config.size() > 0){
				bitmapBGHome = config.get(0).getHomePageImg();
				bitmapBanner = config.get(0).getBannerImg();
				bitProfileText = config.get(0).getProfileText();
				bitmapHowToUse = config.get(0).getHowToUseImg();
				bitmapHowToRegister = config.get(0).getHowToRegisterImg();
			}
		}
		
		if(bitmapBGHome != null){
			imgHomePage.setImageBitmap(bitmapBGHome);
		}
        
        btnRegisStep.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				insertDecorView(TabHomeActivityGroup.mLocalActivityManager.startActivity("registerstep", 
						new Intent(HomeActivity.this,RegisterStepActivity.class)
					.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
					)
					.getDecorView());
			}
		});
        
        btnUseStep.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				insertDecorView(TabHomeActivityGroup.mLocalActivityManager.startActivity("usestep", 
						new Intent(HomeActivity.this,UseStepActivity.class)
					.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
					)
					.getDecorView());
			}
		});
        
        pd = ProgressDialog.show(getParent(), "Loading..", "Please wait..",true,false);
        
        Thread thread = new Thread(this);
        thread.start();
      
      // setImageView by query DB
/*      
 	  ImageView img = (ImageView)findViewById(R.id.imgmainbanner);  
  	  ConfigData configData = new ConfigData(this);
      List<Config> conFigList = new ArrayList<Config>();

        
        try {
			conFigList = configData.query(null, null);
		} catch (Exception e) {
			e.printStackTrace();
		}
	
		if(conFigList != null){
			if(conFigList.size() > 0){
				img.setImageBitmap(conFigList.get(0).getBannerImg());
			}
			else{
				System.out.println("------- configlist == null");
			}
		}else{
			System.out.println("------- configlist <= 0");
		}
		
		*/
        
        
        /*
        //press How To
    	insertDecorView(TabHomeActivityGroup.mLocalActivityManager.startActivity("howto", new Intent(HomeActivity.this,HowToActivity.class)
		.putExtra("HowTo", "register")
		//.putExtra("HowTo", "use")
		.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)).getDecorView());
    	*/
         
	}
	
	
	public void getCheckUpdate(){
		
		if(checkInternet()){
		
			List<Version> version_list_DB = new ArrayList<Version>();
	        try{
	        	
	        	VersionData versionData = new VersionData(this);
	        	version_list_DB = versionData.query(null, null);
	        	
	        } catch (Exception e) {
				e.printStackTrace();
			}
	        
	        
	        String str_msisdn_profile="";
	    	List<Profile> profile_list = new ArrayList<Profile>();
	        try{
	        	
	        	ProfileData profileData = new ProfileData(this);
	        	profile_list = profileData.query(null, null);
	        	if(profile_list!=null){
	            	if( profile_list.size()>0 ){
	            		
	            		if( !profile_list.get(0).getMSISDN().equals("")){
	            			str_msisdn_profile=profile_list.get(0).getMSISDN();
	            		}
	            	}
	            }
	        } catch (Exception e) {
				e.printStackTrace();
			}
	        
	        
	        if(version_list_DB!=null){
	        	
	        	if( version_list_DB.size()>0 ){
	        		
	        		//check update
	        		
	        		List<Version> version_list_xml = new ArrayList<Version>();
	            	try{
	            		
	            		version_list_xml = HttpPostVersionControl.executeHttpPost( getDeviceID(), str_msisdn_profile);
	            		
	            	} catch (Exception e) {
	        			e.printStackTrace();
	        		}
	            	if(version_list_xml!=null){
	            		if(version_list_xml.size()>0){
	            			
	            			 update_config=false;
	            			 update_menulist=false;
	            			 update_menupluslist=false;
	            			 update_monthlyhoro=false;
	            			
	            			if( !version_list_DB.get(0).getConfigAPI().equals(version_list_xml.get(0).getConfigAPI())){
	            				
	            				//update ConfigDB
	            				update_config=true;
	            				
	            				try{
	            					ConfigData configData = new ConfigData(this);
	            					configData.delete(null, null);
	            				} catch (Exception e) {
	            					e.printStackTrace();
	            				}
	            				
	            				//parse Config & Insert ConfigDB
	            	        	getParseAndInsertConfig(str_msisdn_profile);
	            				
	            			}
	            			
	            			if( !version_list_DB.get(0).getMenuListAPI().equals(version_list_xml.get(0).getMenuListAPI())){
	            				
	            				//update MenuListDB
	            				update_menulist=true;
	            				
	            				try{
	            					MenuListData menuListData = new MenuListData(this);
	            					menuListData.delete(null, null);
	            				} catch (Exception e) {
	            					e.printStackTrace();
	            				}
	            				
	            				//parse MenuList & Insert MenuListDB
	            	        	getParseAndInsertMenuList(str_msisdn_profile);
	            				
	            			}
	
	
							if( !version_list_DB.get(0).getMenuPlusListAPI().equals(version_list_xml.get(0).getMenuPlusListAPI())){
								
								//update MenuPlusListDB
								update_menupluslist=true;
								
								try{
									MenuPlusListData menuPlusListData = new MenuPlusListData(this);
									menuPlusListData.delete(null, null);
								} catch (Exception e) {
									e.printStackTrace();
								}
								
								//parse MenuPlusList & Insert MenuPlusListDB
								getParseAndInsertMenuPlusList(str_msisdn_profile);
								
							}
	
	            			
	            			if( !version_list_DB.get(0).getMonthlyHoro().equals(version_list_xml.get(0).getMonthlyHoro())){
	            				
	            				//update MonthlyHoroDB
	            				update_monthlyhoro=true;
	            				
	            				try{
	            					MonthlyHoroData monthlyHoroData = new MonthlyHoroData(this);
	            					monthlyHoroData.delete(null, null);
	            				} catch (Exception e) {
	            					e.printStackTrace();
	            				}
	            				
	            				//parse ContentHoro & Insert MonthlyHoroDB
	            				getParseAndInsertMonthlyHoro(str_msisdn_profile);
	            				
	            			}
	            			
	            			
	            			if( (!update_config) && (!update_menulist) && (!update_menupluslist) && (!update_monthlyhoro)){
	            			
	            				//Not update VersionDB
	            				
	            			}else{
	            				
	            				//update VersionDB
	            				
	            				try{
	            					VersionData versionData = new VersionData(this);
	            					versionData.delete(null, null);
	            				} catch (Exception e) {
	            					e.printStackTrace();
	            				}
	            				
	            				//insert VersionDB
	            				try{
	            					
	            					Version version = new Version();
	            					if(update_config){
	            						version.setConfigAPI(version_list_xml.get(0).getConfigAPI());
	            					}else{
	            						version.setConfigAPI(version_list_DB.get(0).getConfigAPI());
	            					}
	            					if(update_menulist){
	            						version.setMenuListAPI(version_list_xml.get(0).getMenuListAPI());
	            					}else{
	            						version.setMenuListAPI(version_list_DB.get(0).getMenuListAPI());
	            					}
	            					if(update_menupluslist){
	            						version.setMenuPlusListAPI(version_list_xml.get(0).getMenuPlusListAPI());
	            					}else{
	            						version.setMenuPlusListAPI(version_list_DB.get(0).getMenuPlusListAPI());
	            					}
	            					if(update_monthlyhoro){
	            						version.setMonthlyHoro(version_list_xml.get(0).getMonthlyHoro());	
	            					}else{
	            						version.setMonthlyHoro(version_list_DB.get(0).getMonthlyHoro());	
	            					}
	            					
	            					version.setVideo(version_list_DB.get(0).getVideo());	//not update
	            					version.setLuckyNumber(version_list_DB.get(0).getLuckyNumber());	//plus
	            					version.setWeeklyHoro(version_list_DB.get(0).getWeeklyHoro());		//plus
	            					version.setSpecialVideo(version_list_DB.get(0).getSpecialVideo());	//plus
	            					VersionData versionData = new VersionData(this);
	            		        	versionData.insert(version);
	            					
	            				} catch (Exception e) {
	            					e.printStackTrace();
	            				}
	            				
	            			}
	            			
	        				
	
	            		}
	            	}
	            	
	        	}
	        	
	        }else{
	        	
	        	
	        	//open first app.
		        
	        	
	        	//parse VersionControl & Insert VersionDB 
	        	getParseAndInsertVersionControl(str_msisdn_profile);
	        	
	        	
	        	//parse Config & Insert ConfigDB
	        	getParseAndInsertConfig(str_msisdn_profile);
	        	
	        	
	        	//parse Menulist & Insert MenuListDB
	        	getParseAndInsertMenuList(str_msisdn_profile);
	        	
	        	
	        	//parse MenuPluslist & Insert MenuPlusListDB
	        	getParseAndInsertMenuPlusList(str_msisdn_profile);
	        	
	        	
	        	//parse ContentHoro & Insert MonthlyHoroDB
	        	getParseAndInsertMonthlyHoro(str_msisdn_profile);
	        	
	        }
	        
		}
        
	}
	
	
	public void getParseAndInsertVersionControl(String str_msisdn_profile){
		
		List<Version> version_list_First = new ArrayList<Version>();
    	try{
    		
    		version_list_First = HttpPostVersionControl.executeHttpPost(getDeviceID(), str_msisdn_profile);
    		
    	} catch (Exception e) {
			e.printStackTrace();
		}
    	if(version_list_First!=null){
    		if(version_list_First.size()>0){
    			getInsertVersionDB(version_list_First);
    		}
    	}
	}
	
	
	
	public void getParseAndInsertConfig(String str_msisdn_profile){
		
		List<Config> config_list_First = new ArrayList<Config>();
    	try{
    		
    		config_list_First = HttpPostConfig.executeHttpPost(getDeviceID(), str_msisdn_profile);
    		
    	} catch (Exception e) {
    		update_config=false;
			e.printStackTrace();
		}
    	if(config_list_First!=null){
    		if(config_list_First.size()>0){
    			getInsertConfigDB(config_list_First);
    		}
    	}else{
    		update_config=false;
    	}
	}
	
	public void getParseAndInsertMenuList(String str_msisdn_profile){
		
		List<MenuList> menuList_list_First = new ArrayList<MenuList>();
    	try{
    		
    		menuList_list_First = HttpPostMenuList.executeHttpPost(getDeviceID(), str_msisdn_profile, "1");
    		
    		if(menuList_list_First != null){
    			if(menuList_list_First.size() > 0){
    				for (int i = 0; i < menuList_list_First.size(); i++) {
						System.out.println("/////////////////////////////////////");
    					System.out.println(menuList_list_First.get(i).getMenuID());
    					System.out.println("/////////////////////////////////////");
					}
    			}
    		}
    		
    		
    	} catch (Exception e) {
    		update_menulist=false;
			e.printStackTrace();
		}
    	if(menuList_list_First!=null){
    		if(menuList_list_First.size()>0){
    			getInsertMenuListDB(menuList_list_First);
    		}
    	}else{
    		update_menulist=false;
    	}
	}
	
	
	public void getParseAndInsertMenuPlusList(String str_msisdn_profile){
		
		List<MenuList> menuPlusList_list_First = new ArrayList<MenuList>();
    	try{
    		
    		menuPlusList_list_First = HttpPostMenuList.executeHttpPost(getDeviceID(), str_msisdn_profile, "2");
    		
    	} catch (Exception e) {
    		update_menupluslist=false;
			e.printStackTrace();
		}
    	if(menuPlusList_list_First!=null){
    		if(menuPlusList_list_First.size()>0){
    			getInsertMenuPlusListDB(menuPlusList_list_First);
    		}
    	}else{
    		update_menupluslist=false;
    	}
	}
	
	
	public void getParseAndInsertMonthlyHoro(String str_msisdn_profile){
		
		List<ContentHoro> monthlyHoro_list_First = new ArrayList<ContentHoro>();
    	try{
    		
    		monthlyHoro_list_First = HttpPostContent.executeHttpPost(getDeviceID(), str_msisdn_profile, "1");
    		
    	} catch (Exception e) {
    		update_monthlyhoro=true;
			e.printStackTrace();
		}
    	if(monthlyHoro_list_First!=null){
    		if(monthlyHoro_list_First.size()>0){
    			getInsertMonthlyHoroDB(monthlyHoro_list_First);
    		}
    	}else{
    		update_monthlyhoro=true;
    	}
	}
	
	
	public void getInsertVersionDB(List<Version> version_list){
		
		try{
			
			VersionData versionData = new VersionData(this);
			versionData.insert(version_list.get(0));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public void getInsertConfigDB(List<Config> config_list){
		
		try{
			
			ConfigData configData = new ConfigData(this);
			configData.insert(config_list.get(0));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public void getInsertMenuListDB(List<MenuList> menuList_list){
		
		MenuListData menuListData = new MenuListData(this);
		Log.log("------- Method getInsertMenulistDB");
		Log.log("-------size : " + menuList_list.size());
		try{
			for(int i=0; i<menuList_list.size(); i++){
				
				try{
					menuListData.insert(menuList_list.get(i));
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public void getInsertMenuPlusListDB(List<MenuList> menuList_list){
		
		MenuPlusListData menuPlusListData = new MenuPlusListData(this);
		
		try{
			for(int i=0; i<menuList_list.size(); i++){
				
				try{
					menuPlusListData.insert(menuList_list.get(i));
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	

	public void getInsertMonthlyHoroDB(List<ContentHoro> monthlyHoro_list){
		
		MonthlyHoroData monthlyHoroData = new MonthlyHoroData(this);
		
		try{
			for(int i=0; i<monthlyHoro_list.size(); i++){
				ContentHoro monthlyHoro = new ContentHoro();
				monthlyHoro.setID(monthlyHoro_list.get(i).getID());
				monthlyHoro.setTitle(monthlyHoro_list.get(i).getTitle());
				monthlyHoro.setDesc(monthlyHoro_list.get(i).getDesc());
				monthlyHoro.setDate(monthlyHoro_list.get(i).getDate());
				
				try{
					monthlyHoroData.insert(monthlyHoro);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}	
	
	
	
	public String getDeviceID(){
    	
    	String str_device = null;
			try{
	        	TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
	        	str_device= telephonyManager.getDeviceId();
	        } catch (Exception e) {
				e.printStackTrace();
			}
	        try{
				if(str_device==null || str_device.equals("")){
					WifiManager wifiMan = (WifiManager)this.getSystemService(Context.WIFI_SERVICE);
		    		WifiInfo wifiInf = wifiMan.getConnectionInfo();
		    		str_device = wifiInf.getMacAddress();
				}
	    	} catch (Exception e) {
				e.printStackTrace();
			}
		return str_device;
	}
	
	
	private void insertDecorView(View view) {
   	 
    	getParent().setContentView(view, new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
    }
	
	
	public boolean checkInternet() {
    	ConnectivityManager connec =  ( ConnectivityManager ) getSystemService( Context.CONNECTIVITY_SERVICE ); 
    	if ( connec.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTED || connec.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTED ){
    	     // connect 
    		return true;
    	}else{
    	     // not-connect 
    		return false;
    	}
    }

	public void run() {
		try {
			Log.log("------ run HomeActivity ----");
			getCheckUpdate();
		
		} catch (Exception e) {
			e.printStackTrace();
		}
    
    	handler.sendEmptyMessage(0);
	}


	private Handler handler = new Handler() {
	    
	    public void handleMessage(Message msg) {
	    	
	    	try {
	    		
	        	pd.dismiss();
	            	
	    	}
	    	catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    }
	};
}
