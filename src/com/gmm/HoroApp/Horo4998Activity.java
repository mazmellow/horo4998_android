package com.gmm.HoroApp;

import java.util.ArrayList;
import java.util.List;

import com.gmm.HoroApp.R;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;

public class Horo4998Activity extends Activity {
    /** Called when the activity is first created. */
	
	static String App_ID ="186";
	static String AppVersion ="1.0";
	static String ApiVersion ="2.0.0";
	
	public static Bitmap bitmapCoverFlow;
	
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        System.out.println("----------- HoroscopeActivity ----------");
        
        bitmapCoverFlow = BitmapFactory.decodeResource(getResources(), R.drawable.luckynumber_menu);
        
        List<Profile> profile_list = new ArrayList<Profile>();
        
        try{
        	
        	ProfileData profileData = new ProfileData(this);
        	profile_list = profileData.query(null, null);
        	
        } catch (Exception e) {
			e.printStackTrace();
		}
        
        
        if(profile_list!=null){
        	
        	if( profile_list.size()>0 ){
        		
        		//go to TAB Home MainMenu
        		Intent homeIntent = new Intent(Horo4998Activity.this, TabMenuActivity.class);								//
    			startActivity(homeIntent);
    			finish();
        		
        	}
        	
        }else{
        	
        	//go to RegisterActivity
        	Intent homeIntent = new Intent(Horo4998Activity.this, RegisterActivity.class);								//
			startActivity(homeIntent);
			finish();
        	
        }
        
    }
}