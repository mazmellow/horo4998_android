package com.gmm.HoroApp;

import java.util.ArrayList;
import java.util.List;

import com.gmm.HoroApp.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;

public class HoroMenuActivity extends Activity{
	
	private TextView  textView;
//	static String URLXXX;
	ImageView image_banner;
	
	MenuListData menuListData;
	List<MenuList> listMenulist;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.horomenu);
        System.out.println("Slide Menu--------");
        menuListData = new MenuListData(this);
        listMenulist = new ArrayList<MenuList>();
        
        image_banner = (ImageView)findViewById(R.id.imghoromenubanner);

        if(HomeActivity.bitmapBanner != null){
        	image_banner.setImageBitmap(HomeActivity.bitmapBanner);
        }
        
        textView = (TextView) findViewById(this.getResources()
                .getIdentifier("statusText", "id", "com.gmm.HoroApp"));
        // note resources below are taken using getIdentifier to allow importing
        // this library as library.
        final CoverFlow coverFlow1 = (CoverFlow) findViewById(this.getResources().getIdentifier("coverflow", "id",
                "com.gmm.HoroApp"));
        setupCoverFlow(coverFlow1, false);
        final CoverFlow reflectingCoverFlow = (CoverFlow) findViewById(this.getResources().getIdentifier(
                "coverflowReflect", "id", "com.gmm.HoroApp"));
        setupCoverFlow(reflectingCoverFlow, true);
        
        try {
        	listMenulist = menuListData.query(null, null);
		} catch (Exception e) {
			e.printStackTrace();
		}
        
	}
	
    /**
     * Setup cover flow.
     * 
     * @param mCoverFlow
     *            the m cover flow
     * @param reflect
     *            the reflect
     */
    private void setupCoverFlow(final CoverFlow mCoverFlow, final boolean reflect) {
    	
        BaseAdapter coverImageAdapter;
        
        MenuListData menuListData = new MenuListData(this);
        List<MenuList> listMenuList = new ArrayList<MenuList>();
        
        try{
        	listMenuList =  menuListData.query(null, null);
        }catch (Exception e) {
        	e.printStackTrace();
		}
                
        
//        URLXXX = listMenuList.get(0).getMenuTypeUrl();
//        URLXXX = "http://www.google.com";
        
        
        if(listMenuList != null){
        	System.out.println("__----- listMenuList != null");
        	if(listMenuList.size()>0){
        		System.out.println("----- listMenuList > 0");
        		
        		 if (reflect) {
        	            coverImageAdapter = new ReflectingImageAdapter(new ResourceImageAdapter(this,listMenuList));
        	        } else {
        	            coverImageAdapter = new ResourceImageAdapter(this,listMenuList);
        	        }
        	        mCoverFlow.setAdapter(coverImageAdapter);
        	        mCoverFlow.setSelection(2, true);
        	        setupListeners(mCoverFlow);
        	}else{
        		System.out.println("listMenuList < 0");
        	}
        }else{
        	System.out.println("listMenuList = null");
        }
    }

    /**
     * Sets the up listeners.
     * 
     * @param mCoverFlow
     *            the new up listeners
     */
    private void setupListeners(final CoverFlow mCoverFlow) {
        mCoverFlow.setOnItemClickListener(new OnItemClickListener() {
            
            public void onItemClick(final AdapterView< ? > parent, final View view, final int position, final long id) {
                textView.setText("Item clicked! : " + id);
                Log.log("id:" + id);
                if(id > 3){
                	Log.log("id > 3");
                	insertDecorView(TabHoroActivityGroup.mLocalActivityManager.startActivity("webview", 
    						new Intent(HoroMenuActivity.this,WebViewActivity.class)
    					.putExtra("URLX", listMenulist.get((int)id).getMenuTypeUrl())
    					.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
    					)
    					.getDecorView());
                }else{
                	Log.log("id <= 3");
                	Intent gol = new Intent(HoroMenuActivity.this,TabHoroMenuActivity.class);
                	gol.putExtra("IDTAB", (int)(id+1));
                	gol.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                	startActivity(gol);
                }
                
            }

        });
        mCoverFlow.setOnItemSelectedListener(new OnItemSelectedListener() {
            
            public void onItemSelected(final AdapterView< ? > parent, final View view, final int position, final long id) {
                textView.setText("Item selected! : " + id);
            }

            
            public void onNothingSelected(final AdapterView< ? > parent) {
                textView.setText("Nothing clicked!");
            }
        });
    }
    
    
    private void insertDecorView(View view) {
      	 
    	getParent().setContentView(view, new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
    }
	

}
