package com.gmm.HoroApp;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.gmm.HoroApp.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.TextView.OnEditorActionListener;

public class HoroPlusMenuActivity extends Activity{
	
	
	EditText et_msisdn;
	String str_enter_msisdn;
	
	EditText et_onetimepass;
	String str_onetimepass;
	
	AlertDialog alertDialog_enterphone;
	AlertDialog alertDialog_enterOTP;
	
	
	private int mYear;
    private int mMonth;
    private int mDay;
    int n_month;
    static final int DATE_DIALOG_ID = 0;
    
    String str_msisdnReg=null;
	String DATE_Subscribe=null;
	String sMonth;
	String strShow_date;
	
	String MSISDN_Subscribe;
	
	private TextView textView;
	ImageView image_banner;
	
	static String URLYYY;
	
	static boolean check_member;
	
	
	MenuPlusListData menuplusListData;
	List<MenuList> listMenuList;
	
	boolean update_luckyNumber=false;
	boolean update_weeklyHoro=false;
	
	
	 @Override
	    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        setContentView(R.layout.horoplusmenu);
	        
	        System.out.println("----------- HoroPlusMenuActivity ----------"+checkInternet());

	        check_member = false;
	        
	        
	        menuplusListData = new MenuPlusListData(HoroPlusMenuActivity.this);
	        listMenuList = new ArrayList<MenuList>();
	        
	        image_banner = (ImageView)findViewById(R.id.imghoroplusmenubanner);
	        
	        if(HomeActivity.bitmapBanner != null){
	        	image_banner.setImageBitmap(HomeActivity.bitmapBanner);
	        }
	        
	        
		}
		

	 protected void onResume() {
			
			super.onResume();
			
			if(!check_member){
				
				getCheckProfile();
				
				
				
			}
			
			//test
//			else{
//				getCheckUpdate("6687541247");
//			}
			
			
		}

	 
	 public void getCheckProfile(){
		 
		if(checkInternet()){
			
			List<Profile> profile_list = new ArrayList<Profile>();
	        
	        try{
	        	
	        	ProfileData profileData = new ProfileData(this);
	        	profile_list = profileData.query(null, null);
	        	
	        } catch (Exception e) {
				e.printStackTrace();
			}
	        
	        if(profile_list!=null){
	        	
	        	if( profile_list.size()>0 ){
	        		
	        		if(profile_list.get(0).getMSISDN().equals("")){
	        			
	        			//pop up Enter Msisdn & Enter OTP 
	        			popupEnterPhoneNum();
	        			
	        		}else{
	        			
	        			if(profile_list.get(0).getFlagVerified().equals("no")){
	        				
	        				//pop up Confirm Msisdn 
	        				//change Msisdn --> pop up Enter Msisdn & Enter OTP
	        				//confirm Msisdn --> pop up Enter OTP
	        				
	        				String s_profile_msisdn = profile_list.get(0).getMSISDN();
	        				String s_profile_birthdate = profile_list.get(0).getBirthdate();
	        				String title = "";
	        				String s_msg_msisdn = "0"+s_profile_msisdn.substring(2);
	        				String message = "Your phone number is "+ s_msg_msisdn;
	        				showConfirmMsisdnDialogBox( title, message, s_profile_msisdn, s_profile_birthdate);
	        				
	        				
	        			}else if(profile_list.get(0).getFlagVerified().equals("yes")){
	        				
	        				
	        				getCheckMember( profile_list.get(0).getMSISDN() );
	        				
	        			}
	        		}
	        	}
	        	
	        }else{
	        	
	        	//pop up Enter Msisdn & Enter OTP
	        	popupEnterPhoneNum();
	        }
	        
	        
		}else{
			
			//no Internet
			//alert 
			String title = getString(R.string.title_internet_false);
            String message = getString(R.string.msg_internet_false);
            showNoticeDialogBox(title, message);
		}
		 
	 }
	 
	 
	 public void getCheckMember(String ck_member_MSISDN){
		 
		//check member 
		//yes: can use HoroPlus
		//no: set Msisdn="" and  FlagVerified="no"
		 
		List<StatusPost> statusPost_CheckMember = new ArrayList<StatusPost>();
 		try{
 			statusPost_CheckMember = HttpPostGetAccountDetail.executeHttpPost( getDeviceID(), ck_member_MSISDN);
 			
 		} catch (Exception e) {
			e.printStackTrace();
		}
 		
 		if(statusPost_CheckMember!=null){
     		if(statusPost_CheckMember.size()>0){
     			
     			if(statusPost_CheckMember.get(0).getStatus().trim().toUpperCase().equals("OK")){	
    				
    				//status : OK  ,, Member
    				//Use App**
     				
     				System.out.println("getZODIAC_ID :"+statusPost_CheckMember.get(0).getZODIAC_ID());
     				System.out.println("getZODIAC :"+statusPost_CheckMember.get(0).getZODIAC());
     				System.out.println("getBIRTHDATE :"+statusPost_CheckMember.get(0).getBIRTHDATE());
     			
     				
     				List<Profile> profile_list = new ArrayList<Profile>();
     		        try{
     		        	ProfileData profileData = new ProfileData(this);
     		        	profile_list = profileData.query(null, null);
     		        } catch (Exception e) {
     					e.printStackTrace();
     				}
     		        if(profile_list!=null){
     		        	if( profile_list.size()>0 ){
     		        		if(!profile_list.get(0).getBirthdate().equals(statusPost_CheckMember.get(0).getBIRTHDATE().trim())){
     		        			
     		        			try{
	            		        	ProfileData profileData = new ProfileData(HoroPlusMenuActivity.this);
	            		        	profileData.delete(null, null);
	            		        } catch (Exception e) {
	            					e.printStackTrace();
	            				}
	            		        
	            				//insert Msisdn 
	            				getInsertProfile(ck_member_MSISDN, "yes", statusPost_CheckMember.get(0).getBIRTHDATE().trim());
	            				
     		        		}
     		        	}
     		        }
     				
     				getCheckUpdate(ck_member_MSISDN);
     				
     				
    						
        		} else{
        			
        			//status : ERR  ,, Not Member
        			
        			System.out.println("----- status : ERR  ,, Not Member");
        			
        			//alert
    				String title = "Not member";
    				String message = "Do you want to subscripe ?";
    				showSubscribeDialogBox( title, message, ck_member_MSISDN);
    				
        		}
     		}
     	}
		 
	 }
	 
	 
	 public void getCheckUpdate(String Msisdn){
		 
		 
		 List<Version> version_list_DB = new ArrayList<Version>();
	        try{
	        	
	        	VersionData versionData = new VersionData(this);
	        	version_list_DB = versionData.query(null, null);
	        	
	        } catch (Exception e) {
				e.printStackTrace();
			}
	       if(version_list_DB!=null){
	        	if( version_list_DB.size()>0 ){
	        		
	        		//check update 
	        		
	        		List<Version> version_list_xml = new ArrayList<Version>();
	            	try{
	            		
	            		version_list_xml = HttpPostVersionControl.executeHttpPost( getDeviceID(), Msisdn);
	            		
	            	} catch (Exception e) {
	        			e.printStackTrace();
	        		}
	            	
	            	if(version_list_xml!=null){
	            		if(version_list_xml.size()>0){
	            			
	            			 update_luckyNumber=false;
	            			 update_weeklyHoro=false;
	            			
	            			
	            			if( !version_list_DB.get(0).getLuckyNumber().equals(version_list_xml.get(0).getLuckyNumber())){
	            				
	            				//update LuckyNumberDB
	            				update_luckyNumber=true;
	            				
	            				try{
	            					LuckyNumberData LuckyNumberData = new LuckyNumberData(this);
	            					LuckyNumberData.delete(null, null);
	            				} catch (Exception e) {
	            					e.printStackTrace();
	            				}
	            				
	            				//parse LuckyNumber & Insert LuckyNumberDB
	            				getParseAndInsertLuckyNumber(Msisdn);
	            				
	            				
	            			}else{
	            				
	            				//First
	            				List<ContentHoro> LuckyNumber_list = new ArrayList<ContentHoro>();
	            				try{
	            					LuckyNumberData luckyNumberData = new LuckyNumberData(this);
	            					LuckyNumber_list = luckyNumberData.query(null, null);
	            				} catch (Exception e) {
	            					e.printStackTrace();
	            				}
	            				
	            				if(LuckyNumber_list!=null){
	            					//not
	            				}else{
	            					
	            					//parse LuckyNumber & Insert LuckyNumberDB
		            				getParseAndInsertLuckyNumber(Msisdn);
	            				}
	            				
	            			}
	            			
	            			
	            			
	            			if( !version_list_DB.get(0).getWeeklyHoro().equals(version_list_xml.get(0).getWeeklyHoro())){
	            				
	            				//update WeeklyHoroDB
	            				update_weeklyHoro=true;
	            				
	            				try{
	            					WeeklyHoroData weeklyHoroData = new WeeklyHoroData(this);
	            					weeklyHoroData.delete(null, null);
	            				} catch (Exception e) {
	            					e.printStackTrace();
	            				}
	            				
	            				//parse WeeklyHoro & Insert WeeklyHoroDB
	            				getParseAndInsertWeeklyHoro(Msisdn);
	            				
	            			}else{
	            				
	            				//First
	            				List<ContentHoro> monthlyHoro_list_First = new ArrayList<ContentHoro>();
	            				try{
	            					WeeklyHoroData weeklyHoroData = new WeeklyHoroData(this);
	            					monthlyHoro_list_First = weeklyHoroData.query(null, null);
	            				} catch (Exception e) {
	            					e.printStackTrace();
	            				}
	            				if(monthlyHoro_list_First!=null){
	            					//not
	            				}else{
	            					//parse WeeklyHoro & Insert WeeklyHoroDB
		            				getParseAndInsertWeeklyHoro(Msisdn);
	            				}
	            			}
	            			
	            			
	            			
	            			if( (!update_luckyNumber) && (!update_weeklyHoro) ){
	                			
	            				//Not update VersionDB
	            				
	            			}else{
	            				
	            				
	            				//update VersionDB
	            				
	            				try{
	            					VersionData versionData = new VersionData(this);
	            					versionData.delete(null, null);
	            				} catch (Exception e) {
	            					e.printStackTrace();
	            				}
	            				
	            				//insert VersionDB
	            				try{
	            					
	            					Version version = new Version();
	            					version.setConfigAPI(version_list_DB.get(0).getConfigAPI());
	            					version.setMenuListAPI(version_list_DB.get(0).getMenuListAPI());
	            					version.setMenuPlusListAPI(version_list_DB.get(0).getMenuPlusListAPI());
	            					version.setMonthlyHoro(version_list_DB.get(0).getMonthlyHoro());
	            					version.setVideo(version_list_DB.get(0).getVideo());
	            					if(update_luckyNumber){
	            						version.setLuckyNumber(version_list_xml.get(0).getLuckyNumber());	//plus
	            					}else{
	            						version.setLuckyNumber(version_list_DB.get(0).getLuckyNumber());	//plus
	            					}
	            					if(update_weeklyHoro){
	            						version.setWeeklyHoro(version_list_xml.get(0).getWeeklyHoro());		//plus
	            					}else{
	            						version.setWeeklyHoro(version_list_DB.get(0).getWeeklyHoro());		//plus
	            					}
	            					version.setSpecialVideo(version_list_DB.get(0).getSpecialVideo());		//not update
	            					VersionData versionData = new VersionData(this);
	            		        	versionData.insert(version);
	            					
	            				} catch (Exception e) {
	            					e.printStackTrace();
	            				}
	            				
	            			}
	            			
	            			
	            			
	            			check_member = true;
	            			
	            			//Start Tum
	            			
	            			textView = (TextView) findViewById(this.getResources()
	            	                .getIdentifier("statusText", "id", "com.gmm.HoroApp"));
	            	        // note resources below are taken using getIdentifier to allow importing
	            	        // this library as library.

	            	        final CoverFlowPlus coverFlow1 = (CoverFlowPlus) findViewById(this.getResources().getIdentifier("coverflow", "id",
	            	                "com.gmm.HoroApp"));

	            	        setupCoverFlow(coverFlow1, false);

	            	        final CoverFlowPlus reflectingCoverFlow = (CoverFlowPlus) findViewById(this.getResources().getIdentifier(
	            	                "coverflowReflect", "id", "com.gmm.HoroApp"));

	            	        setupCoverFlow(reflectingCoverFlow, true);

	            	        try {
	            				listMenuList = menuplusListData.query(null, null);
	            			} catch (Exception e) {
	            				e.printStackTrace();
	            			}
	            			
	            			
	            			
	            		}
	            	}
	            	
	        	}
	       }
	         
	 }
	 
	 
	 public void getParseAndInsertLuckyNumber(String str_msisdn_profile){
			
		List<ContentHoro> contentHoro_luckynumber_list = new ArrayList<ContentHoro>();
    	try{
    		
    		contentHoro_luckynumber_list = HttpPostContent.executeHttpPost( getDeviceID(), str_msisdn_profile, "3");
    		
    	} catch (Exception e) {
    		update_luckyNumber=false;
			e.printStackTrace();
		}
    	if(contentHoro_luckynumber_list!=null){
    		if(contentHoro_luckynumber_list.size()>0){
    			getInsertLuckyNumberDB(contentHoro_luckynumber_list);
    		}
    	}else{
    		update_luckyNumber=false;
    	}
	}
	 
	 public void getInsertLuckyNumberDB(List<ContentHoro> contentHoro_luckynumber_list){
		
		LuckyNumberData luckyNumberData = new LuckyNumberData(this);
		try{
			for(int i=0; i<contentHoro_luckynumber_list.size(); i++){
				ContentHoro luckynumberHoro = new ContentHoro();
				luckynumberHoro.setID(contentHoro_luckynumber_list.get(i).getID());
				luckynumberHoro.setTitle(contentHoro_luckynumber_list.get(i).getTitle());
				luckynumberHoro.setDesc(contentHoro_luckynumber_list.get(i).getDesc());
				luckynumberHoro.setDate(contentHoro_luckynumber_list.get(i).getDate());
				
				try{
					luckyNumberData.insert(luckynumberHoro);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	 
	 
	 public void getParseAndInsertWeeklyHoro(String str_msisdn_profile){
		
		List<ContentHoro> monthlyHoro_list_First = new ArrayList<ContentHoro>();
    	try{
    		
    		monthlyHoro_list_First = HttpPostContent.executeHttpPost( getDeviceID(), str_msisdn_profile, "4");
    		
    	} catch (Exception e) {
    		update_weeklyHoro=false;
			e.printStackTrace();
		}
    	if(monthlyHoro_list_First!=null){
    		if(monthlyHoro_list_First.size()>0){
    			getInsertWeeklyHoroDB(monthlyHoro_list_First);
    		}
    	}else{
    		update_weeklyHoro=false;
    	}
	}
	 
	 public void getInsertWeeklyHoroDB(List<ContentHoro> monthlyHoro_list){
			
		 WeeklyHoroData weeklyHoroData = new WeeklyHoroData(this);
		
		try{
			for(int i=0; i<monthlyHoro_list.size(); i++){
				ContentHoro monthlyHoro = new ContentHoro();
				monthlyHoro.setID(monthlyHoro_list.get(i).getID());
				monthlyHoro.setTitle(monthlyHoro_list.get(i).getTitle());
				monthlyHoro.setDesc(monthlyHoro_list.get(i).getDesc());
				monthlyHoro.setDate(monthlyHoro_list.get(i).getDate());
				
				try{
					weeklyHoroData.insert(monthlyHoro);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
 
	 
	 public void getPicker_Enter_Birthdate(){
		 
		 System.out.println("--------------- getPicker_Enter_Birthdate");
		 
		 final Calendar c = Calendar.getInstance();
	        mYear = c.get(Calendar.YEAR);
	        mMonth = c.get(Calendar.MONTH);
	        mDay = c.get(Calendar.DAY_OF_MONTH);
		 
		 showDialog(DATE_DIALOG_ID);
		 
	 }
	 
	 
	 public void getSubscriptTrial(){
		 
		 System.out.println("--- getSubscriptTrial");
		 
		 List<StatusPost> statusPost_SubscriptTrial = new ArrayList<StatusPost>();
 		try{
 			
 			statusPost_SubscriptTrial = HttpPostSubscriptTrial.executeHttpPost( getDeviceID(), MSISDN_Subscribe, DATE_Subscribe );
 			
 		} catch (Exception e) {
				e.printStackTrace();
			}
 		
 		if(statusPost_SubscriptTrial!=null){
     		if(statusPost_SubscriptTrial.size()>0){
     			
     			if(statusPost_SubscriptTrial.get(0).getStatus().trim().toUpperCase().equals("OK")){	
     				
     				//status : OK
     				
     				System.out.println("--- getSubscriptTrial ==== status : OK");
     				
     				try{
    		        	ProfileData profileData = new ProfileData(HoroPlusMenuActivity.this);
    		        	profileData.delete(null, null);
    		        } catch (Exception e) {
    					e.printStackTrace();
    				}
    		        
    				//insert Msisdn 
    				getInsertProfile( MSISDN_Subscribe, "yes", DATE_Subscribe);
    				
     				
     				//alert
         			String title = "Status Subscript Trial";
         			String message;
         			if(statusPost_SubscriptTrial.get(0).getStatusCode().trim().equals("")){
            			message = "success";
            		}else {
            			message =  statusPost_SubscriptTrial.get(0).getStatusCode();
            		}
         			showNoticeDialogBox(title, message);
     				
     						
         		} else{
         			
         			//status : ERR
         			
         			System.out.println("--- getSubscriptTrial ==== status : ERR");
         			
         			MSISDN_Subscribe="";
         			DATE_Subscribe="";
         				
         			//alert
         			String title = "Status Subscript Trial";
         			String message;
         			if(statusPost_SubscriptTrial.get(0).getStatusDetail().trim().equals("")){
	            			message = "not success";
	            		}else {
	            			message =  statusPost_SubscriptTrial.get(0).getStatusDetail();
	            		}
         			showNoticeDialogBox(title, message);
         		}
     			
     		}
 		}
 		
	 }
	 
	 
	 public void popupEnterPhoneNum(){
			
			try {
				
				System.out.println("==== popupEnterPhoneNum ====");
				
				AlertDialog.Builder builder;
				
				//We need to get the instance of the LayoutInflater, use the context of this activity
	            LayoutInflater inflater = (LayoutInflater) HoroPlusMenuActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	            
	            //Inflate the view from a predefined XML layout
	            View layout = inflater.inflate(R.layout.popup_msisdn, (ViewGroup) findViewById(R.id.popup_msisdn));
	            
	            
	            str_enter_msisdn = null;
	            
	            et_msisdn = (EditText) layout.findViewById(R.id.popupmsisdn_et_msisdn);
	            et_msisdn.setOnEditorActionListener(new OnEditorActionListener(){
	    			public boolean onEditorAction(TextView v, int actionId,
	    					KeyEvent event) {
	    				
	    					//Hide keyBoard
	    	                InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
	    	                in.hideSoftInputFromWindow(et_msisdn
	    	                        .getApplicationWindowToken(),
	    	                        InputMethodManager.HIDE_NOT_ALWAYS);
	    					
	    	                //Set
	    	                str_enter_msisdn = null;
	    	                str_enter_msisdn = et_msisdn.getText().toString().trim();
	    	                str_enter_msisdn = "66"+str_enter_msisdn.substring(1);
	    	                
	    	                
	    				return false;
	    			}
	            });
	            
	            
	            ImageView i_msisdn_butt_cancel = (ImageView) layout.findViewById(R.id.popupmsisdn_butt_cancel);
	            i_msisdn_butt_cancel.setOnClickListener(new OnClickListener() {
	    	            public void onClick(final View v) {
	    	            	
	    	            	System.out.println("press : i_msisdn_butt_cancel");
	    	            	
	    	            	alertDialog_enterphone.cancel();
	    	            	
	    	            }
	    	        });
	            
	            
	            ImageView i_msisdn_butt_ok = (ImageView) layout.findViewById(R.id.popupmsisdn_butt_ok);
	            i_msisdn_butt_ok.setOnClickListener(new OnClickListener() {
	    	            public void onClick(final View v) {
	    	            	
	    	            	System.out.println("press : i_msisdn_butt_ok");
	    	            	
	    	            	
	    	            	if( !et_msisdn.getText().toString().trim().equals("") ){
	    	            		str_enter_msisdn = et_msisdn.getText().toString().trim();
	    	            		str_enter_msisdn = "66"+str_enter_msisdn.substring(1);
	    	            	}
	    	            	
	    	            	
	    	            	if(str_enter_msisdn!=null && !str_enter_msisdn.equals("") && str_enter_msisdn.length()==11){
	    	            		
	    	            		
	    	            		System.out.println("str_enter_msisdn ::"+str_enter_msisdn);
	    	            		
	    	            		
	    	            		alertDialog_enterphone.cancel();
	    	            		
	    	            		//post Send OTP : status
	    	            		List<StatusPost> statusPost_sendOTP = new ArrayList<StatusPost>();
	    	            		try{
	    	            			
	    	            			statusPost_sendOTP = HttpPostSendOTP.executeHttpPost( getDeviceID(),str_enter_msisdn);
	    	            			
	    	            		} catch (Exception e) {
									e.printStackTrace();
								}
	    	            		
	    	            		if(statusPost_sendOTP!=null){
		    	            		if(statusPost_sendOTP.size()>0){
			    	            		
			    	            		if(statusPost_sendOTP.get(0).getStatus().trim().toUpperCase().equals("OK")){
			    	            			
			    	            			//status : OK
			    	            			
				    	            		//next popup enter OTP
				    	            		popupEnterOTP( str_enter_msisdn, "" , false);
				    	            		
			    	            		} else{
			    	            			
			    	            			//status : ERR 
			    	            			
				    	            		//alert
			    	            			String title = "Status send phone number";
			                                String message = null;
			                                if(statusPost_sendOTP.get(0).getStatusDetail().trim().equals("")){
			                                	message = "not success";
			                                } else{
			                                	message = statusPost_sendOTP.get(0).getStatusDetail();
			                                }
			                                showNoticeSendOTPErrDialogBox(title, message);
			                                
			    	            		}
		    	            		}
	    	            		}
	    	            		
	    	            	} else{
	    	            		
	    	            		//alert : Please, Enter your phone number.
	                    		String title = getString(R.string.popup_msisdn_title);
	                            String message = getString(R.string.popup_msisdn_msg);
	                            showNoticeDialogBox(title, message);
	    	            	}
	    	            	
	    	            }
	    	        });
	            
	            
	            builder = new AlertDialog.Builder(getParent()).setCancelable(false);
	            builder.setView(layout);
	            builder.setInverseBackgroundForced(true);
	            alertDialog_enterphone = builder.create();
	            alertDialog_enterphone.show();
				
			} catch (Exception e) {
	            e.printStackTrace();
	        }
			
		}
	 
	 
	 public void popupEnterOTP(final String str_otp_Msisdn, final String str_otp_Birthdate, final boolean from_confirm){
			
			try {
				
				System.out.println("==== popupEnterOTP ===="+str_otp_Msisdn);
				
				AlertDialog.Builder builder;
				
				//We need to get the instance of the LayoutInflater, use the context of this activity
	            LayoutInflater inflater = (LayoutInflater) HoroPlusMenuActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	            
	            //Inflate the view from a predefined XML layout
	            View layout = inflater.inflate(R.layout.popup_otp, (ViewGroup) findViewById(R.id.popup_otp));
	            
	            
	            et_onetimepass = (EditText) layout.findViewById(R.id.popupotp_et_otp);
	            et_onetimepass.setOnEditorActionListener(new OnEditorActionListener(){
	    			public boolean onEditorAction(TextView v, int actionId,
	    					KeyEvent event) {
	    				
	    					//Hide keyBoard
	    	                InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
	    	                in.hideSoftInputFromWindow(et_onetimepass
	    	                        .getApplicationWindowToken(),
	    	                        InputMethodManager.HIDE_NOT_ALWAYS);
	    					
	    	                //Set
	    	                str_onetimepass = null;
	    	                str_onetimepass = et_onetimepass.getText().toString().trim();
	    	            	
	    	                
	    				return false;
	    			}
	            });
	            
	            
	            ImageView i_otp_butt_cancel = (ImageView) layout.findViewById(R.id.popupotp_butt_cancel);
	            i_otp_butt_cancel.setOnClickListener(new OnClickListener() {
	    	            public void onClick(final View v) {
	    	            	
	    	            	System.out.println("press : i_otp_butt_cancel");
	    	            	
	    	            	alertDialog_enterOTP.cancel();
	    	            	
	    	            	if(from_confirm){
	    	            		
		        				String title = "";
		        				String s_msg_msisdn = "0"+str_otp_Msisdn.substring(2);
		        				String message = "Your phone number is "+ s_msg_msisdn;
		        				showConfirmMsisdnDialogBox( title, message, str_otp_Msisdn, str_otp_Birthdate);
		        				
	    	            	}else{
	    	            		popupEnterPhoneNum();
	    	            	}
	    	            	
	    	            }
	    	        });
	            
	            
	            ImageView i_otp_butt_ok = (ImageView) layout.findViewById(R.id.popupotp_butt_ok);
	            i_otp_butt_ok.setOnClickListener(new OnClickListener() {
	    	            public void onClick(final View v) {
	    	            	
	    	            	System.out.println("press : i_otp_butt_ok");
	    	            	
	    	            	
	    	            	if( !et_onetimepass.getText().toString().trim().equals("") ){
	    	            		str_onetimepass = et_onetimepass.getText().toString().trim();
	    	            	}
	    	            	
	    	            	if(str_onetimepass!=null && !str_onetimepass.equals("")){
	    	            		
	    	            		
	    	            		alertDialog_enterOTP.cancel();
	    	            		
	    	            		//post Check OTP : status
	    	            		List<StatusPost> statusPost_validateOTP = new ArrayList<StatusPost>();
	    	            		try{
	    	            			
	    	            			statusPost_validateOTP = HttpPostValidateOTP.executeHttpPost( getDeviceID(), str_otp_Msisdn, str_onetimepass);
	    	            			
	    	            		} catch (Exception e) {
									e.printStackTrace();
								}
	    	            		
	    	            		
	    	            		if(statusPost_validateOTP!=null){
		    	            		if(statusPost_validateOTP.size()>0){
		    	            			
		    	            			if(statusPost_validateOTP.get(0).getStatus().trim().toUpperCase().equals("OK")){	
		    	            				
		    	            				//status : OK
		    	            				
		    	            				try{
		    	            		        	
		    	            		        	ProfileData profileData = new ProfileData(HoroPlusMenuActivity.this);
		    	            		        	profileData.delete(null, null);
		    	            		        	
		    	            		        } catch (Exception e) {
		    	            					e.printStackTrace();
		    	            				}
		    	            				
		    	            				//insert Msisdn 
		    	            				getInsertProfile(str_otp_Msisdn, "yes", str_otp_Birthdate);
		    	            				
		    	            				
		    	            				//alert
		        	            			String title = "Status send OTP";
		        	            			String message;
		        	            			if(statusPost_validateOTP.get(0).getStatusDetail().trim().equals("")){
			        	            			message = "success";
			        	            		}else {
			        	            			message =  statusPost_validateOTP.get(0).getStatusDetail();
			        	            		}
		        	            			showNoticeSendOTPSuccessDialogBox(title, message, str_otp_Msisdn);
		    	            				
		    	            						
		        	            		} else{
		        	            			
		        	            			//status : ERR
		        	            			
		        	            			//alert
		        	            			String title = "Status send OTP";
		        	            			String message;
		        	            			if(statusPost_validateOTP.get(0).getStatusDetail().trim().equals("")){
			        	            			message = getString( R.string.popup_otp_msg_err );
			        	            		}else {
			        	            			message =  statusPost_validateOTP.get(0).getStatusDetail();
			        	            		}
		                            		showNoticeDialogBox(title, message);
		        	            		}
		    	            			
		    	            		}
	    	            		}
	    	            		
	    	            	} else{
	    	            		
	    	            		//alert 
	                    		String title = getString(R.string.popup_otp_title);
	                            String message = getString(R.string.popup_otp_msg);
	                            showNoticeDialogBox(title, message);
	    	            	}
	    	            	
	    	            	
	    	            	
	    	            }
	    	        });
	            
	            
	            builder = new AlertDialog.Builder(getParent()).setCancelable(false);
	            builder.setView(layout);
	            builder.setInverseBackgroundForced(true);
	            alertDialog_enterOTP = builder.create();
	            alertDialog_enterOTP.show();
	            
				
			} catch (Exception e) {
	            e.printStackTrace();
	        }
			
		}
	 
	 
	 
	 public void getInsertProfile(String str_msisdn, String str_flagverified, String str_birthday){
	    	
    	Profile profile = new Profile();
    	profile.setMSISDN(str_msisdn);
    	profile.setFlagVerified(str_flagverified);
    	profile.setBirthdate(str_birthday);
    	
    	try{
        	
        	ProfileData profileData = new ProfileData(this);
        	profileData.insert(profile);
        	
        } catch (Exception e) {
			e.printStackTrace();
		}
    	
    }
	 
	 
	 private void showNoticeDialogBox(final String title, final String message) {
	    	
	    	System.out.println("---- showNoticeDialogBox");
	        Builder setupAlert;
	        setupAlert = new AlertDialog.Builder( getParent() );    
	        setupAlert.setTitle(title);
	        setupAlert.setMessage(message);
	        setupAlert.setPositiveButton("Ok",
	            new DialogInterface.OnClickListener() {
	              public void onClick(DialogInterface dialog, int whichButton) {
	            	  
	              }
	            });
	        setupAlert.show();
	      }
	 
	 

	 private void showNoticeSendOTPErrDialogBox(final String title, final String message) {
	    	
	    	System.out.println("---- showNoticeSendOTPErrDialogBox");
	        Builder setupAlert;
	        setupAlert = new AlertDialog.Builder( getParent() );    
	        setupAlert.setTitle(title);
	        setupAlert.setMessage(message);
	        setupAlert.setPositiveButton("Ok",
	            new DialogInterface.OnClickListener() {
	              public void onClick(DialogInterface dialog, int whichButton) {
	            	  popupEnterPhoneNum();
	              }
	            });
	        setupAlert.show();
	      }
	 
	 private void showNoticeSendOTPSuccessDialogBox(final String title, final String message, final String MSISDN) {
	    	
	    	System.out.println("---- showNoticeSendOTPSuccessDialogBox");
	        Builder setupAlert;
	        setupAlert = new AlertDialog.Builder( getParent() );    
	        setupAlert.setTitle(title);
	        setupAlert.setMessage(message);
	        setupAlert.setPositiveButton("Ok",
	            new DialogInterface.OnClickListener() {
	              public void onClick(DialogInterface dialog, int whichButton) {
	            	  
	            	  getCheckMember(MSISDN);
	              }
	            });
	        setupAlert.show();
	      }
	 
	 
	 private void showConfirmMsisdnDialogBox(final String title, final String message, final String msisdnn, final String birthdatee) {
	    	
	    	System.out.println("-- showChargingDialogBox");
	        Builder setupAlert;
	        setupAlert = new AlertDialog.Builder( getParent() ).setTitle(title).setMessage(message).setPositiveButton( "Change" ,
	            new DialogInterface.OnClickListener() {
	              public void onClick(DialogInterface dialog, int whichButton) {
	            	  
	            	  System.out.println("Change :");
	            	  
	            	  try{
	                  	
	                  	ProfileData profileData = new ProfileData(HoroPlusMenuActivity.this);
	                  	profileData.delete(null, null);
	                  	
	                  	getInsertProfile_Press_Skip();
	                  	
	                  } catch (Exception e) {
	          			e.printStackTrace();
	          		  }
	            	  
	            	  popupEnterPhoneNum();
	    
	            	  
	              }
	        	}).setNegativeButton( "yes" , new DialogInterface.OnClickListener() {
			      public void onClick(DialogInterface dialog, int whichButton) {
			    	  
			    	  System.out.println("Confirm : ");
			    	  
			    	  popupEnterOTP( msisdnn, birthdatee, true);
			    	  	
			      }
	        	});
	        setupAlert.show();
		}
	 
	 
	 public void getInsertProfile_Press_Skip(){
	    	
	    	Profile profile = new Profile();
	    	profile.setMSISDN("");
	    	profile.setFlagVerified("no");
	    	profile.setBirthdate("");
	    	
	    	try{
	        	
	        	ProfileData profileData = new ProfileData(this);
	        	profileData.insert(profile);
	        	
	        } catch (Exception e) {
				e.printStackTrace();
			}
	    	
	    }
	 
	 
	 private void showSubscribeDialogBox(final String title, final String message, final String msisdn) {
	    	
	    	System.out.println("-- showChargingDialogBox");
	        Builder setupAlert;
	        setupAlert = new AlertDialog.Builder( getParent() ).setTitle(title).setMessage(message).setPositiveButton( "yes" ,
	            new DialogInterface.OnClickListener() {
	              public void onClick(DialogInterface dialog, int whichButton) {
	            	  
	            	  System.out.println("yes :");
	            	  
	            	  MSISDN_Subscribe = msisdn;
	            	  getPicker_Enter_Birthdate();
	    
	              }
	        	}).setNegativeButton( "no" , new DialogInterface.OnClickListener() {
			      public void onClick(DialogInterface dialog, int whichButton) {
			    	  
			    	  System.out.println("no : ");
			    	  
			    	  try{
      		        	
      		        	ProfileData profileData = new ProfileData(HoroPlusMenuActivity.this);
      		        	profileData.delete(null, null);
      		        	
      		        	getInsertProfile_Press_Skip();
      		        	
      		        	} catch (Exception e) {
      		        		e.printStackTrace();
      		        	}
      				
			      }
	        	});
	        setupAlert.show();
		}
	 
	 
	 
	 @Override
	    protected Dialog onCreateDialog(int id) {
	        switch (id) {
	        case DATE_DIALOG_ID:
	            return new DatePickerDialog( getParent() ,
	                        mDateSetListener,
	                        mYear, mMonth, mDay);
	        }
	        return null;
	    }
	    
	    // updates the date we display in the TextView
	    private void updateDisplay() {
	    	
	    	System.out.println("---updateDisplay");
	    	
	        System.out.println("mYear :"+mYear);
	        System.out.println("mMonth :"+mMonth);
	        System.out.println("mDay :"+mDay);
	        
	        n_month = mMonth+1;
	  
	        
	        DATE_Subscribe = mYear+"-"+n_month+"-"+mDay;
	        
	        System.out.println("str_date :"+DATE_Subscribe);
	        
	        
	        if(DATE_Subscribe!=null && !DATE_Subscribe.equals("")){
				 getSubscriptTrial();
			 }
	        
	        /*
	        if(n_month==1){
	        	sMonth = getString(R.string.month1);
	        }else if(n_month==2){
	        	sMonth = getString(R.string.month2);
	        }else if(n_month==3){
	        	sMonth = getString(R.string.month3);
	        }else if(n_month==4){
	        	sMonth = getString(R.string.month4);
	        }else if(n_month==5){
	        	sMonth = getString(R.string.month5);
	        }else if(n_month==6){
	        	sMonth = getString(R.string.month6);
	        }else if(n_month==7){
	        	sMonth = getString(R.string.month7);
	        }else if(n_month==8){
	        	sMonth = getString(R.string.month8);
	        }else if(n_month==9){
	        	sMonth = getString(R.string.month9);
	        }else if(n_month==10){
	        	sMonth = getString(R.string.month10);
	        }else if(n_month==11){
	        	sMonth = getString(R.string.month11);
	        }else if(n_month==12){
	        	sMonth = getString(R.string.month12);
	        }
	        
	        strShow_date = mDay+"  "+ sMonth +"  "+mYear;
	        
	        //mPickDate.setText(strShow_date);
	        */
	    }

		//the callback received when the user "sets" the date in the dialog
		private DatePickerDialog.OnDateSetListener mDateSetListener =
	        new DatePickerDialog.OnDateSetListener() {

	            public void onDateSet(DatePicker view, int year, 
	                                  int monthOfYear, int dayOfMonth) {
	                mYear = year;
	                mMonth = monthOfYear;
	                mDay = dayOfMonth;
	                updateDisplay();
	            }
	        };
	        
	        
    
    
    public String getDeviceID(){
    	
    	  String str_device = null;
			try{
	        	TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
	        	str_device= telephonyManager.getDeviceId();
	        } catch (Exception e) {
				e.printStackTrace();
			}
	        try{
				if(str_device==null || str_device.equals("")){
					WifiManager wifiMan = (WifiManager)this.getSystemService(Context.WIFI_SERVICE);
		    		WifiInfo wifiInf = wifiMan.getConnectionInfo();
		    		str_device = wifiInf.getMacAddress();
				}
	    	} catch (Exception e) {
				e.printStackTrace();
			}
		return str_device;
	}
	        
    public boolean checkInternet() {
    	ConnectivityManager connec =  ( ConnectivityManager ) getSystemService( Context.CONNECTIVITY_SERVICE ); 
    	if ( connec.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTED || connec.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTED ){
    	     // connect 
    		return true;
    	}else{
    	     // not-connect 
    		return false;
    	}
    }        
	        
    
    /**
     * Setup cover flow.
     * 
     * @param mCoverFlow
     *            the m cover flow
     * @param reflect
     *            the reflect
     */
    private void setupCoverFlow(final CoverFlowPlus mCoverFlow, final boolean reflect) {
        BaseAdapter coverImageAdapter;
        
        MenuPlusListData menuListData = new MenuPlusListData(this);
        List<MenuList> listMenuList = new ArrayList<MenuList>();
        
        
        try{
        	listMenuList =  menuListData.query(null, null);
        }catch (Exception e) {
        	e.printStackTrace();
		}
        
        
//      URLYYY = listMenuList.get(0).getMenuTypeUrl();
        URLYYY = "http://www.google.com";
        
          
        if(listMenuList != null){
        	System.out.println("__----- listMenuList != null");
        	if(listMenuList.size()>0){
        		System.out.println("----- listMenuList > 0");
        		
        			if (reflect) {
        						coverImageAdapter = new ReflectingImagePlus(new ResourceImagePlus(this,listMenuList));
        			} else {
        						coverImageAdapter = new ResourceImagePlus(this,listMenuList);
        			}
        			
        				mCoverFlow.setAdapter(coverImageAdapter);
        				mCoverFlow.setSelection(2, true);
        				setupListeners(mCoverFlow);
        				
        	}else{		System.out.println("listMenuList < 0");	}
        	}else{		System.out.println("listMenuList = null");	}
    
    }
    /**
     * Sets the up listeners.
     * 
     * @param mCoverFlow
     *            the new up listeners
     */
    
    
	
    private void setupListeners(final CoverFlowPlus mCoverFlow) {
        mCoverFlow.setOnItemClickListener(new OnItemClickListener() {
            
            public void onItemClick(final AdapterView< ? > parent, final View view, final int position, final long id) {
                textView.setText("Item clicked! : " + id);
				
                if(id > 3){
                	if(listMenuList != null){
						if(listMenuList.size() > 0){
							insertDecorView(TabHoroPlusActivityGroup.mLocalActivityManager.startActivity("webviewplus", 
		    						new Intent(HoroPlusMenuActivity.this,WebViewPlusActivity.class)
		    					.putExtra("URLY", listMenuList.get((int)id).getMenuTypeUrl()) //  i'm not sure
		    					.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
		    					)
		    					.getDecorView());
						}
					}
					
                }else{
                	Intent gol = new Intent(HoroPlusMenuActivity.this,TabPlusMenuActivity.class);
                	gol.putExtra("IDTAB", (int)(id+1));
                	gol.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                	startActivity(gol);
                }  
                
            }

        });
        mCoverFlow.setOnItemSelectedListener(new OnItemSelectedListener() {
            
            public void onItemSelected(final AdapterView< ? > parent, final View view, final int position, final long id) {
                textView.setText("Item selected! : " + id);
            }

            
            public void onNothingSelected(final AdapterView< ? > parent) {
                textView.setText("Nothing clicked!");
            }
        });
    }
    
    
    private void insertDecorView(View view) {
     	 
    	getParent().setContentView(view, new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
    }
    
    
}
