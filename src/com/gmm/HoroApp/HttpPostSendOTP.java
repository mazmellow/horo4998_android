package com.gmm.HoroApp;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

public class HttpPostSendOTP {
	
       
    public static List<StatusPost> executeHttpPost(String str_deviceID, String str_msisdn) throws Exception {
    	
    	System.out.println(" ---- HttpPostSendOTP ---- "+" str_msisdn :"+str_msisdn
    			+" str_deviceID :"+str_deviceID);
    	
    	
    	BufferedReader in = null;
    	String result = null;
		try {
			
			HttpClient client = new DefaultHttpClient();
			HttpPost request = new HttpPost(
					
			"http://hvpiphone.gmember.com/gmmhoroscope/api/sendOTP.jsp");			
			
			
			List<NameValuePair> postParameters = new ArrayList<NameValuePair>();
			
			postParameters.add(new BasicNameValuePair("APP_ID", HoroscopeActivity.App_ID));	
			postParameters.add(new BasicNameValuePair("DEVICE", str_deviceID));
			
			
			postParameters.add(new BasicNameValuePair("MSISDN", str_msisdn));
			
			postParameters.add(new BasicNameValuePair("APPVERSION", HoroscopeActivity.AppVersion));	
			postParameters.add(new BasicNameValuePair("APIVERSION", HoroscopeActivity.ApiVersion));
			
//			System.out.println("APP_ID = "+HoroscopeActivity.App_ID);
//			System.out.println("DEVICE = "+str_deviceID);
//			System.out.println("MSISDN = "+str_msisdn);
//			System.out.println("APPVERSION = "+HoroscopeActivity.AppVersion);
//			System.out.println("APP_ID = "+HoroscopeActivity.ApiVersion);
			
			
			UrlEncodedFormEntity formEntity = new UrlEncodedFormEntity(postParameters);
			request.setHeader("User-Agent", "Android "+android.os.Build.VERSION.RELEASE+"/"+android.os.Build.MODEL);
			request.setEntity(formEntity);
			HttpResponse response = client.execute(request);
			
			
			in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			
			StringBuffer sb = new StringBuffer("");
			String line = "";
			String NL = System.getProperty("line.separator");
			while ((line = in.readLine()) != null) {
				sb.append(line + NL);
			}
			
			in.close();
			
			result = sb.toString();
			
			System.out.println(" XML HttpPostSendOTP :"+result);
			
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		
		//XMLStatusPostHandler
		SAXParser parser = SAXParserFactory.newInstance().newSAXParser();
    	XMLReader xr = parser.getXMLReader();
    	XMLStatusPostHandler mhandler = new XMLStatusPostHandler();
    	xr.setContentHandler(mhandler);
    	xr.parse(new InputSource(new StringReader(result)));
    	return mhandler.getStatusPostLists();
	}

}
