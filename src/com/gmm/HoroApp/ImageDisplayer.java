package com.gmm.HoroApp;

import com.gmm.HoroApp.R;

import android.graphics.Bitmap;
import android.widget.ImageView;


public class ImageDisplayer implements Runnable {
	
	public ImageView view;
	public Bitmap bmp;


	public ImageDisplayer(ImageView imageView, Bitmap bmp)
	{
		
		this.view = imageView;
		this.bmp = bmp;

	}

	public void run()
	{
		
		if (bmp != null) {
			
			view.setImageBitmap(bmp);
		}
		else {
			
			view.setImageResource(R.drawable.cover);
		}
		
	}

}
