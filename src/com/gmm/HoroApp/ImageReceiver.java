package com.gmm.HoroApp;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

import android.graphics.BitmapFactory;
import android.widget.ImageView;


public class ImageReceiver extends Thread{
	
	String url;
	ImageReceivedCallback callback;
	ImageView view;

	public ImageReceiver(String url, ImageReceivedCallback callback, ImageView view)
	{
		this.url = url;
		this.callback = callback;
		this.view = view;
		start();
	}


	@Override
	public void run()
	{
		ImageDisplayer displayer=null;
		try
		{
			HttpURLConnection conn = (HttpURLConnection)(new URL(url)).openConnection();
			conn.connect();
			//if (conn.getContentType().regionMatches(true,0,"image",0, 5)) {
				displayer = new ImageDisplayer(view, BitmapFactory.decodeStream(conn.getInputStream()));
				/*
			}
			else {
				System.out.println("else  connect url cover");
				displayer = new ImageDisplayer(view, null);
			}*/
			
		}
		catch (IOException e)
		{
			
			e.printStackTrace();
			displayer = new ImageDisplayer(view, null);
		}
		finally {
			callback.onImageReceived(displayer);
		}
	}

}
