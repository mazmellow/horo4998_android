package com.gmm.HoroApp;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import kankan.wheel.widget.OnWheelChangedListener;
import kankan.wheel.widget.WheelView;
import kankan.wheel.widget.adapters.ArrayWheelAdapter;
import kankan.wheel.widget.adapters.NumericWheelAdapter;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

public class LoveMeterActivity extends Activity{
	
	//go to LoveMeterDetailActivity
	
	private ImageView imgbanner;
	private ImageView imgResult;
	private EditText edtMyName, edtYourName;
	private Button btnMyDay, btnMyMonth, btnMyYear, btnYourDay, btnYourMonth, btnYourYear;
	
//	static String myName, yourName;
	
	static final int DATE_DIALOG_ID = 0;
	
	private int mYear;
    private int mMonth;
    private int mDay;
    int n_month;
    
    int value_day1;
	int value_month1;
	int value_day2;
	int value_month2;
    
	static String sMyName;
    static String sYourName;
    int press_butt_date;
    
    String sMonth;
    
//    String strShowMyDate = null;
//    String strShowYourDate = null;
//    String BirthDate;
    
    List<Profile> Listprofile;
    ProfileData profiledata = new ProfileData(this);
	
    AlertDialog alertDialog_datepicker;
	
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lovemeter);
        
        System.out.println("----------- LoveMeterActivity ----------");
        
//		//============ costom Date picker ======== 
//        LinearLayout ly = new LinearLayout(this);
//        ly.setLayoutParams(new LayoutParams(180, 180));
//        
//        Button btn = new Button(this);
//        
//        ly.addView(btn);
        
		 
		Listprofile = new ArrayList<Profile>();
		Listprofile = profiledata.query(null, null);
        
		imgbanner = (ImageView)findViewById(R.id.imglovemeterbanner);
        edtMyName = (EditText)findViewById(R.id.edtmyname);
        btnMyDay = (Button)findViewById(R.id.btnmyday);
        btnMyMonth = (Button)findViewById(R.id.btnmymonth);
        btnMyYear = (Button)findViewById(R.id.btnmyyear);
        
        edtYourName = (EditText)findViewById(R.id.edtyourname);
        btnYourDay = (Button)findViewById(R.id.btnyourday);
        btnYourMonth = (Button)findViewById(R.id.btnyourmonth);
        btnYourYear = (Button)findViewById(R.id.btnyouryear);
        
        imgResult = (ImageView)findViewById(R.id.imgresult);
        
        
        if(HomeActivity.bitmapBanner != null){
        	imgbanner.setImageBitmap(HomeActivity.bitmapBanner);
        }
        
        edtMyName.setOnEditorActionListener(new OnEditorActionListener() {
			
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				//Hide keyBoard
                InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                in.hideSoftInputFromWindow(edtMyName.getApplicationWindowToken(),InputMethodManager.HIDE_NOT_ALWAYS);

                //Set KeyWord
//                myName = null;
//                myName = edtMyName.getText().toString().trim();
//                
//                Log.log("MyName : "+ myName);
            	
                
				return false;
			}
		});
        
        edtYourName.setOnEditorActionListener(new OnEditorActionListener() {
			
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				// Hide KeyBoard
				InputMethodManager in = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
				in.hideSoftInputFromInputMethod(edtYourName.getApplicationWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
				
				//Set KeyWord
//				yourName = null;
//				yourName = edtYourName.getText().toString().trim();
//				
//				Log.log("YourName : " + yourName);
				return false;
			}
		});
        
        btnMyDay.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				press_butt_date=1;
				try {
					Log.log("---- btnMyDate onClick -----");
					popupDatePicker();
//					showDialog(DATE_DIALOG_ID);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
        btnMyMonth.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				press_butt_date=1;
				try {
					Log.log("---- btnMyDate onClick -----");
					popupDatePicker();
					//showDialog(DATE_DIALOG_ID);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
        
        btnMyYear.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				press_butt_date=1;
				try {
					Log.log("---- btnMyDate onClick -----");
					popupDatePicker();
					//showDialog(DATE_DIALOG_ID);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
        
        btnYourDay.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				press_butt_date=2;
				try {
					popupDatePicker();
					//showDialog(DATE_DIALOG_ID);
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
		});
        
        btnYourMonth.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				press_butt_date=2;
				try {
					popupDatePicker();
					//showDialog(DATE_DIALOG_ID);
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
		});
        
        btnYourYear.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				press_butt_date=2;
				try {
					popupDatePicker();
					//showDialog(DATE_DIALOG_ID);
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
		});
        
        imgResult.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				
				sMyName = edtMyName.getText().toString().trim();
				sYourName = edtYourName.getText().toString().trim();
				
				if((sMyName == null || sMyName.equals(""))){
					showNoticeDialogBox(getString(R.string.regist_title), getString(R.string.inputmyname));
				}else if(sYourName == null || sYourName.equals("")){
					showNoticeDialogBox(getString(R.string.regist_title), getString(R.string.inputyourname));
				}else if(value_day1 == 0){
					showNoticeDialogBox(getString(R.string.regist_title), getString(R.string.inputmydate));
				}else if(value_day2 == 0){
					showNoticeDialogBox(getString(R.string.regist_title), getString(R.string.inputyourdate));
				}else{
					
					String str_numLover = "text" + (new Integer(getNumberLove(value_day1,value_month1)).toString()) +"_"+ (new Integer(getNumberLove(value_day2,value_month2)).toString());
					
					insertDecorView(TabHoroLoveMeterActivity.mLocalActivityManager.startActivity("lovemeterdetail", new Intent(LoveMeterActivity.this,LoveMeterDetailActivity.class)
                	.putExtra("str_numLover", str_numLover)
                	.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)).getDecorView());
                	
				}
			}
		});
        
     // get the current date
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        // display the current date
        getQueryProfile();
        updateDisplay();

        
	}
    
//    private void setDate(int n){
//    	press_butt_date = n;
//    	try {
//			showDialog(DATE_DIALOG_ID);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//    }
	
	private void insertDecorView(View view) {
	   	 
    	getParent().setContentView(view, new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
    }

	
	protected Dialog onCreateDialog(int id) {
		
		Calendar c = Calendar.getInstance();
    	int cyear = c.get(Calendar.YEAR);
    	int cmonth = c.get(Calendar.MONTH);
    	int cday = c.get(Calendar.DAY_OF_MONTH);
		
		switch (id) {
        case DATE_DIALOG_ID:
            return new DatePickerDialog(getParent(),mDateSetListener,mYear, mMonth, mDay);
        }
        return null;
	}

	//the callback received when the user "sets" the date in the dialog
	private DatePickerDialog.OnDateSetListener mDateSetListener =
        new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                mYear = year;
                mMonth = monthOfYear;
                mDay = dayOfMonth;
                updateDisplay();
            }
        };
	
     // updates the date we display in the TextView
     private void updateDisplay() {
        	
        	Log.log("---updateDisplay");
        	
            Log.log("mYear :"+mYear);
            Log.log("mMonth :"+mMonth);
            Log.log("mDay :"+mDay);
            Log.log("press_butt_date :"+ press_butt_date);
            
       
            if(press_butt_date == 1){
            	
            	Log.log("if press_butt_date == 1");
            	Log.log(mYear+"-"+pad(n_month)+"-"+pad(mDay));
            	
            	n_month = mMonth+1;
    	        value_day1 = mDay;
    	        value_month1 = n_month;
    	        
    	    //    getUpdateProfile(Listprofile.get(0).getMSISDN(), mYear+"-"+pad(n_month)+"-"+pad(mDay));
    	        
    	        Log.log("value_day1 :"+value_day1+"_____ value_month1 :"+value_month1);
    	        
            }else if(press_butt_date ==2){
            	
            	n_month = mMonth+1;
            	value_day2 = mDay;
    	        value_month2 = n_month;
    	        
    	        Log.log("value_day2 :"+value_day2+"_____ value_month2 :"+value_month2);
            }
            
            else if(press_butt_date == 3){
            	n_month = mMonth;
    	        value_day1 = mDay;
    	        value_month1 = mMonth;
            }
            
            
            if(n_month==1){
            	sMonth = getString(R.string.month1);
            }else if(n_month==2){
            	sMonth = getString(R.string.month2);
            }else if(n_month==3){
            	sMonth = getString(R.string.month3);
            }else if(n_month==4){
            	sMonth = getString(R.string.month4);
            }else if(n_month==5){
            	sMonth = getString(R.string.month5);
            }else if(n_month==6){
            	sMonth = getString(R.string.month6);
            }else if(n_month==7){
            	sMonth = getString(R.string.month7);
            }else if(n_month==8){
            	sMonth = getString(R.string.month8);
            }else if(n_month==9){
            	sMonth = getString(R.string.month9);
            }else if(n_month==10){
            	sMonth = getString(R.string.month10);
            }else if(n_month==11){
            	sMonth = getString(R.string.month11);
            }else if(n_month==12){
            	sMonth = getString(R.string.month12);
            }
            
            
            if(press_butt_date == 1){
            	
    	        btnMyDay.setText(mDay+"");
    	        btnMyMonth.setText(sMonth+"");
    	        btnMyYear.setText(mYear+"");
    	        
    	        
            }else if(press_butt_date == 2){
            	
    	        btnYourDay.setText(mDay+"");
    	        Log.log("1");
    	        btnYourMonth.setText(sMonth+"");
    	        Log.log("2");
    	        btnYourYear.setText(mYear+"");
    	        Log.log("3");
            }else if(press_butt_date == 3){
            	
            	btnMyDay.setText(mDay+"");
    	        btnMyMonth.setText(sMonth+"");
    	        btnMyYear.setText(mYear+"");

            }
            
            
    }    
	
    public int getNumberLove(int dayy, int monthh){
    	
    	int num = (monthh*100) + dayy;
    	int number = 0;
    	
    	if (num>=321 && num<=420) {
    		number= 1;
    	}
    	if (num>=421 && num<=521) {
    		number= 2;
    	}
    	if (num>=522 && num<=621) {
    		number= 3;
    	}
    	if (num>=622 && num<=722) {
    		number= 4;
    	}
    	if (num>=723 && num<=822) {
    		number= 5;
    	}
    	if (num>=823 && num<=923) {
    		number= 6;
    	}
    	if (num>=924 && num<=1023) {
    		number= 7;
    	}
    	if (num>=1024 && num<=1122) {
    		number= 8;
    	}
    	if (num>=1123 && num<=1222) {
    		number= 9;
    	}
    	if (num>=1223 || num<=120) {
    		number= 10;
    	}
    	if (num>=121 && num<=219) {
    		number= 11;
    	}
    	if (num>=220 && num<=320) {
    		number= 12;
    	}
    	
    	return number;
    	
    }
	
	private void showNoticeDialogBox(final String title, final String message) {
    	
    	Log.log("---- showNoticeDialogBox");
        Builder setupAlert;
        setupAlert = new AlertDialog.Builder(getParent());    
        setupAlert.setTitle(title);
        setupAlert.setMessage(message);
        setupAlert.setPositiveButton("Ok",
            new DialogInterface.OnClickListener() {
              public void onClick(DialogInterface dialog, int whichButton) {
            	 // HoroApp.this.edtUsername.setText("");
            	 // HoroApp.this.edtPassword.setText("");
              }
            });
        setupAlert.show();
      }
	
	 public void getUpdateProfile(String str_msisdn, String str_birthday){
	    	Log.log("-------getUpdateProfile");
	    	
	    	if(!(str_msisdn.equals(""))){
	    		str_msisdn = "66"+str_msisdn.substring(2);
	    	}
	    	
	    	Profile profile = new Profile();
	    	profile.setMSISDN(str_msisdn);
	    	profile.setFlagVerified("no");
	    	profile.setBirthdate(str_birthday);
	    	
	    	try{
	        	
	        	ProfileData profileData = new ProfileData(this);
	        	profileData.delete(null, null);
	        	profileData.insert(profile);
	        	
	        } catch (Exception e) {
				e.printStackTrace();
			}
	    	
 	 }
	 
	 
	 
	 
	 public void getQueryProfile(){
		 Log.log("-------getQueryProfile");
		 String ssMonth = "";

		 
		 press_butt_date = 3;
		 
		 if(Listprofile != null){
			 if(Listprofile.size() > 0){
				 if(Listprofile.get(0).getBirthdate().length() >= 10){
					 mYear = Integer.parseInt(Listprofile.get(0).getBirthdate().substring(0, 4));
					 mMonth = Integer.parseInt(Listprofile.get(0).getBirthdate().substring(5, 7));
					 mDay = Integer.parseInt(Listprofile.get(0).getBirthdate().substring(8,10));
					 
					 if(mMonth==1){
						 ssMonth = getString(R.string.month1);
			         }else if(n_month==2){
			        	 ssMonth = getString(R.string.month2);
			         }else if(n_month==3){
			        	 ssMonth = getString(R.string.month3);
			         }else if(n_month==4){
			        	 ssMonth = getString(R.string.month4);
			         }else if(n_month==5){
			        	 ssMonth = getString(R.string.month5);
			         }else if(n_month==6){
			        	 ssMonth = getString(R.string.month6);
			         }else if(n_month==7){
			        	 ssMonth = getString(R.string.month7);
			         }else if(n_month==8){
			        	 ssMonth = getString(R.string.month8);
			         }else if(n_month==9){
			        	 ssMonth = getString(R.string.month9);
			         }else if(n_month==10){
			        	 ssMonth = getString(R.string.month10);
			         }else if(n_month==11){
			        	 ssMonth = getString(R.string.month11);
			         }else if(n_month==12){
			        	 ssMonth = getString(R.string.month12);
			         }
				 }
				 
			 }
		 } 
		 
	 }
	 
	 private static String pad(int c){
			if(c >= 10)
				return String.valueOf(c);
			else
				return "0" + String.valueOf(c);
	 }
	 
	 public void popupDatePicker(){
		 try {
			AlertDialog.Builder builder;
			LayoutInflater inflater = (LayoutInflater) getParent().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View layout = inflater.inflate(R.layout.date_layout, (ViewGroup) findViewById(R.id.date_layout));
			
			
			
			Calendar calendar = Calendar.getInstance();

	        final WheelView month = (WheelView)layout.findViewById(R.id.month);
	        final WheelView year = (WheelView)layout.findViewById(R.id.year);
	        final WheelView day = (WheelView)layout.findViewById(R.id.day);
	        final Button btnset = (Button)layout.findViewById(R.id.btnset);
	        final Button btncancel = (Button)layout.findViewById(R.id.btncancel);
	        
	        
	        btnset.setOnClickListener(new OnClickListener() {
				
				public void onClick(View v) {
					System.out.println("day "+day.getCurrentItem());
					System.out.println("month "+month.getCurrentItem()+1);
					System.out.println("year "+year.getCurrentItem());
					
					mDay = day.getCurrentItem() + 1;
	                mMonth = month.getCurrentItem();
	                mYear = year.getCurrentItem() + 2455;
	                updateDisplay();
	                alertDialog_datepicker.dismiss();
				}
			});
	        
	        btncancel.setOnClickListener(new OnClickListener(){
	        	public void onClick(View v){
	        		alertDialog_datepicker.dismiss();
	        	}
	        });
	        
	        
	        OnWheelChangedListener listener = new OnWheelChangedListener() {
	            public void onChanged(WheelView wheel, int oldValue, int newValue) {
	                updateDays(year, month, day);
	            }
	        };

	        // month
	        int curMonth = calendar.get(Calendar.MONTH);
	        String months[] = new String[] {
	        							getString(R.string.month1), 
	        							getString(R.string.month2), 
	        							getString(R.string.month3), 
	        							getString(R.string.month4), 
	        							getString(R.string.month5),
	        							getString(R.string.month6), 
	        							getString(R.string.month7), 
	        							getString(R.string.month8), 
	        							getString(R.string.month9), 
	        							getString(R.string.month10), 
	        							getString(R.string.month11), 
	        							getString(R.string.month12)
	        							};
	        
	        month.setViewAdapter(new DateArrayAdapter(getParent(), months, curMonth));
	        month.setCurrentItem(curMonth);
	        month.addChangingListener(listener);
	    
	        // year
	        int curYear = calendar.get(Calendar.YEAR);
	        year.setViewAdapter(new DateNumericAdapter(getParent(), curYear + 543 - 100, curYear + 543, 0));
	        year.setCurrentItem(50);
	        year.addChangingListener(listener);
	        
	        //day
	        updateDays(year, month, day);
	        day.setCurrentItem(calendar.get(Calendar.DAY_OF_MONTH) - 1);
			
			
			
			
			
			
			builder = new AlertDialog.Builder(getParent()).setCancelable(false);
            builder.setView(layout);
            builder.setInverseBackgroundForced(true);
            alertDialog_datepicker = builder.create();
            alertDialog_datepicker.show();
			
		} catch (Exception e) {
		}
	 }
	 
	 void updateDays(WheelView year, WheelView month, WheelView day) {
	        Calendar calendar = Calendar.getInstance();
	        calendar.set(Calendar.YEAR, calendar.get(Calendar.YEAR) + year.getCurrentItem());
	        calendar.set(Calendar.MONTH, month.getCurrentItem());
	        
	        int maxDays = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
	        day.setViewAdapter(new DateNumericAdapter(getParent(), 1, maxDays, calendar.get(Calendar.DAY_OF_MONTH) - 1));
	        int curDay = Math.min(maxDays, day.getCurrentItem() + 1);
	        day.setCurrentItem(curDay - 1, true);
	    }
	 
	    private class DateNumericAdapter extends NumericWheelAdapter {
	        // Index of current item
	        int currentItem;
	        // Index of item to be highlighted
	        int currentValue;
	        
	        /**
	         * Constructor
	         */
	        public DateNumericAdapter(Context context, int minValue, int maxValue, int current) {
	            super(context, minValue, maxValue);
	            this.currentValue = current;
	            setTextSize(16);
	        }
	        
	        @Override
	        protected void configureTextView(TextView view) {
	            super.configureTextView(view);
	            if (currentItem == currentValue) {
//	                view.setTextColor(0xFF0000F0);
	            }
	            view.setTypeface(Typeface.SANS_SERIF);
	        }
	        
	        @Override
	        public View getItem(int index, View cachedView, ViewGroup parent) {
	            currentItem = index;
	            return super.getItem(index, cachedView, parent);
	        }
	    }
	    
	    private class DateArrayAdapter extends ArrayWheelAdapter<String> {
	        // Index of current item
	        int currentItem;
	        // Index of item to be highlighted
	        int currentValue;
	        
	        /**
	         * Constructor
	         */
	        public DateArrayAdapter(Context context, String[] items, int current) {
	            super(context, items);
	            this.currentValue = current;
	            setTextSize(16);
	        }
	        
	        @Override
	        protected void configureTextView(TextView view) {
	            super.configureTextView(view);
	            if (currentItem == currentValue) {
//	                view.setTextColor(0xFF0000F0);
	            }
	            view.setTypeface(Typeface.SANS_SERIF);
	        }
	        
	        @Override
	        public View getItem(int index, View cachedView, ViewGroup parent) {
	            currentItem = index;
	            return super.getItem(index, cachedView, parent);
	        }
	    }
}
