package com.gmm.HoroApp;

import com.gmm.HoroApp.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class LoveMeterDetailActivity extends Activity{
	
	String str_numLover;
	
	TextView txtHeader;
	TextView txtResultDetail;
	TextView txtPercent;
	ImageView btnShareFacebook;
	
	String textShowLover;
	
	
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lovemeter_detail);
        
        System.out.println("----------- LoveMeterDetailActivity ----------");
        
        txtHeader = (TextView)findViewById(R.id.txtlovedetailheader);
        txtPercent = (TextView)findViewById(R.id.txtpercent);
        txtResultDetail = (TextView)findViewById(R.id.txtresultdetail);
        btnShareFacebook = (ImageView)findViewById(R.id.img_sharefacebook);
        
        Bundle d = getIntent().getExtras();
        if (d != null) {
        	str_numLover = d.getString("str_numLover");
        	Log.log(str_numLover);
        }

        btnShareFacebook.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				String textid = str_numLover.replace("text", "");
				String sharefacebook = "https://www.facebook.com/sharer/sharer.php?u=http://4998horo.gmember.com/share/match/"+ textid +"/"+ LoveMeterActivity.sMyName +"/" + LoveMeterActivity.sYourName;
				Intent in = new Intent(LoveMeterDetailActivity.this, WebFullScreenActivity.class);
				in.putExtra("URL_webview", sharefacebook);
				startActivity(in); 
			}
		});
        
        //https://www.facebook.com/sharer/sharer.php?u=http://4998horo.gmember.com/share/match/10_10/A/S
        
        
        txtHeader.setText(getString(R.string.you) + LoveMeterActivity.sMyName + getString(R.string.and) + getString(R.string.you) + LoveMeterActivity.sYourName);
        
        
        if(str_numLover.equals("text1_1")){
        	textShowLover = getString(R.string.text1_1);
        }else if(str_numLover.equals("text1_2")){
        	textShowLover = getString(R.string.text1_2);
        }else if(str_numLover.equals("text1_3")){
        	textShowLover = getString(R.string.text1_3);
        }else if(str_numLover.equals("text1_4")){
        	textShowLover = getString(R.string.text1_4);
        }else if(str_numLover.equals("text1_5")){
        	textShowLover = getString(R.string.text1_5);
        }else if(str_numLover.equals("text1_6")){
        	textShowLover = getString(R.string.text1_6);
        }else if(str_numLover.equals("text1_7")){
        	textShowLover = getString(R.string.text1_7);
        }else if(str_numLover.equals("text1_8")){
        	textShowLover = getString(R.string.text1_8);
        }else if(str_numLover.equals("text1_9")){
        	textShowLover = getString(R.string.text1_9);
        }else if(str_numLover.equals("text1_10")){
        	textShowLover = getString(R.string.text1_10);
        }else if(str_numLover.equals("text1_11")){
        	textShowLover = getString(R.string.text1_11);
        }else if(str_numLover.equals("text1_12")){
        	textShowLover = getString(R.string.text1_12);
        	
        }else if(str_numLover.equals("text2_1")){
        	textShowLover = getString(R.string.text2_1);
        }else if(str_numLover.equals("text2_2")){
        	textShowLover = getString(R.string.text2_2);
        }else if(str_numLover.equals("text2_3")){
        	textShowLover = getString(R.string.text2_3);
        }else if(str_numLover.equals("text2_4")){
        	textShowLover = getString(R.string.text2_4);
        }else if(str_numLover.equals("text2_5")){
        	textShowLover = getString(R.string.text2_5);
        }else if(str_numLover.equals("text2_6")){
        	textShowLover = getString(R.string.text2_6);
        }else if(str_numLover.equals("text2_7")){
        	textShowLover = getString(R.string.text2_7);
        }else if(str_numLover.equals("text2_8")){
        	textShowLover = getString(R.string.text2_8);
        }else if(str_numLover.equals("text2_9")){
        	textShowLover = getString(R.string.text2_9);
        }else if(str_numLover.equals("text2_10")){
        	textShowLover = getString(R.string.text2_10);
        }else if(str_numLover.equals("text2_11")){
        	textShowLover = getString(R.string.text2_11);
        }else if(str_numLover.equals("text2_12")){
        	textShowLover = getString(R.string.text2_12);
        	
        }else if(str_numLover.equals("text3_1")){
        	textShowLover = getString(R.string.text3_1);
        }else if(str_numLover.equals("text3_2")){
        	textShowLover = getString(R.string.text3_2);
        }else if(str_numLover.equals("text3_3")){
        	textShowLover = getString(R.string.text3_3);
        }else if(str_numLover.equals("text3_4")){
        	textShowLover = getString(R.string.text3_4);
        }else if(str_numLover.equals("text3_5")){
        	textShowLover = getString(R.string.text3_5);
        }else if(str_numLover.equals("text3_6")){
        	textShowLover = getString(R.string.text3_6);
        }else if(str_numLover.equals("text3_7")){
        	textShowLover = getString(R.string.text3_7);
        }else if(str_numLover.equals("text3_8")){
        	textShowLover = getString(R.string.text3_8);
        }else if(str_numLover.equals("text3_9")){
        	textShowLover = getString(R.string.text3_9);
        }else if(str_numLover.equals("text3_10")){
        	textShowLover = getString(R.string.text3_10);
        }else if(str_numLover.equals("text3_11")){
        	textShowLover = getString(R.string.text3_11);
        }else if(str_numLover.equals("text3_12")){
        	textShowLover = getString(R.string.text3_12);
        	
        }else if(str_numLover.equals("text4_1")){
        	textShowLover = getString(R.string.text4_1);
        }else if(str_numLover.equals("text4_2")){
        	textShowLover = getString(R.string.text4_2);
        }else if(str_numLover.equals("text4_3")){
        	textShowLover = getString(R.string.text4_3);
        }else if(str_numLover.equals("text4_4")){
        	textShowLover = getString(R.string.text4_4);
        }else if(str_numLover.equals("text4_5")){
        	textShowLover = getString(R.string.text4_5);
        }else if(str_numLover.equals("text4_6")){
        	textShowLover = getString(R.string.text4_6);
        }else if(str_numLover.equals("text4_7")){
        	textShowLover = getString(R.string.text4_7);
        }else if(str_numLover.equals("text4_8")){
        	textShowLover = getString(R.string.text4_8);
        }else if(str_numLover.equals("text4_9")){
        	textShowLover = getString(R.string.text4_9);
        }else if(str_numLover.equals("text4_10")){
        	textShowLover = getString(R.string.text4_10);
        }else if(str_numLover.equals("text4_11")){
        	textShowLover = getString(R.string.text4_11);
        }else if(str_numLover.equals("text4_12")){
        	textShowLover = getString(R.string.text4_12);
        	
        }else if(str_numLover.equals("text5_1")){
        	textShowLover = getString(R.string.text5_1);
        }else if(str_numLover.equals("text5_2")){
        	textShowLover = getString(R.string.text5_2);
        }else if(str_numLover.equals("text5_3")){
        	textShowLover = getString(R.string.text5_3);
        }else if(str_numLover.equals("text5_4")){
        	textShowLover = getString(R.string.text5_4);
        }else if(str_numLover.equals("text5_5")){
        	textShowLover = getString(R.string.text5_5);
        }else if(str_numLover.equals("text5_6")){
        	textShowLover = getString(R.string.text5_6);
        }else if(str_numLover.equals("text5_7")){
        	textShowLover = getString(R.string.text5_7);
        }else if(str_numLover.equals("text5_8")){
        	textShowLover = getString(R.string.text5_8);
        }else if(str_numLover.equals("text5_9")){
        	textShowLover = getString(R.string.text5_9);
        }else if(str_numLover.equals("text5_10")){
        	textShowLover = getString(R.string.text5_10);
        }else if(str_numLover.equals("text5_11")){
        	textShowLover = getString(R.string.text5_11);
        }else if(str_numLover.equals("text5_12")){
        	textShowLover = getString(R.string.text5_12);
        	
        }else if(str_numLover.equals("text6_1")){
        	textShowLover = getString(R.string.text6_1);
        }else if(str_numLover.equals("text6_2")){
        	textShowLover = getString(R.string.text6_2);
        }else if(str_numLover.equals("text6_3")){
        	textShowLover = getString(R.string.text6_3);
        }else if(str_numLover.equals("text6_4")){
        	textShowLover = getString(R.string.text6_4);
        }else if(str_numLover.equals("text6_5")){
        	textShowLover = getString(R.string.text6_5);
        }else if(str_numLover.equals("text6_6")){
        	textShowLover = getString(R.string.text6_6);
        }else if(str_numLover.equals("text6_7")){
        	textShowLover = getString(R.string.text6_7);
        }else if(str_numLover.equals("text6_8")){
        	textShowLover = getString(R.string.text6_8);
        }else if(str_numLover.equals("text6_9")){
        	textShowLover = getString(R.string.text6_9);
        }else if(str_numLover.equals("text6_10")){
        	textShowLover = getString(R.string.text6_10);
        }else if(str_numLover.equals("text6_11")){
        	textShowLover = getString(R.string.text6_11);
        }else if(str_numLover.equals("text6_12")){
        	textShowLover = getString(R.string.text6_12);
        	
        }else if(str_numLover.equals("text7_1")){
        	textShowLover = getString(R.string.text7_1);
        }else if(str_numLover.equals("text7_2")){
        	textShowLover = getString(R.string.text7_2);
        }else if(str_numLover.equals("text7_3")){
        	textShowLover = getString(R.string.text7_3);
        }else if(str_numLover.equals("text7_4")){
        	textShowLover = getString(R.string.text7_4);
        }else if(str_numLover.equals("text7_5")){
        	textShowLover = getString(R.string.text7_5);
        }else if(str_numLover.equals("text7_6")){
        	textShowLover = getString(R.string.text7_6);
        }else if(str_numLover.equals("text7_7")){
        	textShowLover = getString(R.string.text7_7);
        }else if(str_numLover.equals("text7_8")){
        	textShowLover = getString(R.string.text7_8);
        }else if(str_numLover.equals("text7_9")){
        	textShowLover = getString(R.string.text7_9);
        }else if(str_numLover.equals("text7_10")){
        	textShowLover = getString(R.string.text7_10);
        }else if(str_numLover.equals("text7_11")){
        	textShowLover = getString(R.string.text7_11);
        }else if(str_numLover.equals("text7_12")){
        	textShowLover = getString(R.string.text7_12);
        	
        }else if(str_numLover.equals("text8_1")){
        	textShowLover = getString(R.string.text8_1);
        }else if(str_numLover.equals("text8_2")){
        	textShowLover = getString(R.string.text8_2);
        }else if(str_numLover.equals("text8_3")){
        	textShowLover = getString(R.string.text8_3);
        }else if(str_numLover.equals("text8_4")){
        	textShowLover = getString(R.string.text8_4);
        }else if(str_numLover.equals("text8_5")){
        	textShowLover = getString(R.string.text8_5);
        }else if(str_numLover.equals("text8_6")){
        	textShowLover = getString(R.string.text8_6);
        }else if(str_numLover.equals("text8_7")){
        	textShowLover = getString(R.string.text8_7);
        }else if(str_numLover.equals("text8_8")){
        	textShowLover = getString(R.string.text8_8);
        }else if(str_numLover.equals("text8_9")){
        	textShowLover = getString(R.string.text8_9);
        }else if(str_numLover.equals("text8_10")){
        	textShowLover = getString(R.string.text8_10);
        }else if(str_numLover.equals("text8_11")){
        	textShowLover = getString(R.string.text8_11);
        }else if(str_numLover.equals("text8_12")){
        	textShowLover = getString(R.string.text8_12);
        	
        }else if(str_numLover.equals("text9_1")){
        	textShowLover = getString(R.string.text9_1);
        }else if(str_numLover.equals("text9_2")){
        	textShowLover = getString(R.string.text9_2);
        }else if(str_numLover.equals("text9_3")){
        	textShowLover = getString(R.string.text9_3);
        }else if(str_numLover.equals("text9_4")){
        	textShowLover = getString(R.string.text9_4);
        }else if(str_numLover.equals("text9_5")){
        	textShowLover = getString(R.string.text9_5);
        }else if(str_numLover.equals("text9_6")){
        	textShowLover = getString(R.string.text9_6);
        }else if(str_numLover.equals("text9_7")){
        	textShowLover = getString(R.string.text9_7);
        }else if(str_numLover.equals("text9_8")){
        	textShowLover = getString(R.string.text9_8);
        }else if(str_numLover.equals("text9_9")){
        	textShowLover = getString(R.string.text9_9);
        }else if(str_numLover.equals("text9_10")){
        	textShowLover = getString(R.string.text9_10);
        }else if(str_numLover.equals("text9_11")){
        	textShowLover = getString(R.string.text9_11);
        }else if(str_numLover.equals("text9_12")){
        	textShowLover = getString(R.string.text9_12);
        	
        }else if(str_numLover.equals("text10_1")){
        	textShowLover = getString(R.string.text10_1);
        }else if(str_numLover.equals("text10_2")){
        	textShowLover = getString(R.string.text10_2);
        }else if(str_numLover.equals("text10_3")){
        	textShowLover = getString(R.string.text10_3);
        }else if(str_numLover.equals("text10_4")){
        	textShowLover = getString(R.string.text10_4);
        }else if(str_numLover.equals("text10_5")){
        	textShowLover = getString(R.string.text10_5);
        }else if(str_numLover.equals("text10_6")){
        	textShowLover = getString(R.string.text10_6);
        }else if(str_numLover.equals("text10_7")){
        	textShowLover = getString(R.string.text10_7);
        }else if(str_numLover.equals("text10_8")){
        	textShowLover = getString(R.string.text10_8);
        }else if(str_numLover.equals("text10_9")){
        	textShowLover = getString(R.string.text10_9);
        }else if(str_numLover.equals("text10_10")){
        	textShowLover = getString(R.string.text10_10);
        }else if(str_numLover.equals("text10_11")){
        	textShowLover = getString(R.string.text10_11);
        }else if(str_numLover.equals("text10_12")){
        	textShowLover = getString(R.string.text10_12);
        	
        }else if(str_numLover.equals("text11_1")){
        	textShowLover = getString(R.string.text11_1);
        }else if(str_numLover.equals("text11_2")){
        	textShowLover = getString(R.string.text11_2);
        }else if(str_numLover.equals("text11_3")){
        	textShowLover = getString(R.string.text11_3);
        }else if(str_numLover.equals("text11_4")){
        	textShowLover = getString(R.string.text11_4);
        }else if(str_numLover.equals("text11_5")){
        	textShowLover = getString(R.string.text11_5);
        }else if(str_numLover.equals("text11_6")){
        	textShowLover = getString(R.string.text11_6);
        }else if(str_numLover.equals("text11_7")){
        	textShowLover = getString(R.string.text11_7);
        }else if(str_numLover.equals("text11_8")){
        	textShowLover = getString(R.string.text11_8);
        }else if(str_numLover.equals("text11_9")){
        	textShowLover = getString(R.string.text11_9);
        }else if(str_numLover.equals("text11_10")){
        	textShowLover = getString(R.string.text11_10);
        }else if(str_numLover.equals("text11_11")){
        	textShowLover = getString(R.string.text11_11);
        }else if(str_numLover.equals("text11_12")){
        	textShowLover = getString(R.string.text11_12);
        	
        }else if(str_numLover.equals("text12_1")){
        	textShowLover = getString(R.string.text12_1);
        }else if(str_numLover.equals("text12_2")){
        	textShowLover = getString(R.string.text12_2);
        }else if(str_numLover.equals("text12_3")){
        	textShowLover = getString(R.string.text12_3);
        }else if(str_numLover.equals("text12_4")){
        	textShowLover = getString(R.string.text12_4);
        }else if(str_numLover.equals("text12_5")){
        	textShowLover = getString(R.string.text12_5);
        }else if(str_numLover.equals("text12_6")){
        	textShowLover = getString(R.string.text12_6);
        }else if(str_numLover.equals("text12_7")){
        	textShowLover = getString(R.string.text12_7);
        }else if(str_numLover.equals("text12_8")){
        	textShowLover = getString(R.string.text12_8);
        }else if(str_numLover.equals("text12_9")){
        	textShowLover = getString(R.string.text12_9);
        }else if(str_numLover.equals("text12_10")){
        	textShowLover = getString(R.string.text12_10);
        }else if(str_numLover.equals("text12_11")){
        	textShowLover = getString(R.string.text12_11);
        }else if(str_numLover.equals("text12_12")){
        	textShowLover = getString(R.string.text12_12);
        	
        }
        
      //set text Lover
        Log.log("------------- detail");
        String textShow = textShowLover.substring(1);
        textShow = textShow.replace("[b]", " "+LoveMeterActivity.sMyName+" ");
        textShow = textShow.replace("[2]", " "+LoveMeterActivity.sYourName+" ");
        
        txtResultDetail.setText(textShow);
        
        
      //set text %Lover
        Log.log("----------percent");
        int num_lover = Integer.parseInt(textShowLover.substring(0, 1))*20;
        String strnum = new Integer(num_lover).toString()+"";
        txtPercent.setText(strnum);
        
	}
	
}