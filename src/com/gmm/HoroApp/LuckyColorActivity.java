//package com.gmm.gmmhoroscope;
//
//import android.app.Activity;
//import android.graphics.Bitmap;
//import android.graphics.BitmapFactory;
//import android.graphics.Color;
//import android.os.Bundle;
//import android.webkit.WebView;
//
//public class LuckyColorActivity extends Activity{
//
//	WebView mWebView = null;
//
//	protected void onCreate(Bundle savedInstanceState) {
//		
//		super.onCreate(savedInstanceState);
//		//setContentView(R.layout.luckycolour);
//		//ImageView img = (ImageView)findViewById(R.id.imgluckycolor);
//		//img.setOnTouchListener(this);
//		
////		ZoomImageView zoom;
////		Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.luckycolor);
////		zoom = new ZoomImageView(this, bitmap);
////		setContentView(zoom);
//	
//		setContentView(R.layout.luckycolorwebview);
//        mWebView = (WebView)findViewById(R.id.webview);
//        mWebView.getSettings().setBuiltInZoomControls(true);
//        mWebView.setBackgroundColor(Color.BLACK);
//        mWebView.loadUrl("file:///android_asset/luckycolor.html");  
//        
//        
////        mWebView.loadUrl("http://4998horo.gmember.com/contents2012/videoclip/thumb/0b4dcff10639bb2ca9499076ea7cf31e.png");
//	}
//
//}










package com.gmm.HoroApp;

import com.gmm.HoroApp.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Display;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

public class LuckyColorActivity extends Activity implements Runnable{
	
	
	private ProgressDialog pd;

	TextView mText_header;
	WebView mWebView;
	
	String URL_webview; 
	//String Text_header;
	
	boolean fin;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
		
		System.out.println("--- WebViewAcitivity");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.auspiciousday);
        
        fin=false;
        Log.log("Before ById");
        mWebView = (WebView)findViewById(R.id.webviewaus);
        Log.log("Aftr ById");
        //if(checkInternet()){
			
			pd = ProgressDialog.show( getParent() , "Loading..", "Please wait..",true,false);
	        
	        Thread thread = new Thread(this);
	        thread.start();
	        
	        /*
		}else{
			
			//alert 
    		String title = getString(R.string.title_internet_false);
            String message = getString(R.string.msg_internet_false);
            showNoticeDialogBox(title, message);
		}
        */
        
        
	}
	
	public void run() {
    	try {
    		
    		System.out.println("-run -- WebViewAcitivity");
    		
    		
    		mWebView.getSettings().setJavaScriptEnabled(true);
    		mWebView.getSettings().setSupportZoom(true);
    		
    		mWebView.getSettings().setLoadWithOverviewMode(true);
    		mWebView.getSettings().setUseWideViewPort(true);
    		mWebView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
    		mWebView.setPadding(0, 0, 0, 0);
    		mWebView.setInitialScale(getScale());
    		
//    		System.out.println("getScale() = "+getScale());
	        
	        mWebView.clearCache(true);
	        mWebView.clearView();
	        mWebView.clearHistory();
	        mWebView.clearFormData();
	        
//	        mWebView.loadUrl(URL_webview);
	        mWebView.setBackgroundColor(Color.BLACK);
	        mWebView.loadUrl("file:///android_asset/luckycolor.html");
	        fin=true;
    		
    		
		} catch (Exception e) {
			e.printStackTrace();
		}
        
        handler.sendEmptyMessage(0);
    }
	
	private int getScale(){
	    Display display = ((WindowManager) getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay(); 
	    int width = display.getWidth(); 
	    System.out.println("width = "+width);
	    Double val = new Double(width)/new Double(640);
	    val = val * 100d;
	    System.out.println("val.intValue() = "+val.intValue());
	    return val.intValue();
	}
	
	private Handler handler = new Handler() {
	    @Override
	    public void handleMessage(Message msg) {
	    	try {
	    		
	            pd.dismiss();
	            
	    	}
	    	catch (Exception e) {
				e.printStackTrace();
			}
	    }
	};
	
	
	protected void onResume() {
		
		super.onResume();
		
		System.out.println("---------- onResume() ----webview------");
		
//		if(fin){
//			finish();
//		}
		
	}
	
	
	private class HelloWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }
    
    
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK) && mWebView.canGoBack()) {
        	
        	
        	System.out.println("======== onKeyDown");
        	
            mWebView.goBack();
            
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
    
    
    private void showNoticeDialogBox(final String title, final String message) {
    	
    	System.out.println("---- showNoticeDialogBox");
        Builder setupAlert;
        setupAlert = new AlertDialog.Builder(LuckyColorActivity.this);    
        setupAlert.setTitle(title);
        setupAlert.setMessage(message);
        setupAlert.setPositiveButton("Ok",
            new DialogInterface.OnClickListener() {
              public void onClick(DialogInterface dialog, int whichButton) {
            	  
              }
            });
        setupAlert.show();
      }

    
    private boolean checkInternet() {
		
    	ConnectivityManager connec =  ( ConnectivityManager ) getSystemService( Context.CONNECTIVITY_SERVICE ); 
    	
    	if ( connec.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTED || connec.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTED )
    	{
    	     // connect 
    		return true;
    	}
    	else
    	{
    	     // not-connect 
    		return false;
    	}
    }

}
