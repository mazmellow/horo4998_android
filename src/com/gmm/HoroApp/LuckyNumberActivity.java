package com.gmm.HoroApp;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.anddev.andengine.engine.Engine;
import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.engine.handler.IUpdateHandler;
import org.anddev.andengine.engine.options.EngineOptions;
import org.anddev.andengine.engine.options.EngineOptions.ScreenOrientation;
import org.anddev.andengine.engine.options.resolutionpolicy.RatioResolutionPolicy;
import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.entity.scene.background.ColorBackground;
import org.anddev.andengine.entity.sprite.Sprite;
import org.anddev.andengine.entity.text.Text;
import org.anddev.andengine.entity.util.FPSLogger;
import org.anddev.andengine.opengl.texture.Texture;
import org.anddev.andengine.opengl.texture.TextureOptions;
import org.anddev.andengine.opengl.texture.region.TextureRegion;
import org.anddev.andengine.opengl.texture.region.TextureRegionFactory;
import org.anddev.andengine.ui.activity.BaseGameActivity;

import android.content.Context;
import android.content.Intent;
import android.hardware.SensorManager;
import android.media.MediaPlayer;
import android.os.Handler;
import android.os.Message;
import android.os.Vibrator;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.TextView;

public class LuckyNumberActivity extends BaseGameActivity implements Runnable{//implements SensorEventListener{

	private static final int CAMERA_WIDTH = 380;
    private static final int CAMERA_HEIGHT = 480;
	
    
    private Sprite sprite;
    private Camera mCamera;
    private Texture mTexture;
    private TextureRegion mSpriteTextureRegion;
    private SensorManager sensorManager;
 
    private int accellerometerSpeedX;
    private int accellerometerSpeedY;
    private int sX, sY; // Sprite coordinates
	
	//========= New Parameter
    
    public boolean statusRotation;
    private int spriteIndex;
    private float scale = 0;
    private int ssX, ssY;
    private int ImageWidth = 128;
    private int ImageHeight = 128;
    
    private List<TextureRegion> listRegionBox;
    private List<Sprite> listSpriteBox;
    private int	boxX[] = {10, 130, 250};
    private int boxY = 5;
    
    private List<TextureRegion> listRegionRun;
    private List<Sprite> listSpriteRun;
    
    private List<TextureRegion> listRegionShow;
    private List<Sprite> listSpriteShow;
    
    private Sprite spriteLight;
    private TextureRegion regionLight;
    
    private List<TextureRegion> listRegionLuckyBlackGround;
    private List<SubSprite> listSpriteLuckyBlackGorund;
    private SubSprite subSprite;
    
    private TextureRegion regionBG;
    private Sprite spriteBG;
    
    private TextureRegion regionBG2;
    private Sprite spriteBG2;
    
    private TextureRegion regionDialog;
    private Sprite spriteDialog;
    
    private Text textCenter;
    private Text textLeft;
    
    private String pathImage = "image/";
    
    private int [] index = new int[3];
    
    private int mYear, mMonth, mDay, monthUser = 0;
    
    private LuckyNumberData luckyNumberData;
	private List<ContentHoro> listContent;
	
	private ProfileData profileData;
	private List<Profile> listProfile;
	
	private String textShow1;
	private String textShow2;
	private String textShow3;
	private String textShow4;
	private String yourLuckynumber;
	
//	private MediaPlayer run;
	private MediaPlayer show;
    
    private String luckynumberRun[] = {
    						"lucky_num_a_0.png",
    						"lucky_num_a_1.png",
    						"lucky_num_a_2.png",
    						"lucky_num_a_3.png",
    						"lucky_num_a_4.png",
    						"lucky_num_a_5.png",
    						"lucky_num_a_6.png",
    						"lucky_num_a_7.png",
    						"lucky_num_a_8.png",
    						"lucky_num_a_9.png"
    						
    						};
    private String lucknumberShow[] = {
    						"gold_num_0.png",
    						"gold_num_1.png",
    						"gold_num_2.png",
    						"gold_num_3.png",
    						"gold_num_4.png",
    						"gold_num_5.png",
    						"gold_num_6.png",
    						"gold_num_7.png",
    						"gold_num_8.png",
    						"gold_num_9.png"

    						};
    
    private String luckynumberBlackGround[] = {
    						"lucky_num_a_0.png",
    						"lucky_num_a_1.png",
				    		"lucky_num_a_2.png", 
				    		"lucky_num_a_3.png", 
				    		"lucky_num_a_4.png", 
				    		"lucky_num_a_5.png", 
							"lucky_num_a_6.png", 
							"lucky_num_a_7.png", 
							"lucky_num_a_8.png", 
							"lucky_num_a_9.png", 
							"lucky_num_a_star.png",
				
							"num_set_a_0.png",
							"num_set_a_1.png", 
							"num_set_a_2.png", 
							"num_set_a_3.png", 
							"num_set_a_4.png", 
							"num_set_a_5.png",
							"num_set_a_6.png", 
							"num_set_a_7.png", 
							"num_set_a_8.png", 
							"num_set_a_9.png", 
							"num_set_a_star.png",
				
							"num_set_b_0.png",
							"num_set_b_1.png", 
							"num_set_b_2.png", 
							"num_set_b_3.png", 
							"num_set_b_4.png", 
							"num_set_b_5.png",
							"num_set_b_6.png", 
							"num_set_b_7.png", 
							"num_set_b_8.png", 
							"num_set_b_9.png", 
							"num_set_b_star.png",
							
							//===============
							"lucky_num_a_0.png",
							"lucky_num_a_1.png",
				    		"lucky_num_a_2.png", 
				    		"lucky_num_a_3.png", 
				    		"lucky_num_a_4.png", 
				    		"lucky_num_a_5.png", 
							"lucky_num_a_6.png", 
							"lucky_num_a_7.png", 
							"lucky_num_a_8.png", 
							"lucky_num_a_9.png", 
							"lucky_num_a_star.png",
				
							"num_set_a_0.png",
							"num_set_a_1.png", 
							"num_set_a_2.png", 
							"num_set_a_3.png", 
							"num_set_a_4.png", 
							"num_set_a_5.png",
							"num_set_a_6.png", 
							"num_set_a_7.png", 
							"num_set_a_8.png", 
							"num_set_a_9.png", 
							"num_set_a_star.png",
				
							"num_set_b_0.png",
							"num_set_b_1.png", 
							"num_set_b_2.png", 
							"num_set_b_3.png", 
							"num_set_b_4.png", 
							"num_set_b_5.png",
							"num_set_b_6.png", 
							"num_set_b_7.png", 
							"num_set_b_8.png", 
							"num_set_b_9.png", 
							"num_set_b_star.png"
			
			};
    
    private int [] txtlucky = {
    						R.string.txtlucky1,
    						R.string.txtlucky2,
    						R.string.txtlucky3,
    						R.string.txtlucky4,
    						R.string.txtlucky5,
    						R.string.txtlucky6,
    						R.string.txtlucky7,
    						R.string.txtlucky8,
    						R.string.txtlucky9,
    						R.string.txtlucky10,
    						R.string.txtlucky11,
    						R.string.txtlucky12,
    						R.string.txtlucky13,
    						R.string.txtlucky14,
    						R.string.txtlucky15,
    						R.string.txtlucky16,
    						R.string.txtlucky17,
    						R.string.txtlucky18,
    						R.string.txtlucky19,
    						R.string.txtlucky20,
    						R.string.txtlucky21,
    						R.string.txtlucky22,
    						R.string.txtlucky23,
    						R.string.txtlucky24
    					};
    
//    private Texture mFontTexture;
//    private Font mFont;
    private TextView txt1;
	private TextView txt2;
	private TextView txt3;
	private TextView txt4;
	private TextView yourTxtluckynumber;
	private TextView currentMonth;
	
	////////public static boolean luckynumberloadcomplete = false;
    
    public Engine onLoadEngine() {    	
    	System.out.println("----- onLoadEngine -----");
    	
    	if(!TabPlusMenuActivity.luckynumberloadcomplete){
			Intent in = new Intent(LuckyNumberActivity.this, TabPlusMenuActivity.class);
			in.putExtra("IDTAB", 3);
			startActivity(in);
			
			TabPlusMenuActivity.luckynumberloadcomplete = true;
			
			finish();
		}
    	
    	txt1 = new TextView(this);
    	txt2 = new TextView(this);
    	txt3 = new TextView(this);
    	txt4 = new TextView(this);
    	yourTxtluckynumber = new TextView(this);
    	currentMonth = new TextView(this);
    	
    	statusRotation = true;
    	
    	//=========== get the current date
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH) ;
        mDay = c.get(Calendar.DAY_OF_MONTH);
        
      //============ query luckynumber =======
		luckyNumberData = new LuckyNumberData(LuckyNumberActivity.this);
    	listContent = new ArrayList<ContentHoro>();
    	profileData = new ProfileData(LuckyNumberActivity.this);
    	listProfile = new ArrayList<Profile>();
    	
    	try {
    		listProfile = profileData.query(null, null);
    		listContent = luckyNumberData.query(null, null);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		//=========== query month user========
		if(listProfile != null){
			if(listProfile.size() > 0){				
				monthUser = Integer.parseInt(listProfile.get(0).getBirthdate().toString().substring(5, 7)); 
				if(monthUser < 4){
					monthUser += 9;
				}else{
					monthUser -= 3;
				}
				Log.log("Month User : " + monthUser);
			}
		}
		 
		//=========== Random luckyNumber =====
		if(listContent != null){
			Log.log("listContent != null");
			if(listContent.size() > 0){
					Log.log("Title : "  + listContent.get(monthUser).getTitle());

//					String s1 = listContent.get(monthUser).getTitle().substring(0,1);
//					String s2 = listContent.get(monthUser).getTitle().substring(1,2);
//					String s3 = listContent.get(monthUser).getTitle().substring(2,3);

					
					//  ================= Wait backend ==========
					for(int i = 0; i < listContent.size(); i++){
						
						System.out.println(listContent.get(i).getID()  + " equals  " + monthUser); 
						
						if(listContent.get(i).getID().equals(monthUser+"")){
							
							System.out.println("Title : " + listContent.get(i).getTitle()); 
							
							String nums[] = listContent.get(i).getTitle().toString().trim().split(",");
							
							index[0] = Integer.parseInt(nums[(int)(Math.random()*5)]);
							index[1] = Integer.parseInt(nums[(int)(Math.random()*5)]);
							index[2] = Integer.parseInt(nums[(int)(Math.random()*5)]);
							
							yourLuckynumber = ""+index[0] + index[1] + index[2];
						}
					}
					
					
					
//					index[0] = 3;
//					index[1] = 9;
//					index[2] = 7;
					
					
					//============= getText show after show luckynumber from backend ==========
					
					textShow1 = getString(R.string.textShow1);
					textShow2 = getString(R.string.textShow2);
					textShow3 = getString(R.string.textShow3);
					textShow4 = getString(R.string.textShow4);
					
//					if(listContent.get(mMonth).getDesc().length() > 25){
//						textShow1 = listContent.get(mMonth).getDesc().substring(0, 25);
//						textShow2 = listContent.get(mMonth).getDesc().substring(25, listContent.get(mMonth).getDesc().length());
//					}
//					else{
//						textShow1 = listContent.get(mMonth).getDesc();
//						textShow2 = "";
//					}
					
					
			}else{
				Log.log("listContent.size < 0");
			}
		}else{
			Log.log("listContent == null");
			int n_month = mMonth+1;
			String sMonth = "";
			
			if(n_month==1){
            	sMonth = getString(R.string.month1);
            }else if(n_month==2){
            	sMonth = getString(R.string.month2);
            }else if(n_month==3){
            	sMonth = getString(R.string.month3);
            }else if(n_month==4){
            	sMonth = getString(R.string.month4);
            }else if(n_month==5){
            	sMonth = getString(R.string.month5);
            }else if(n_month==6){
            	sMonth = getString(R.string.month6);
            }else if(n_month==7){
            	sMonth = getString(R.string.month7);
            }else if(n_month==8){
            	sMonth = getString(R.string.month8);
            }else if(n_month==9){
            	sMonth = getString(R.string.month9);
            }else if(n_month==10){
            	sMonth = getString(R.string.month10);
            }else if(n_month==11){
            	sMonth = getString(R.string.month11);
            }else if(n_month==12){
            	sMonth = getString(R.string.month12);
            }
			
			index[0] = (int) (Math.random()*9);
			index[1] = (int) (Math.random()*9);
			index[2] = (int) (Math.random()*9);
			
			textShow1 = getString(R.string.textShow1);
			textShow2 = getString(R.string.textShow2);// + sMonth + " " + mYear;
		}
		
		
    	spriteIndex = 1;
    	
    	listRegionBox = new ArrayList<TextureRegion>();
    	listSpriteBox = new ArrayList<Sprite>();
    	
    	listRegionRun = new ArrayList<TextureRegion>();
    	listSpriteRun = new ArrayList<Sprite>();
    	
    	listRegionShow = new ArrayList<TextureRegion>();
    	listSpriteShow = new ArrayList<Sprite>();
    	
    	listRegionLuckyBlackGround = new ArrayList<TextureRegion>();
    	listSpriteLuckyBlackGorund = new ArrayList<SubSprite>();
    	
//    	run = MediaPlayer.create(LuckyNumberActivity.this, R.raw.run);
    	
		this.mCamera = new Camera(0, 0, CAMERA_WIDTH, CAMERA_HEIGHT);
        return new Engine(new EngineOptions(true, ScreenOrientation.PORTRAIT, new RatioResolutionPolicy(CAMERA_WIDTH, CAMERA_HEIGHT), this.mCamera));
	}

   
    public void onLoadComplete() {
    	System.out.println("---- onLoadComplete ----");
    	listennerShek();
    	TabPlusMenuActivity.luckynumberloadcomplete = true;
	}

	public void onLoadResources() {
        System.out.println("-------- onLoadResources -----");
		
        //=========== Set Sprite BlackGround =====
    	this.mTexture = new Texture(512, 512, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.regionBG = TextureRegionFactory.createFromAsset(this.mTexture, this, pathImage + "bg_screen_01.png", 0, 0);
        this.mEngine.getTextureManager().loadTexture(this.mTexture);
        
        this.mTexture = new Texture(512, 512, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.regionBG2 = TextureRegionFactory.createFromAsset(this.mTexture, this, pathImage + "bg_screen_base.png", 0, 0);
        this.mEngine.getTextureManager().loadTexture(this.mTexture);
        
        
        
        for(int i = 0; i<luckynumberBlackGround.length; i++){
        	
        	//=========== Set Sprite LuckyNumber BlackGround =====
        	this.mTexture = new Texture(ImageWidth, ImageHeight, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
            this.mSpriteTextureRegion = TextureRegionFactory.createFromAsset(this.mTexture, this, pathImage + luckynumberBlackGround[i], 0, 0);
            listRegionLuckyBlackGround.add(this.mSpriteTextureRegion);
            this.mEngine.getTextureManager().loadTexture(this.mTexture);
        }
        
        //============ Set Sprite Light ======
    	this.mTexture = new Texture(512, 512, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
    	this.regionLight = TextureRegionFactory.createFromAsset(this.mTexture, this, pathImage + "item_light_ray_02.png", 0, 0);
    	this.mEngine.getTextureManager().loadTexture(this.mTexture);
        
        for(int i =0 ; i<3; i++){
        	
        	//============ Set Sprite Box =======
        	this.mTexture = new Texture(ImageWidth, ImageHeight, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        	this.mSpriteTextureRegion = TextureRegionFactory.createFromAsset(this.mTexture, this, pathImage + "item_box.png", 0, 0);
        	this.listRegionBox.add(this.mSpriteTextureRegion);
        	this.mEngine.getTextureManager().loadTexture(this.mTexture);
        	
        	//============ Set Sprite Run =======
        	this.mTexture = new Texture(ImageWidth, ImageHeight, TextureOptions.BILINEAR_PREMULTIPLYALPHA);	
        	this.mSpriteTextureRegion = TextureRegionFactory.createFromAsset(this.mTexture, this, pathImage + luckynumberRun[index[i]], 0, 0);
        	this.listRegionRun.add(this.mSpriteTextureRegion);
        	this.mEngine.getTextureManager().loadTexture(this.mTexture);
        	
        	//============ Set Sprite Show =======
        	this.mTexture = new Texture(ImageWidth, ImageHeight, TextureOptions.BILINEAR_PREMULTIPLYALPHA);	
        	this.mSpriteTextureRegion = TextureRegionFactory.createFromAsset(this.mTexture, this, pathImage + lucknumberShow[index[i]], 0, 0);
        	this.listRegionShow.add(this.mSpriteTextureRegion);
        	this.mEngine.getTextureManager().loadTexture(this.mTexture);
        }
        
    	//============ Set Sprite Dialog =======
    	this.mTexture = new Texture(512, 512, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
    	this.regionDialog = TextureRegionFactory.createFromAsset(this.mTexture, this, pathImage + "box_blk_trans_01.png", 0, 0);
    	this.mEngine.getTextureManager().loadTexture(this.mTexture);        
    	
    	//============= Set Text ==============
//    	this.mFontTexture = new Texture(128, 128, TextureOptions.BILINEAR);
//
//        this.mFont = new Font(this.mFontTexture, Typeface.create(Typeface.DEFAULT, Typeface.BOLD), 20, true, Color.WHITE); 
//        this.mEngine.getTextureManager().loadTexture(this.mFontTexture);
//        this.mEngine.getFontManager().loadFont(this.mFont);
        
//        // =========== Set Sound ==========
//        SoundFactory.setAssetBasePath("raw/");
//        try {
//                this.Sound = SoundFactory.createSoundFromAsset(this.mEngine.getSoundManager(), this, "run.mp3");
//        } catch (final IOException e) {
//                e.printStackTrace();
//        }
        
	}

	public Scene onLoadScene() {
		System.out.println("------- onLoadScene -----");
//		sensorManager = (SensorManager) this.getSystemService(this.SENSOR_SERVICE);
//        sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), sensorManager.SENSOR_DELAY_GAME);
 
        this.mEngine.registerUpdateHandler(new FPSLogger());
        this.mEngine.registerUpdateHandler(new IUpdateHandler() {
            public void onUpdate(float pSecondsElapsed) {
            	
            	if(TabPlusMenuActivity.shaked){
            		updateSprite();
            	}   
            }
 
            public void reset() {
            	System.out.println("------ reset ----");
            	txt1.setVisibility(View.VISIBLE);
            	txt2.setVisibility(View.VISIBLE);
            	txt3.setVisibility(View.VISIBLE);
            	txt4.setVisibility(View.VISIBLE);
            	yourTxtluckynumber.setVisibility(View.VISIBLE);
            	currentMonth.setVisibility(View.VISIBLE);
            }
        });
 
        final Scene scene = new Scene(1);
        scene.setBackground(new ColorBackground(0.2f, 0.3f, 0.4f));
 
        //============ Set BlackGround =========
        spriteBG = new Sprite(0,0,this.regionBG);
        spriteBG.setScale(1.5f);
        scene.attachChild(spriteBG);
        
        spriteBG2 = new Sprite(0,0,this.regionBG2);
        spriteBG2.setScale(1.5f);
        scene.attachChild(spriteBG2);
        
        for(int i = 0; i<luckynumberBlackGround.length; i++){
        	ssX = (int)(Math.random()*270);
            ssY = (int)(Math.random()*430);
            
        	//========== Set Sprite LuckyNumber BlackGround ====
        	subSprite = new SubSprite(ssX, ssY, this.listRegionLuckyBlackGround.get(i));
        	subSprite.setScale(1.0f);
        	subSprite.setAlpha(0.8f);
        	
        	
        	int vel = (int)(Math.random()*2 + 1);
        	if(Math.random() > 0.5f){
        		subSprite.vX = vel;
        		subSprite.setRotation(vel);
			}else{
				subSprite.vX = -vel;
				subSprite.setRotation(-vel);
			}
			
			if(Math.random() > 0.5f){
				subSprite.vY = vel;
			}else{
				subSprite.vY = -vel;
			}
			subSprite.setRotation(ssX);
        	
        	listSpriteLuckyBlackGorund.add(subSprite);
        	scene.attachChild(subSprite);
        }
        
        
        sX = (CAMERA_WIDTH - this.mSpriteTextureRegion.getWidth()) / 2;
        sY = (CAMERA_HEIGHT - this.mSpriteTextureRegion.getHeight()) / 2;
        
        //========= Set Sprite Light
    	spriteLight = new Sprite((CAMERA_WIDTH - this.regionLight.getWidth()) / 2, (CAMERA_HEIGHT - this.regionLight.getHeight()) / 2, this.regionLight);	
    	spriteLight.setScale(2.0f);
    	spriteLight.setVisible(false);
    	scene.attachChild(spriteLight);
        
        for(int i=0; i<3;i++){
        	
        	//========= Set Sprite Box =========
        	sprite = new Sprite(boxX[i], boxY, this.listRegionBox.get(i));
        	sprite.setScale(0.9f);
        	listSpriteBox.add(sprite);
        	scene.attachChild(sprite);
        	
        	//========= Set Sprite Run =========
        	sprite = new Sprite(sX, sY, this.listRegionRun.get(i));
        	sprite.setScale(scale);	
        	listSpriteRun.add(sprite);
        	scene.attachChild(sprite);
        	
        	//========= Set Sprite Show =========
        	sprite = new Sprite(boxX[i], boxY, this.listRegionShow.get(i));
        	sprite.setScale(0.9f);
        	sprite.setVisible(false);
        	listSpriteShow.add(sprite);
        	scene.attachChild(sprite);        	
        	
        }

        //========= Set Sprite Dialog =========
    	spriteDialog = new Sprite((CAMERA_WIDTH - this.regionDialog.getWidth()) / 2, ((CAMERA_HEIGHT - this.regionDialog.getHeight()) / 2) + 50, this.regionDialog);
    	spriteDialog.setVisible(false);
    	spriteDialog.setScale(0.6f);
    	scene.attachChild(spriteDialog); 
    	
    	//========== Set Text ==============        
    	txt1.setText(textShow1);
    	txt2.setText(textShow2);
    	txt3.setText(textShow3);
    	txt4.setText(textShow4);
    	yourTxtluckynumber.setText(yourLuckynumber);
    	currentMonth.setText(getCurrentDate());
    	
    	
    	txt1.setPadding(60, 220, 0, 0);
    	txt2.setPadding(190, 400, 0, 0);
    	txt3.setPadding(130, 440, 0, 0);
    	txt4.setPadding(150, 490, 0, 0);
    	yourTxtluckynumber.setPadding(210, 320, 0, 0);
    	currentMonth.setPadding(120,270, 0, 0);
    	
    	
    	txt1.setTextSize(18);
    	txt2.setTextSize(18);
    	txt3.setTextSize(18);
    	txt4.setTextSize(18);
    	yourTxtluckynumber.setTextSize(25);
    	currentMonth.setTextSize(18);
    	
    	txt1.setVisibility(View.GONE);
    	txt2.setVisibility(View.GONE);
    	txt3.setVisibility(View.GONE);
    	txt4.setVisibility(View.GONE);
    	yourTxtluckynumber.setVisibility(View.GONE);
    	currentMonth.setVisibility(View.GONE);
    	
    	
    	this.addContentView(txt1, new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
    	this.addContentView(txt2, new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
    	this.addContentView(txt3, new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
    	this.addContentView(txt4, new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
    	this.addContentView(yourTxtluckynumber, new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
    	this.addContentView(currentMonth, new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
    	
    	
    	
        return scene;
	}
	
	
	
	private void updateSprite(){
		float x;
    	float y;
    	int vX;
    	int vY;
    	float degree;
    	
    	
		// ======== Set Sprite LuckyNumber BlackGround =====
		for(int i = 0; i<luckynumberBlackGround.length; i++){
			

			x = listSpriteLuckyBlackGorund.get(i).getX();
    		y = listSpriteLuckyBlackGorund.get(i).getY();
    		vX = listSpriteLuckyBlackGorund.get(i).vX;
    		vY = listSpriteLuckyBlackGorund.get(i).vY;
    		degree = listSpriteLuckyBlackGorund.get(i).getRotation();
    		
			if(statusRotation){
	    		
	    		if(x > CAMERA_WIDTH - (ImageWidth - 100) || x < -100){
	    			listSpriteLuckyBlackGorund.get(i).vX = -(vX);
	        	}
	    		if(y > CAMERA_HEIGHT - (ImageHeight - 100) || y < -100){
	    			listSpriteLuckyBlackGorund.get(i).vY = -(vY);
	        	}
	    		
	    		x += listSpriteLuckyBlackGorund.get(i).vX;
	        	y += listSpriteLuckyBlackGorund.get(i).vY;
	        	
	        	
	        	if(listSpriteLuckyBlackGorund.get(i).getRotation() < 150){
	        		degree -= 5.0f;
	        	}else{
	        		degree += 5.0f;
	        	}
			}
			listSpriteLuckyBlackGorund.get(i).setPosition(x, y);
        	listSpriteLuckyBlackGorund.get(i).setRotation(degree);
		}
		
		
		//=========== Set Sprite Run Update ::: Xn = 10, Yn = 10, Xo = 96, Yo = 176
		if(spriteIndex == 1){
			if(scale >= 0.9){
				spriteLight.setVisible(true);
				if(sX > 10.0f){
					sX -= 4.33f;
					}
				
				if(sY > 10.0f){
					sY -= 6.3f;
				}
				else{
					spriteIndex = 2; 
					scale = 0;
					sX = (CAMERA_WIDTH - this.mSpriteTextureRegion.getWidth()) / 2;
			        sY = (CAMERA_HEIGHT - this.mSpriteTextureRegion.getHeight()) / 2;
					listSpriteShow.get(0).setVisible(true);
					soundStop();
				}
				
			}else{
				scale += 0.01f;
			}
			
			listSpriteRun.get(0).setScale(scale);
			listSpriteRun.get(0).setPosition(sX, sY);
			
			soundRun();
			
		}else if(spriteIndex == 2){
			
			spriteLight.setVisible(false);
			
			if(scale >= 0.9){
				spriteLight.setVisible(true);
				if(sX > 1300.0f){
					sX += 0.0f;
					}
				
				if(sY > 10.0f){
					sY -= 8.3f;
				}
				else{
					spriteIndex = 3; 
					scale = 0;
					sX = (CAMERA_WIDTH - this.mSpriteTextureRegion.getWidth()) / 2;
			        sY = (CAMERA_HEIGHT - this.mSpriteTextureRegion.getHeight()) / 2;
					listSpriteShow.get(1).setVisible(true);
					soundStop();
				}
				
			}else{
				scale += 0.01f;
			}
			
			listSpriteRun.get(1).setScale(scale);
			listSpriteRun.get(1).setPosition(sX, sY);
			soundRun();
			
		}else if(spriteIndex == 3){
			
			spriteLight.setVisible(false);
			
			if(scale >= 0.9){
				spriteLight.setVisible(true);
				if(sX < 250.0f){
					sX += 6.0;
					}
				
				if(sY > 10.0f){
					sY -= 8.3f;
				}
				else{
					spriteIndex = 4; 
					scale = 0;
					listSpriteShow.get(2).setVisible(true);
					spriteLight.setVisible(false);
					statusRotation = false;
					spriteDialog.setVisible(true);

					Thread thread = new Thread(this);
			        thread.start();
					
			        soundStop();

				}
				
			}else{
				scale += 0.01f;
			}
			
			listSpriteRun.get(2).setScale(scale);
			listSpriteRun.get(2).setPosition(sX, sY);
			
		}
		
	}

	private void soundRun(){
//		if(!(run.isPlaying())){
//    		run.setLooping(false);
//    		run.start();
//		}
	}
	
	private void soundStop(){
//		run.stop();
//		run.release();
//		run = null;
//		run = MediaPlayer.create(LuckyNumberActivity.this, R.raw.run);
		show = null;
		show = MediaPlayer.create(LuckyNumberActivity.this, R.raw.show);
		show.setLooping(false);
		show.start();
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		Log.log("----- onTouchEven t -----");
		
		System.out.println("getX: " + event.getX()); 
		System.out.println("getY: " + event.getY());
		TabPlusMenuActivity.shaked = false;		
		if(!statusRotation){
			
			TabPlusMenuActivity.luckynumberloadcomplete = false;
			
			insertDecorView(TabPlusLuckyNumberActivity
					.mLocalActivityManager
					.startActivity("splashluckynumber", new Intent(LuckyNumberActivity.this,SplashLuckyNumberActivity.class)
					.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP))
					.getDecorView());	
		}
		
		return super.onTouchEvent(event);
	}
	
	private void insertDecorView(View view) {
	   	 
    	getParent().setContentView(view, new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
    }

	public void run() {
		try {
			Log.log("------ run HomeActivity ----");
			
		
		} catch (Exception e) {
			e.printStackTrace();
		}
    
    	handler.sendEmptyMessage(0);
	}


	private Handler handler = new Handler() {
	    
	    public void handleMessage(Message msg) {
	    	
	    	try {
	    		txt1.setVisibility(View.VISIBLE);
	    		txt2.setVisibility(View.VISIBLE);
	    		txt3.setVisibility(View.VISIBLE);
	    		txt4.setVisibility(View.VISIBLE);
	    		yourTxtluckynumber.setVisibility(View.VISIBLE);
	    		currentMonth.setVisibility(View.VISIBLE);
	    	}
	    	catch (Exception e) {
	    		Log.log("Error");
				e.printStackTrace();
			}
	    }
	};

	////////////public static boolean shaked;
	private MediaPlayer mp;
	private ShakeListener mShaker;
	protected boolean _active = true;
	protected int _splashTime = 2000;


	private void listennerShek(){

		Log.log("--------- LuckyNumberActivity ------------");
		
	    final Vibrator vibe = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
	    TabPlusMenuActivity.shaked = false;
	    mShaker = new ShakeListener(this);
	    mShaker.setOnShakeListener(new ShakeListener.OnShakeListener () {
	      public void onShake(){
	    	    
	    	if(!TabPlusMenuActivity.shaked){
	    		TabPlusMenuActivity.shaked = true;
	    		
	    		mp = MediaPlayer.create(LuckyNumberActivity.this, R.raw.shk);
	    		mp.setLooping(false);
	    		mp.start();
	    		
	    		
	    		vibe.vibrate(2000);
	    		
	    		Thread splashTread = new Thread() {
					@Override
					public void run() {
						try {

							int waited = 0;
							while (_active && (waited < _splashTime)) {
								sleep(100);
								if (_active) {
									waited += 200;
								}
							}

						} catch (InterruptedException e) {
							// do nothing
						} finally {

							try {
//								
								TabPlusMenuActivity.shaked = true;
						        
						        mp.stop();
						        mp.release();

							} catch (Exception e) {
								e.printStackTrace();
							}

						}
					}
				};
				splashTread.start(); 
	    		
	    	}
	      }
	    });
	}
	
	private String getCurrentDate(){
		if(mDay < 16){	
			return getString(txtlucky[mMonth*2]) + " " + (mYear+543) ;
		}
		return getString(txtlucky[(mMonth*2)+1]) + " " + (mYear+543);
		
		
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		
		Log.log("--------------- onKeyDown LuckyNumberActivity");
		
		if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
			TabPlusMenuActivity.shaked = false;
			finish();
			onDestroy();
			TabPlusMenuActivity.luckynumberloadcomplete = false;
		}
		
		
		return super.onKeyDown(keyCode, event);
	}
	
}
