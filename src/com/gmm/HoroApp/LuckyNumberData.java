package com.gmm.HoroApp;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

public class LuckyNumberData {
	

	private Context mContext;
	
	public LuckyNumberData(Context context){
		mContext = context;
	}
	
	
	public void insert(final ContentHoro horo) {
		
		System.out.println(" LuckyNumberData -- insert");
		
		System.out.println("horo.getID() :"+horo.getID());
		
		ContentValues values = new ContentValues();	
		
		values.put(ContentHoro.COL_id, horo.getID());
		values.put(ContentHoro.COL_title, horo.getTitle());
		values.put(ContentHoro.COL_desc, horo.getDesc());
		values.put(ContentHoro.COL_date, horo.getDate());		//4
		
    	mContext.getContentResolver().insert(UsersLuckyNumberDB.AuthenticationInfo.CONTENT_URI, values);
    	
	}
	
	
	public void update(final ContentHoro horo, String selection, final String selectionArgs) {
		
		System.out.println(" LuckyNumberData -- update");
		
		//String selection = Content.COL_id + "=?";
		ContentValues values = new ContentValues(); 
		
		values.put(ContentHoro.COL_id, horo.getID());
		values.put(ContentHoro.COL_title, horo.getTitle());
		values.put(ContentHoro.COL_desc, horo.getDesc());
		values.put(ContentHoro.COL_date, horo.getDate());		//4
		
		mContext.getContentResolver().update(UsersLuckyNumberDB.AuthenticationInfo.CONTENT_URI, 
		    values, 
		    selection, 
		    new String[]{selectionArgs} 
			);
	}
	
	
	
	
	
	public final List<ContentHoro> query( String selection, final String contentCodee) {
		
		System.out.println(" LuckyNumberData -- query");
		
		// An array specifying which columns to return.
		String columns[] = new String[] { 
				ContentHoro.COL_id,
				ContentHoro.COL_title,
				ContentHoro.COL_desc,
				ContentHoro.COL_date }; //4

		//selection = Content.COL_contentcode + "=?";

		Uri myUri = UsersLuckyNumberDB.AuthenticationInfo.CONTENT_URI;
		Cursor cur = mContext.getContentResolver().query(myUri, columns, selection, new String[]{contentCodee}, null);
		
		List<ContentHoro> luckyNumberHoroListt = new ArrayList<ContentHoro>();

		// check if found any query result
		if (cur.getCount() > 0) {
			
			cur.moveToFirst();
			int numRows = cur.getCount();
			
			for (int i = 0; i < numRows; ++i) {
			
				ContentHoro monthlyHoro = new ContentHoro(); 
				
				monthlyHoro.setID(cur.getString(cur.getColumnIndex(ContentHoro.COL_id)));
				monthlyHoro.setTitle(cur.getString(cur.getColumnIndex(ContentHoro.COL_title)));
				monthlyHoro.setDesc(cur.getString(cur.getColumnIndex(ContentHoro.COL_desc)));
				monthlyHoro.setDate(cur.getString(cur.getColumnIndex(ContentHoro.COL_date)));		//4
				
				
				luckyNumberHoroListt.add(monthlyHoro);
				
				
				System.out.println("--- luckyNumberHoroListt "+i+" :"+luckyNumberHoroListt.get(i).getID());
				System.out.println("luckyNumberHoroListt "+i+" :"+luckyNumberHoroListt.get(i).getTitle());
				System.out.println("luckyNumberHoroListt "+i+" :"+luckyNumberHoroListt.get(i).getDesc());
				System.out.println("luckyNumberHoroListt "+i+" :"+luckyNumberHoroListt.get(i).getDate());
				
				
				cur.moveToNext();
				
			}
			
			return luckyNumberHoroListt;
		}
		
		// close cursor.
		cur.close();
		return null;
	}
	

	
	public void delete( final String selection, final String selectionArgs) {

		System.out.println(" LuckyNumberData -- delete");
		
		//String selection = Content.COL_contentcode +"=?";
		
		Uri myUri = UsersLuckyNumberDB.AuthenticationInfo.CONTENT_URI;
		mContext.getContentResolver().delete(myUri, selection, new String[]{selectionArgs});
		
	}
}
