package com.gmm.HoroApp;

import android.graphics.Bitmap;

public class MenuList {
	
	public static String COL_id="id";
	public static String COL_menuid="menuid";
	public static String COL_menutypeid="menutypeid";
	public static String COL_menutypecallname="menutypecallname";
	public static String COL_menutypeimg="menutypeimg";		
	public static String COL_menutypeurl="menutypeurl";		//6 COL
	
	private int id=0;
	private String menuid="";
	private String menutypeid="";
	private String menutypecallname="";
	
	private Bitmap menutypeimg;
	private String menutypeimg_url="";
	
	private String menutypeurl="";
	
	
	
	public int getID() {
		return(id);
	}
	
	public void setID(int id) {
		this.id=id;
	}
	
	
	public String getMenuID() {
		return(menuid);
	}
	
	public void setMenuID(String menuid) {
		this.menuid=menuid;
	}

	
	public String getMenuTypeID() {
		return(menutypeid);
	}
	
	public void setMenuTypeID(String menutypeid) {
		this.menutypeid=menutypeid;
	}
	
	
	public String getMenuTypeCallName() {
		return(menutypecallname);
	}
	
	public void setMenuTypeCallName(String menutypecallname) {
		this.menutypecallname=menutypecallname;
	}
	
	

	public Bitmap getMenuTypeImg() {
		return(menutypeimg);
	}
	
	public void setMenuTypeImg(Bitmap menutypeimg) {
		this.menutypeimg=menutypeimg;
	}
	
	
	public String getMenuTypeImg_URL() {
		return(menutypeimg_url);
	}
	
	public void setMenuTypeImg_URL(String menutypeimg_url) {
		this.menutypeimg_url=menutypeimg_url;
	}
	
	
	public String getMenuTypeUrl() {
		return(menutypeurl);
	}
	
	public void setMenuTypeUrl(String menutypeurl) {
		this.menutypeurl=menutypeurl;
	}
	
}
