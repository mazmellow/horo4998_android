package com.gmm.HoroApp;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;

public class MenuListData {
	
	private Context mContext;
	
	public MenuListData(Context context){
		mContext = context;
	}


	public void insert(final MenuList menu) {
		
		System.out.println(" MenuListData -- insert");
		
		ContentValues values = new ContentValues();	
		//id
		values.put(MenuList.COL_menuid, menu.getMenuID());
		values.put(MenuList.COL_menutypeid, menu.getMenuTypeID());
		values.put(MenuList.COL_menutypecallname, menu.getMenuTypeCallName());
		
		try{
			
			if(menu.getMenuTypeImg_URL().trim().length()>0){
			
				DefaultHttpClient mHttpClient = new DefaultHttpClient();
		    	HttpGet mHttpGet = new HttpGet( menu.getMenuTypeImg_URL() );		//Link
		    	HttpResponse mHttpResponse = mHttpClient.execute(mHttpGet);
		
		    	if (mHttpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
		    	  HttpEntity entity = mHttpResponse.getEntity();
		    	    if ( entity != null) {
		    	      values.put(MenuList.COL_menutypeimg, EntityUtils.toByteArray(entity));
		    	    }
		    	    
		    	}else{
		    		values.put(MenuList.COL_menutypeimg, "");
		    	}
		    	
			}else{
				
				values.put(MenuList.COL_menutypeimg, "");
			}
    	
		}catch(Exception e) {
			System.out.println("error contect save img_cover in AlbumData");
			values.put(MenuList.COL_menutypeimg, "");
		} 
		
		values.put(MenuList.COL_menutypeurl, menu.getMenuTypeUrl());	//5 +id auto
		
    	mContext.getContentResolver().insert(UsersMenuListDB.AuthenticationInfo.CONTENT_URI, values);
    	
	}
	
	
	public void update(final MenuList menu, String selection, final String selectionArgs) {
		
		System.out.println(" MenuListData -- update");
		
		//String selection = Content.COL_id + "=?";
		ContentValues values = new ContentValues(); 
		
		values.put(MenuList.COL_id, menu.getID());
		values.put(MenuList.COL_menuid, menu.getMenuID());
		values.put(MenuList.COL_menutypeid, menu.getMenuTypeID());
		values.put(MenuList.COL_menutypecallname, menu.getMenuTypeCallName());
		
		try{
			ByteArrayOutputStream baos = new ByteArrayOutputStream(); 
			Bitmap bm = menu.getMenuTypeImg();
			bm.compress(Bitmap.CompressFormat.PNG, 100, baos);
			values.put(MenuList.COL_menutypeimg, baos.toByteArray());
		}catch(Exception e) {
			e.printStackTrace();
		}
	
		values.put(MenuList.COL_menutypeurl, menu.getMenuTypeUrl());	//6
		
		
		mContext.getContentResolver().update(UsersMenuListDB.AuthenticationInfo.CONTENT_URI, 
		    values, 
		    selection, 
		    new String[]{selectionArgs} 
			);
		
	}
	
	
	
	public final List<MenuList> query( String selection, final String contentCodee) {
		
		System.out.println(" MenuListData -- query");
		
		// An array specifying which columns to return.
		String columns[] = new String[] { 
				MenuList.COL_id,
				MenuList.COL_menuid,
				MenuList.COL_menutypeid,
				MenuList.COL_menutypecallname,
				MenuList.COL_menutypeimg,
				MenuList.COL_menutypeurl }; //6

		//selection = Content.COL_contentcode + "=?";

		Uri myUri = UsersMenuListDB.AuthenticationInfo.CONTENT_URI;
		Cursor cur = mContext.getContentResolver().query(myUri, columns, selection, new String[]{contentCodee}, null);
		
		List<MenuList> menuListt = new ArrayList<MenuList>();

		// check if found any query result
		if (cur.getCount() > 0) {
			
			cur.moveToFirst();
			int numRows = cur.getCount();
			
			for (int i = 0; i < numRows; ++i) {
			
				MenuList album = new MenuList(); 
				
				album.setID(cur.getInt(cur.getColumnIndex(MenuList.COL_id)));
				album.setMenuID(cur.getString(cur.getColumnIndex(MenuList.COL_menuid)));
				album.setMenuTypeID(cur.getString(cur.getColumnIndex(MenuList.COL_menutypeid)));
				album.setMenuTypeCallName(cur.getString(cur.getColumnIndex(MenuList.COL_menutypecallname)));
				//album.setMenuTypeImg(BitmapFactory.decodeByteArray(bb, 0, bb.length));		//Img
				
				
				//========= New Set MenuTypeImg ==============
				BitmapFactory.Options oo = new BitmapFactory.Options();
			    oo.inPurgeable = true;
				byte[] bb = cur.getBlob(cur.getColumnIndex(MenuList.COL_menutypeimg));
			    album.setMenuTypeImg(BitmapFactory.decodeByteArray(bb, 0, bb.length, oo));
				//============================================
			    
				album.setMenuTypeUrl(cur.getString(cur.getColumnIndex(MenuList.COL_menutypeurl))); 		//6
				
				menuListt.add(album);
				
				
				System.out.println("--- menuListt "+i+" :"+menuListt.get(i).getMenuID());
				System.out.println("--- menuListt "+i+" :"+menuListt.get(i).getMenuTypeID());
				System.out.println("--- menuListt "+i+" :"+menuListt.get(i).getMenuTypeCallName());
				
				if(menuListt.get(i).getMenuTypeImg()!=null){
					System.out.println("menuListt "+i+" : getMenuTypeImg != null");
				}
				
				
				cur.moveToNext();
				
			}
			
			return menuListt;
		}
		
		// close cursor.
		cur.close();
		return null;
	}
	

	
	public void delete( final String selection, final String selectionArgs) {

		System.out.println(" MenuListData -- delete");
		
		//String selection = Content.COL_contentcode +"=?";
		
		Uri myUri = UsersMenuListDB.AuthenticationInfo.CONTENT_URI;
		mContext.getContentResolver().delete(myUri, selection, new String[]{selectionArgs});
		
	}
}
