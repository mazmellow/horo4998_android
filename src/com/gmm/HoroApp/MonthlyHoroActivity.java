//package com.gmm.HoroApp;
//package com.gmm.gmmhoroscope;
//
//import android.app.Activity;
//import android.content.Intent;
//import android.graphics.Bitmap;
//import android.graphics.BitmapFactory;
//import android.graphics.Matrix;
//import android.os.Bundle;
//import android.view.KeyEvent;
//import android.view.MotionEvent;
//import android.view.View;
//import android.view.View.OnClickListener;
//import android.view.View.OnTouchListener;
//import android.view.ViewGroup.LayoutParams;
//import android.widget.ImageView;
//
//public class MonthlyHoroActivity extends Activity implements OnTouchListener, OnClickListener{
//	//go to MonthlyHoroDetailActivity
//	
//	private ImageView imgbanner;
//	private ImageView img, imgSelect, imgNext;
//	private Bitmap bMap;
//	
//	double touchDegree, moveDegree;
//	float difDegree = 0;
//	float currentDegree = 0;
//	
//	int index = 0;
//
//	int imgNo[] = {
//			R.drawable.no1, 
//			R.drawable.no2, 
//			R.drawable.no3, 
//			R.drawable.no4,
//			R.drawable.no5, 
//			R.drawable.no6, 
//			R.drawable.no7, 
//			R.drawable.no8,
//			R.drawable.no9, 
//			R.drawable.no10, 
//			R.drawable.no11, 
//			R.drawable.no12
//			};
//
//    public void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.monthlyhoro);
//        
//        Log.log("----------- MonthlyHoroActivity ----------");
//        
//        imgbanner = (ImageView)findViewById(R.id.imgmonlybanner);
//        img = (ImageView)findViewById(R.id.imgcycle);
//        imgSelect = (ImageView)findViewById(R.id.imgselect);
//        imgNext = (ImageView)findViewById(R.id.imgnext);
//        
//        if(HomeActivity.bitmapBanner != null){
//        	imgbanner.setImageBitmap(HomeActivity.bitmapBanner);
//        }
//        
//        
//        BitmapFactory.Options oo = new BitmapFactory.Options();
//        oo.inPurgeable = true;
//        bMap = BitmapFactory.decodeResource(getResources(), R.drawable.mc_center,oo);
//        
//        img.setOnTouchListener(this);
//        img.setOnClickListener(this);
//        imgNext.setOnClickListener(new OnClickListener() {
//			
//			public void onClick(View v) {
//				Log.log("------- click -----" + index);
//				insertDecorView(TabHoroMonthlyHoroActivity.mLocalActivityManager.startActivity("monthlyhorodetail", 
//						new Intent(MonthlyHoroActivity.this,MonthlyHoroDetailActivity.class)
//					.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
//					.putExtra("RaSri", index)
//					)
//					.getDecorView());
////				TabHoroMonthlyHoroActivity.mLocalActivityManager.destroyActivity("monthlyhoro", true);
//			}
//		});
//	}
//    
//	public boolean onTouch(View v, MotionEvent event) {
//		
//		double zethaOld = 0, zethaNew = 0;
//		
//		switch (event.getAction()) { 
//		case 0:
//			difDegree = 0;
//			zethaOld = Math.atan2(-(event.getY() - 230), event.getX() - 240);
//			touchDegree = zethaOld * (180/Math.PI);
//			break;
//			
//		case 1:
//			currentDegree += difDegree;
//			setRotateAndSelected(currentDegree);
//			break;
//			
//		case 2:
//			
//			zethaNew = Math.atan2(-(event.getY() - 230), event.getX() - 240);
//			moveDegree = zethaNew * (180/Math.PI);
//			if(moveDegree < 0){
//				moveDegree += 360;
//			}
//			
//			difDegree = (float) (moveDegree - touchDegree);
//			moveDegree += currentDegree;
//			setRotate((float)currentDegree+difDegree);
//			setSelected((float)currentDegree+difDegree);
//			break;
//		}
//		return false;
//	}
//	
//	
//	public void setRotate(float degree){
//		Log.log("-------- setRotate -----");
//		Matrix mat = new Matrix();
//        mat.postRotate(-degree);
//        Bitmap bMapRotate = Bitmap.createBitmap(bMap, 0, 0, bMap.getWidth(), bMap.getHeight(), mat, true);
//        img.setImageBitmap(bMapRotate);
//	}
//
//	public void setSelected(float degree){
//		Log.log("------- setSelected -----");
//		float temp = degree + 15;
//		temp = temp%360;
//	 	index = (int)temp/30;
//	 	
//		if(index >= 0 && index < imgNo.length){
//			imgSelect.setImageResource(imgNo[index]);	
//		}
//	}
//	
//	public void setRotateAndSelected(float degree){
//		Log.log("-------- setRotateAndSelected ----");
//		float temp = degree + 15;
//		temp = temp%360;
//	 	index = (int)temp/30;
//	 	
//	 	Matrix mat = new Matrix();
//        mat.postRotate(-(index * 30));
//        Bitmap bMapRotate = Bitmap.createBitmap(bMap, 0, 0, bMap.getWidth(), bMap.getHeight(), mat, true);
//        img.setImageBitmap(bMapRotate);
//	 	
//	}
//	
//	public void onClick(View v) {
//		Log.log("click " +  v.getId());
//	}
//	
//	private void insertDecorView(View view) {
//	   	 
//    	getParent().setContentView(view, new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
//    }
//
//	
//	public boolean onKeyDown(int keyCode, KeyEvent event) {
//		Log.log("---------- onKeyDown MonthlyHoroActivity ---------");
//		if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
//
//	        return true;
//	    }
//
//	    return super.onKeyDown(keyCode, event);
//	}
//	
//}
