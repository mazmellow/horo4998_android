package com.gmm.HoroApp;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

public class MonthlyHoroData {
	
	private Context mContext;
	
	public MonthlyHoroData(Context context){
		mContext = context;
	}


	public void insert(final ContentHoro horo) {
		
		System.out.println(" MonthlyHoroData -- insert");
		
		System.out.println("horo.getID() :"+horo.getID());
		
		ContentValues values = new ContentValues();	
		
		values.put(ContentHoro.COL_id, horo.getID());
		values.put(ContentHoro.COL_title, horo.getTitle());
		values.put(ContentHoro.COL_desc, horo.getDesc());
		values.put(ContentHoro.COL_date, horo.getDate());		//4
		
    	mContext.getContentResolver().insert(UsersMonthlyHoroDB.AuthenticationInfo.CONTENT_URI, values);
    	
	}
	
	
	public void update(final ContentHoro horo, String selection, final String selectionArgs) {
		
		System.out.println(" MonthlyHoroData -- update");
		
		//String selection = Content.COL_id + "=?";
		ContentValues values = new ContentValues(); 
		
		values.put(ContentHoro.COL_id, horo.getID());
		values.put(ContentHoro.COL_title, horo.getTitle());
		values.put(ContentHoro.COL_desc, horo.getDesc());
		values.put(ContentHoro.COL_date, horo.getDate());		//4
		
		mContext.getContentResolver().update(UsersMonthlyHoroDB.AuthenticationInfo.CONTENT_URI, 
		    values, 
		    selection, 
		    new String[]{selectionArgs} 
			);
	}
	
	
	
	
	
	public final List<ContentHoro> query( String selection, final String contentCodee) {
		
		System.out.println(" MonthlyHoroData -- query");
		
		// An array specifying which columns to return.
		String columns[] = new String[] { 
				ContentHoro.COL_id,
				ContentHoro.COL_title,
				ContentHoro.COL_desc,
				ContentHoro.COL_date }; //4

		//selection = Content.COL_contentcode + "=?";

		Uri myUri = UsersMonthlyHoroDB.AuthenticationInfo.CONTENT_URI;
		Cursor cur = mContext.getContentResolver().query(myUri, columns, selection, new String[]{contentCodee}, null);
		
		List<ContentHoro> monthlyHoroListt = new ArrayList<ContentHoro>();

		// check if found any query result
		if (cur.getCount() > 0) {
			
			cur.moveToFirst();
			int numRows = cur.getCount();
			
			for (int i = 0; i < numRows; ++i) {
			
				ContentHoro monthlyHoro = new ContentHoro(); 
				
				monthlyHoro.setID(cur.getString(cur.getColumnIndex(ContentHoro.COL_id)));
				monthlyHoro.setTitle(cur.getString(cur.getColumnIndex(ContentHoro.COL_title)));
				monthlyHoro.setDesc(cur.getString(cur.getColumnIndex(ContentHoro.COL_desc)));
				monthlyHoro.setDate(cur.getString(cur.getColumnIndex(ContentHoro.COL_date)));		//4
				
				
				monthlyHoroListt.add(monthlyHoro);
				
				
				System.out.println("--- monthlyHoroListt "+i+" :"+monthlyHoroListt.get(i).getID());
				System.out.println("monthlyHoroListt "+i+" :"+monthlyHoroListt.get(i).getTitle());
				System.out.println("monthlyHoroListt "+i+" :"+monthlyHoroListt.get(i).getDesc());
				System.out.println("monthlyHoroListt "+i+" :"+monthlyHoroListt.get(i).getDate());
				
				
				cur.moveToNext();
				
			}
			
			return monthlyHoroListt;
		}
		
		// close cursor.
		cur.close();
		return null;
	}
	

	
	public void delete( final String selection, final String selectionArgs) {

		System.out.println(" MonthlyHoroData -- delete");
		
		//String selection = Content.COL_contentcode +"=?";
		
		Uri myUri = UsersMonthlyHoroDB.AuthenticationInfo.CONTENT_URI;
		mContext.getContentResolver().delete(myUri, selection, new String[]{selectionArgs});
		
	}
}