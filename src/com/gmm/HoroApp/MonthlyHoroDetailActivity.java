package com.gmm.HoroApp;


import java.util.ArrayList;
import java.util.List;

import com.gmm.HoroApp.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ViewFlipper;

public class MonthlyHoroDetailActivity extends Activity{

	private float downXValue;
	private int Zodiac;

	private MonthlyHoroData monthlyHoroData;
	private List<ContentHoro> listContent;
	
	ViewFlipper flipper;
	ImageView imgHead;
	WebView webview;
	Button btnSharefb;
	
	private String summary;
	private int firstRasri;
	
	int header[] = {
			R.drawable.mh_1,
			R.drawable.mh_2,
			R.drawable.mh_3,
			R.drawable.mh_4,
			R.drawable.mh_5,
			R.drawable.mh_6,
			R.drawable.mh_7,
			R.drawable.mh_8,
			R.drawable.mh_9,
			R.drawable.mh_10,
			R.drawable.mh_11,
			R.drawable.mh_12
			
	};
	
	String rasri[] = {
			"aries",
			"taurus", 
			"germini",
			"cancer",
			"leo",
			"virgo",
			"libra",
			"scorpio", 
			"sagittarius", 
			"capricorn", 
			"aquairus", 
			"pisces"
	};
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.monthlyhorodetail);
        
//        flipper = (ViewFlipper)findViewById(R.id.flippers);
        imgHead = (ImageView)findViewById(R.id.imgheadrasri);
        webview = (WebView)findViewById(R.id.webviews);
        btnSharefb = (Button)findViewById(R.id.btnfb);
        
        // ======= query Content Database ======
        monthlyHoroData = new MonthlyHoroData(this);
		listContent = new ArrayList<ContentHoro>();
		
		try{
			listContent =  monthlyHoroData.query(null, null);
        }catch (Exception e) {
        	e.printStackTrace();
		}
        
        // ========== Get index rasi =====
        Bundle rara = getIntent().getExtras();
		if(rara != null){
			Zodiac = rara.getInt("RaSri");
			firstRasri = rara.getInt("RaSri");
			System.out.println("Rasri : " + Zodiac);
			
			String zodiac = ""+   (Zodiac + 1);
			String zodiacDetail = "";
			for(int i = 0; i < listContent.size(); i++){
				if(zodiac.equals(listContent.get(i).getID())){
					zodiacDetail = listContent.get(i).getDesc();
				}
			}
			
			
			imgHead.setImageResource(header[Zodiac]);
			summary = "<html><head><meta http-equiv=Content-type content=text/html; charset=utf-8 />" +
						"<meta name = viewport content = width=device-width></head><body bgcolor='#dddddd'>"+
						zodiacDetail+"</body></html>";
		}
        
        // ======= Set Webview ========
		//webview.loadData(summary, "text/html", "UTF-8");
		
		webview.loadData(summary, "text/html; charset=UTF-8", null);
		
		webview.getSettings().setBuiltInZoomControls(false);
		
		btnSharefb.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				Log.log("---------- click ------");
				String sharefacebook = "http://www.facebook.com/sharer.php?u=http://4998horo.gmember.com/share/monthly/"+ rasri[Zodiac];
				Intent in = new Intent(MonthlyHoroDetailActivity.this, WebFullScreenActivity.class);
				in.putExtra("URL_webview", sharefacebook);
				startActivity(in); 
			}
		});
    }
	

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		Log.log("----------- onKeyDown FlipperActivity -----");
	    if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
	    	
	    	finish();
	    	onDestroy();
	    }
		return super.onKeyDown(keyCode, event);
	}
}
