package com.gmm.HoroApp;

import java.util.ArrayList;
import java.util.List;

import org.anddev.andengine.engine.Engine;
import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.engine.handler.IUpdateHandler;
import org.anddev.andengine.engine.options.EngineOptions;
import org.anddev.andengine.engine.options.EngineOptions.ScreenOrientation;
import org.anddev.andengine.engine.options.resolutionpolicy.RatioResolutionPolicy;
import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.entity.scene.background.ColorBackground;
import org.anddev.andengine.entity.sprite.Sprite;
import org.anddev.andengine.entity.util.FPSLogger;
import org.anddev.andengine.opengl.texture.Texture;
import org.anddev.andengine.opengl.texture.TextureOptions;
import org.anddev.andengine.opengl.texture.region.TextureRegion;
import org.anddev.andengine.opengl.texture.region.TextureRegionFactory;
import org.anddev.andengine.ui.activity.BaseGameActivity;

import android.content.Intent;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup.LayoutParams;

	public class NewMonthlyHoroActivity extends BaseGameActivity{


	private static int CAMERA_WIDTH = 380;
    private static int CAMERA_HEIGHT = 480;
	
    private Camera mCamera;
    private Texture mTexture;
    
    private Sprite spriteBanner;
    private TextureRegion regionBanner;
    
    private Sprite spriteBG;
    private TextureRegion regionBG;
    
    private Sprite spriteBgbottom;
    private TextureRegion regionBgbottom;
    
    private Sprite spriteCenter;
    private TextureRegion regionCenter;
    
    private Sprite spriteButton;
    private TextureRegion regionButton;
    
    private Sprite spriteSelecte;
    private TextureRegion regionSelecte;
    private List<TextureRegion> listRegionSelete;
    private List<Sprite> listSpriteSelecte;
    
    private double touchDegree, moveDegree;
	private float difDegree = 0;
	private float currentDegree = 0;
	
	private float degree = 0;
	private int index = 0;
	private boolean statusFlax = true;
	
	public static boolean monthlyloadcomplete = false;
    
    private String pathImage = "image/";
    
	private String imgSelect[] = {
							"no1.png",
							"no12.png", 
							"no11.png", 
							"no10.png", 
							"no9.png",
							"no8.png", 
							"no7.png", 
							"no6.png", 
							"no5.png",
							"no4.png", 
							"no3.png", 
							"no2.png" 
			};
	
	private	int screenWidth;
	private int screenHeight;
    
    public Engine onLoadEngine() {
    	Log.log("----- onLoadEngine -----");
    	
        listRegionSelete = new ArrayList<TextureRegion>();
        listSpriteSelecte = new ArrayList<Sprite>();
        
        Display display = getWindowManager().getDefaultDisplay(); 
        screenWidth = display.getWidth();  // deprecated
        screenHeight = display.getHeight();  // deprecated
        
        Log.log("======== Screen Width : " + screenWidth);
        Log.log("======== Screen Height : " + screenHeight);
        
        if(screenWidth < 400){
        	CAMERA_WIDTH = 380;
            CAMERA_HEIGHT = 480;
        }else{
        	CAMERA_WIDTH = 380;
            CAMERA_HEIGHT = 480;
        }
        
//        CAMERA_WIDTH = screenWidth;
//        CAMERA_HEIGHT = screenHeight;
        
        
        if(!monthlyloadcomplete){
			Intent in = new Intent(NewMonthlyHoroActivity.this, TabHoroMenuActivity.class);
			in.putExtra("IDTAB", 1);
			startActivity(in);
			
			monthlyloadcomplete = true;
			
			finish();
		}
        
        
		this.mCamera = new Camera(0, 0, CAMERA_WIDTH, CAMERA_HEIGHT);
		return new Engine(new EngineOptions(true, ScreenOrientation.PORTRAIT, new RatioResolutionPolicy(CAMERA_WIDTH, CAMERA_HEIGHT), this.mCamera));
	}
    
    public void onLoadComplete() {
		Log.log("--------- onLoadComplete -------");
		monthlyloadcomplete = false;
	}

	public void onLoadResources() {
        Log.log("-------- onLoadResources -----");
        
        //=========== Set Banner ===============
        this.mTexture = new Texture(512, 512, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.regionBanner = TextureRegionFactory.createFromAsset(this.mTexture, this, pathImage + "banner_default.jpg", 0, 0);
        this.mEngine.getTextureManager().loadTexture(this.mTexture);
		
		//=========== Set Sprite BlackGround =====
    	this.mTexture = new Texture(512, 512, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.regionBG = TextureRegionFactory.createFromAsset(this.mTexture, this, pathImage + "bg_monthlyhoro.jpg", 0, 0);
        this.mEngine.getTextureManager().loadTexture(this.mTexture);
        
		//=========== Set Sprite BlackGround Bottom =====
    	this.mTexture = new Texture(512, 512, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.regionBgbottom = TextureRegionFactory.createFromAsset(this.mTexture, this, pathImage + "mc_bottom_bg.png", 0, 0);
        this.mEngine.getTextureManager().loadTexture(this.mTexture);
	
		//=========== Set Sprite Mc Center =====
    	this.mTexture = new Texture(512, 512, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.regionCenter = TextureRegionFactory.createFromAsset(this.mTexture, this, pathImage + "mc_center_org.png", 0, 0);
        this.mEngine.getTextureManager().loadTexture(this.mTexture);

		//=========== Set Sprite Button =====
    	this.mTexture = new Texture(512, 512, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.regionButton = TextureRegionFactory.createFromAsset(this.mTexture, this, pathImage + "mc_button.png", 0, 0);
        this.mEngine.getTextureManager().loadTexture(this.mTexture);

		//=========== Set Sprite Select =====
        for(int i = 0 ; i< imgSelect.length;i++){

        	this.mTexture = new Texture(512, 512, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
            this.regionSelecte = TextureRegionFactory.createFromAsset(this.mTexture, this, pathImage + imgSelect[i], 0, 0);
            listRegionSelete.add(regionSelecte);
            this.mEngine.getTextureManager().loadTexture(this.mTexture);
        }
	}

	public Scene onLoadScene() {
		System.out.println("------- onLoadScene -----");

        this.mEngine.registerUpdateHandler(new FPSLogger());
        this.mEngine.registerUpdateHandler(new IUpdateHandler() {
            public void onUpdate(float pSecondsElapsed) {
                updateSprite();
            }
 
            public void reset() {
            	System.out.println("------- reset ----");
            }
        });
 
        final Scene scene = new Scene(1);
        scene.setBackground(new ColorBackground(0.2f, 0.3f, 0.4f));
         
        //============ Set BlackGround =========
        spriteBG = new Sprite(0,85,this.regionBG);
        spriteBG.setScale(1.2f);
        scene.attachChild(spriteBG);
        
        //============ Set Banner =========
        spriteBanner = new Sprite(0,0,this.regionBanner);
        scene.attachChild(spriteBanner);
        
        //============ Set BlackGround Bottom =========
        spriteBgbottom = new Sprite((CAMERA_WIDTH - this.regionBgbottom.getWidth()) / 2, 130,this.regionBgbottom);
        spriteBgbottom.setScale(0.8f);
        scene.attachChild(spriteBgbottom);

        //============ Set Mc Center  =========
        spriteCenter = new Sprite((CAMERA_WIDTH - this.regionCenter.getWidth()) / 2, 130,this.regionCenter);
        spriteCenter.setScale(0.8f);
        scene.attachChild(spriteCenter);
        
        //============ Set Button  =========
        spriteButton = new Sprite((CAMERA_WIDTH - this.regionButton.getWidth()) / 2, 320,this.regionButton);
        spriteButton.setScale(0.8f);
        scene.attachChild(spriteButton);
        
        //============ Set Selecte  =========
        for (int i = 0; i < listRegionSelete.size(); i++) {
            spriteSelecte = new Sprite((CAMERA_WIDTH - this.regionSelecte.getWidth()) / 2, 130,this.listRegionSelete.get(i));
            spriteSelecte.setScale(0.8f);
            spriteSelecte.setVisible(false);
            listSpriteSelecte.add(spriteSelecte);
            listSpriteSelecte.get(0).setVisible(true);
            scene.attachChild(listSpriteSelecte.get(i));
		}

        
 
        return scene;
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		
		double zethaOld = 0, zethaNew = 0;
		
		int minX;
		int minY;
		int maxX;
		int maxY;
		int centerX;
		int centerY;
		
		if(screenWidth < 400){
			minX = 100;
			minY = 160;
			maxX = 150;
			maxY = 200;
			
			centerX = 120;
			centerY = 100;
		}else{
//			minX = 140;
//			minY = 430;
			minX = 170;
			minY = 290;
			maxX = 300;
			maxY = 560;
			
			centerX = 490;
			centerY = 235;
		}
		
		
		if(screenHeight > 1000){
			minX *= 2;
			minY *= 2;
			maxX *= 2;
			maxY *= 2;
			centerX *= 2;
			centerY *= 2;
			
			minX += 100;
			minY += 100;
			maxX += 100;
			maxY += 100;
			centerX += 100;
			centerY += 100;
		}
		
		Log.log("X: " + event.getX());
		Log.log("Y: " + event.getY());
		
		if(statusFlax && event.getX() > minX && event.getX() < maxX && event.getY() > minY && event.getY() < maxY){
			
			
		    if(event.getAction() == MotionEvent.ACTION_UP){
		    	if(index == 0){
					index = 12;
				}
				
				int send = 12 - index;
				
				insertDecorView(TabHoroMonthlyHoroActivity.mLocalActivityManager.startActivity("monthlyhorodetail", 
						new Intent(NewMonthlyHoroActivity.this,MonthlyHoroDetailActivity.class)
					.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
					.putExtra("RaSri", send)
					)
					.getDecorView());
		    }
			
			
		}else{
			switch (event.getAction()) { 
			
			case MotionEvent.ACTION_DOWN:
				
				statusFlax = false;
				
				difDegree = 0;
				zethaOld = Math.atan2(event.getY() - centerX, event.getX() - centerY);
				touchDegree = zethaOld * (180/Math.PI);
				break;
				
			case MotionEvent.ACTION_UP:
				
				statusFlax = true;
				
				currentDegree += difDegree;
				float mon = ((currentDegree%360)/30);
				float temp = currentDegree%360 + 15;
			 	index = (int)temp/30;
			 	spriteCenter.setRotation(index * 30);

				break;
				
			case MotionEvent.ACTION_MOVE:
				zethaNew = Math.atan2(event.getY() - centerX, event.getX() - centerY);
				moveDegree = zethaNew * (180/Math.PI);
				if(moveDegree < 0){
					moveDegree += 360;
				}
				
				difDegree = (float) (moveDegree - touchDegree);

				degree = (float)currentDegree+difDegree;
				spriteCenter.setRotation(degree%360);
				setSelected((float)currentDegree+difDegree);
				
				
				break;
			}
		}
		
		return true;
	}
	
	private void updateSprite(){}
	
	public void setSelected(float degree){

		float temp = degree + 15;
		temp = temp%360;
	 	index = (int)temp/30;
	 	
	 	for(int i = 0; i<listSpriteSelecte.size();i++){
	 		if(i == index){
	 			listSpriteSelecte.get(index).setVisible(true);
	 		}else{
	 			listSpriteSelecte.get(i).setVisible(false);
	 		}
	 	}
	}
	
	private void insertDecorView(View view) {
	   	 
    	getParent().setContentView(view, new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
    }
}
