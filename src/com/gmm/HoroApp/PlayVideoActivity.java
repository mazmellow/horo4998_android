package com.gmm.HoroApp;

import com.gmm.HoroApp.R;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.widget.MediaController;
import android.widget.VideoView;

public class PlayVideoActivity extends Activity {
//	 String path ="http://4998horo.gmember.com/contents2012/videoclip/video/198cf363f5c2c2e98dda8dbde12bb28e.mp4";
	 private MediaController ctlr;
	 String LOPA;
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	
    	Bundle jj = getIntent().getExtras();
    	LOPA = jj.getString("link");
    	
        super.onCreate(savedInstanceState);
        setContentView(R.layout.videoview);
        Uri uri=Uri.parse(LOPA);
        VideoView vv = (VideoView)findViewById(R.id.video);
        ctlr = new  MediaController(this);
        ctlr.setMediaPlayer(vv);
        vv.setVideoURI(uri);
        vv.setMediaController(ctlr);
        vv.start();
        
    }
}