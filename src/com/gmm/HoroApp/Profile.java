package com.gmm.HoroApp;

public class Profile {

	public static String COL_msisdn="msisdn";
	public static String COL_flagverified="flagverified";
	public static String COL_birthdate="birthdate";
	
	private String msisdn="";
	private String flagverified="";
	private String birthday="";
	
	public String getMSISDN() {
		return(msisdn);
	}
	
	public void setMSISDN(String msisdn) {
		this.msisdn=msisdn;
	}
	
	
	public String getFlagVerified() {
		return(flagverified);
	}
	
	public void setFlagVerified(String flagverified) {
		this.flagverified=flagverified;
	}
	
	
	public String getBirthdate() {
		return(birthday);
	}
	
	public void setBirthdate(String birthday) {
		this.birthday=birthday;
	}
}
