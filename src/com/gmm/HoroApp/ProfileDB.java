package com.gmm.HoroApp;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;

public class ProfileDB extends ContentProvider{
	
	
	private DatabaseLoginHelper dbLoginHelper;
	private SQLiteDatabase sqliteDB;
	
	
	private static class DatabaseLoginHelper extends SQLiteOpenHelper {

	    private static final String DATABASE_NAME = "GMM.profileDB";
	    private static final int DATABASE_VERSION = 1; // must begin with >= 1
	    private static final String TABLE_NAME = "profile";
	    
	    private final String SQL_CREATE_TABLE = "create table if not exists "+TABLE_NAME+" ("
																		+"msisdn"+" TEXT ,"
																		+"flagverified"+" TEXT ,"
																		+"birthdate"+" TEXT);";
	    
	    
	    public DatabaseLoginHelper(Context context) {
	      super(context, DATABASE_NAME, null, DATABASE_VERSION);
	      
	      //if check DATABASE_VERSION change >> onUpgrade called
	    }

	    
	    public void onCreate(SQLiteDatabase db) {
	      db.execSQL(SQL_CREATE_TABLE);

	    }

	    
	    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
	      //db.execSQL(SQL_DROP_TABLE);
	    }
	}
	
	

	
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		// TODO Auto-generated method stub
		System.out.println("--- ProfileDB -- delete");
		
		sqliteDB = dbLoginHelper.getWritableDatabase();
	    try {
	    	
	        sqliteDB.delete(DatabaseLoginHelper.TABLE_NAME, selection, selectionArgs);
	    } catch (Exception e) {
	    	System.out.println(e.getMessage());
	    } finally {
	      sqliteDB.close();
	    }
		return 0;
	}

	
	public String getType(Uri uri) {
		// TODO Auto-generated method stub
		return null;
	}

	
	public Uri insert(Uri uri, ContentValues values) {
		// TODO Auto-generated method stub
		System.out.println("--- ProfileDB -- insert");
		
		sqliteDB = dbLoginHelper.getWritableDatabase();
	    Uri rowUri = null;
	    try {
	    	
	    	long rowId = sqliteDB.insert(DatabaseLoginHelper.TABLE_NAME, "", values);
	      if (rowId > 0) {
	        
	        /**
	         * ContentUris: Utility methods useful for working with content Uris, those with a "content" scheme. 
	         */
	        rowUri = ContentUris.appendId(UsersProfileDB.AuthenticationInfo.CONTENT_URI.buildUpon(), rowId).build();
	        
	        // notify to allow other threads known and utilize this insert method of content provider
	        getContext().getContentResolver().notifyChange(rowUri, null);
	      }
	    } catch (Exception e) {
	    	System.out.println(e.getMessage());
	    } finally {
	      sqliteDB.close();
	    }
	    return rowUri;
	}

	
	public boolean onCreate() {
		// TODO Auto-generated method stub
		dbLoginHelper = new DatabaseLoginHelper(getContext());
		return false;
	}

	
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {
		// TODO Auto-generated method stub
		System.out.println("--- ProfileDB -- query");
		
		Cursor mCursor = null;
	    try {
	      sqliteDB = dbLoginHelper.getReadableDatabase();
	      /**
	       * public Cursor query (
	       * String table, - name of table 
	       * String[] columns, - column name to return
	       * String selection, - where condition SQL
	       * String[] selectionArgs, - parameter in where condition
	       * String groupBy, 
	       * String having, 
	       * String orderBy) 
	       */
	      
	      mCursor = sqliteDB.query(DatabaseLoginHelper.TABLE_NAME, 
	          projection, 
	          selection, 
	          selectionArgs, 
	          null, 
	          null, 
	          null);
	    } catch (Exception e) {
	      System.out.println(e.getMessage());
	    }
	    
	    return mCursor;
	}

	
	public int update(Uri uri, ContentValues values, String selection,
			String[] selectionArgs) {
		// TODO Auto-generated method stub
		System.out.println("--- ProfileDB -- update");
		
		sqliteDB = dbLoginHelper.getWritableDatabase();
	    try {
	      sqliteDB.update(DatabaseLoginHelper.TABLE_NAME, values, selection, selectionArgs);
	      
	      // notify to allow other threads known and utilize this update method of content provider
	      getContext().getContentResolver().notifyChange(uri, null);
	    } catch (Exception e) {
	    	System.out.println(e.getMessage());
	    } finally {
	      sqliteDB.close();
	    }
		return 0;
	}

}
