package com.gmm.HoroApp;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

public class ProfileData {
	

	private Context mContext;
	
	public ProfileData(Context context){
		mContext = context;
	}
	
	
	public void insert(final Profile profile) {
		
		System.out.println(" ProfileData -- insert");
		
		ContentValues values = new ContentValues();
		values.put(Profile.COL_msisdn, profile.getMSISDN());
		values.put(Profile.COL_flagverified , profile.getFlagVerified());
		values.put(Profile.COL_birthdate , profile.getBirthdate());
		
    	mContext.getContentResolver().insert(UsersProfileDB.AuthenticationInfo.CONTENT_URI, values);
    	
	}
	
	
	public void delete( String selection , final String selectionArgs) {
		
		System.out.println(" ProfileData -- delete");

		Uri myUri = UsersProfileDB.AuthenticationInfo.CONTENT_URI;
		
		//delete all null null
		mContext.getContentResolver().delete(myUri, null, null);
		
	}
	
	
	public final List<Profile> query( String selection, final String selectionArgs) {
		
		System.out.println(" ProfileData -- query");
		
			// An array specifying which columns to return.
			String columns[] = new String[] { Profile.COL_msisdn
											, Profile.COL_flagverified
											, Profile.COL_birthdate};
	
			//String selection = AlbumList.COL_albumid + "=?";
	
			Uri myUri = UsersProfileDB.AuthenticationInfo.CONTENT_URI;
			Cursor cur = null;
			
			try{	
			
			//query all null null
			cur = mContext.getContentResolver().query(myUri, columns, null , null, null);
		
				
			List<Profile> profileList = new ArrayList<Profile>();
	
			// check if found any query result
			if (cur.getCount() > 0) {
				
				cur.moveToFirst();
				Profile profile = new Profile();
					
				profile.setMSISDN(cur.getString(cur.getColumnIndex(Profile.COL_msisdn)));
				profile.setFlagVerified(cur.getString(cur.getColumnIndex(Profile.COL_flagverified)));
				profile.setBirthdate(cur.getString(cur.getColumnIndex(Profile.COL_birthdate)));
					
				profileList.add(profile);
					
					if(profileList.size()>0){
						System.out.println("getMSISDN :"+profileList.get(0).getMSISDN());
						System.out.println("getFlagVerified :"+profileList.get(0).getFlagVerified());
						System.out.println("getBirthday :"+profileList.get(0).getBirthdate());
					}
					
				return profileList;
			
			}
			
			// close cursor.
			cur.close();
			
		} catch (Exception e) {
			
			System.out.println(" ProfileData -- query === error ---"+"cur.close()");
		    System.out.println(e.getMessage());
		}
		
		
		return null;
	}

	
	public void update(final Profile profile ) {
		
		System.out.println(" ProfileData -- update");
		
		String selection = null; // = UserBean.COLUMN_USERNAME + "=?";
		
		ContentValues values = new ContentValues();
		values.put(Profile.COL_msisdn, profile.getMSISDN());
		values.put(Profile.COL_flagverified , profile.getFlagVerified());
		values.put(Profile.COL_birthdate , profile.getBirthdate());
		
		
		mContext.getContentResolver().update(UsersProfileDB.AuthenticationInfo.CONTENT_URI, 
		    values, 
		    selection, 
		    new String[]{null} //new String[]{content.getContentCode()}
			);
	}
}
