package com.gmm.HoroApp;

import java.util.Calendar;

import kankan.wheel.widget.OnWheelChangedListener;
import kankan.wheel.widget.WheelView;
import kankan.wheel.widget.adapters.ArrayWheelAdapter;
import kankan.wheel.widget.adapters.NumericWheelAdapter;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class RegisterActivity extends Activity{

    
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.regis);
        
        System.out.println("----------- RegisterActivity ----------");
        
        //Layout && HTTP Post Register
        
        final Button btnOK = (Button)findViewById(R.id.btnregok);
        final Button btnSkip	= (Button)findViewById(R.id.btnregskip);
        final EditText edtMsisdn = (EditText)findViewById(R.id.edtphonenumber);
//        final DatePicker datePicker = (DatePicker)findViewById(R.id.datePicker);
        
        System.out.println("---------- DateCustom --------");
        //================ Date Custom =============
        Calendar calendar = Calendar.getInstance();

        final WheelView month = (WheelView) findViewById(R.id.month);
        final WheelView year = (WheelView) findViewById(R.id.year);
        final WheelView day = (WheelView) findViewById(R.id.day);
        
        OnWheelChangedListener listener = new OnWheelChangedListener() {
            public void onChanged(WheelView wheel, int oldValue, int newValue) {
                updateDays(year, month, day);
            }
        };

        // month
        int curMonth = calendar.get(Calendar.MONTH);
        String months[] = new String[] {
					        		getString(R.string.month1), 
									getString(R.string.month2), 
									getString(R.string.month3), 
									getString(R.string.month4), 
									getString(R.string.month5),
									getString(R.string.month6), 
									getString(R.string.month7), 
									getString(R.string.month8), 
									getString(R.string.month9), 
									getString(R.string.month10), 
									getString(R.string.month11), 
									getString(R.string.month12)
        							};
        
        month.setViewAdapter(new DateArrayAdapter(this, months, curMonth));
        month.setCurrentItem(curMonth);
        month.addChangingListener(listener);
    
        // year
        int curYear = calendar.get(Calendar.YEAR);
        year.setViewAdapter(new DateNumericAdapter(this, curYear + 543 - 100, curYear + 543, 0));
        year.setCurrentItem(curYear);
        year.addChangingListener(listener);
        
        //day
        updateDays(year, month, day);
        day.setCurrentItem(calendar.get(Calendar.DAY_OF_MONTH) - 1);
        
        
        
        
        System.out.println("----------- Before onclick --------");
        
        
        
        btnOK.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {

				String strMsisdn = "";
 				if(!(edtMsisdn.getText().length() < 10)){
					
 					System.out.println("day "+day.getCurrentItem());
					System.out.println("month "+month.getCurrentItem());
					System.out.println("year "+year.getCurrentItem());
					
					strMsisdn = (year.getCurrentItem() + 2455) +"-"+pad(month.getCurrentItem()+1)+"-"+pad((day.getCurrentItem()+1))   ;
					
					//strMsisdn = datePicker.getYear() + "-" + pad(datePicker.getMonth()+1) + "-" + pad(datePicker.getDayOfMonth());
					
					
					getInsertProfile_Press_OK(edtMsisdn.getText().toString().trim(), strMsisdn);
					
					Intent homeIntent = new Intent(RegisterActivity.this, TabMenuActivity.class);								//
	    			startActivity(homeIntent);
	    			finish();
					
					//getInsertProfile_Press_OK("0818667415", "1896-09-14");
				}else{
					showNoticeDialogBox(getString(R.string.inputmsisdn).toString());
				}
					
				
			}
		});
        
        btnSkip.setOnClickListener( new OnClickListener() {
			
			public void onClick(View v) {
		        getInsertProfile_Press_Skip();
				
		        Intent homeIntent = new Intent(RegisterActivity.this, TabMenuActivity.class);								//
    			startActivity(homeIntent);
    			finish();
		        
			}
		});

    }
    
    
    public void getInsertProfile_Press_OK(String str_msisdn, String str_birthday){
    	
    	
    	str_msisdn = "66"+str_msisdn.substring(1);
    	
    	Profile profile = new Profile();
    	profile.setMSISDN(str_msisdn);
    	profile.setFlagVerified("no");
    	profile.setBirthdate(str_birthday);
    	
    	try{
        	
        	ProfileData profileData = new ProfileData(this);
        	profileData.insert(profile);
        	
        } catch (Exception e) {
			e.printStackTrace();
		}
    	
    }
    
    
    public void getInsertProfile_Press_Skip(){
    	
    	Profile profile = new Profile();
    	profile.setMSISDN("");
    	profile.setFlagVerified("no");
    	profile.setBirthdate("");
    	
    	try{
        	
        	ProfileData profileData = new ProfileData(this);
        	profileData.insert(profile);
        	
        } catch (Exception e) {
			e.printStackTrace();
		}
    	
    }
    
	private static String pad(int c){
		if(c >= 10)
			return String.valueOf(c);
		else
			return "0" + String.valueOf(c);
	}
	
	private void showNoticeDialogBox(final String message) {

        Builder setupAlert;
        setupAlert = new AlertDialog.Builder(this);    
//        setupAlert.setTitle(title);
        setupAlert.setMessage(message);
        setupAlert.setPositiveButton("Ok",
            new DialogInterface.OnClickListener() {
              public void onClick(DialogInterface dialog, int whichButton) {
            	  
              }
            });
        setupAlert.show();
      }
	
	 /**
     * Updates day wheel. Sets max days according to selected month and year
     */
    void updateDays(WheelView year, WheelView month, WheelView day) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, calendar.get(Calendar.YEAR) + year.getCurrentItem());
        calendar.set(Calendar.MONTH, month.getCurrentItem());
        
        int maxDays = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        day.setViewAdapter(new DateNumericAdapter(this, 1, maxDays, calendar.get(Calendar.DAY_OF_MONTH) - 1));
        int curDay = Math.min(maxDays, day.getCurrentItem() + 1);
        day.setCurrentItem(curDay - 1, true);
    }
    
    /**
     * Adapter for numeric wheels. Highlights the current value.
     */
    private class DateNumericAdapter extends NumericWheelAdapter {
        // Index of current item
        int currentItem;
        // Index of item to be highlighted
        int currentValue;
        
        /**
         * Constructor
         */
        public DateNumericAdapter(Context context, int minValue, int maxValue, int current) {
            super(context, minValue, maxValue);
            this.currentValue = current;
            setTextSize(16);
        }
        
        @Override
        protected void configureTextView(TextView view) {
            super.configureTextView(view);
            if (currentItem == currentValue) {
//                view.setTextColor(0xFF0000F0);
            }
            view.setTypeface(Typeface.SANS_SERIF);
        }
        
        @Override
        public View getItem(int index, View cachedView, ViewGroup parent) {
            currentItem = index;
            return super.getItem(index, cachedView, parent);
        }
    }
    
    /**
     * Adapter for string based wheel. Highlights the current value.
     */
    private class DateArrayAdapter extends ArrayWheelAdapter<String> {
        // Index of current item
        int currentItem;
        // Index of item to be highlighted
        int currentValue;
        
        /**
         * Constructor
         */
        public DateArrayAdapter(Context context, String[] items, int current) {
            super(context, items);
            this.currentValue = current;
            setTextSize(16);
        }
        
        @Override
        protected void configureTextView(TextView view) {
            super.configureTextView(view);
            if (currentItem == currentValue) {
//                view.setTextColor(0xFF0000F0);
            }
            view.setTypeface(Typeface.SANS_SERIF);
        }
        
        @Override
        public View getItem(int index, View cachedView, ViewGroup parent) {
            currentItem = index;
            return super.getItem(index, cachedView, parent);
        }
    }
	
}
