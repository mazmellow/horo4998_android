package com.gmm.HoroApp;

import com.gmm.HoroApp.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;

public class RegisterStepActivity extends Activity{

	private ImageView imgbanner, imgregisterstep;
	
	protected void onCreate(Bundle savedInstanceState) {
		
		Log.log("-------------- RegisterStepActivity ----------");
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.registerstep);
		
		imgbanner = (ImageView)findViewById(R.id.imgregisterstepbanner);
		imgregisterstep = (ImageView)findViewById(R.id.imgregisterstep);
		
		if(HomeActivity.bitmapBanner != null){
			imgbanner.setImageBitmap(HomeActivity.bitmapBanner);
		}
		if(HomeActivity.bitmapHowToRegister != null){
			Log.log("bitmapHowToRegister != null");
			imgregisterstep.setImageBitmap(HomeActivity.bitmapHowToRegister);
		}
	}

	
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		
	    if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {

	    	insertDecorView(TabHomeActivityGroup.mLocalActivityManager.startActivity("home", 
					new Intent(RegisterStepActivity.this,HomeActivity.class)).
					
					getDecorView());
	    	//TabHomeActivityGroup.mLocalActivityManager.destroyActivity("registerstep", true);
	    	
	        return true;
	    }

	    return super.onKeyDown(keyCode, event);
		
	}

	private void insertDecorView(View view) {
    	
    	getParent().setContentView(view, new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
    }
	
}
