package com.gmm.HoroApp;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.gmm.HoroApp.R;

import android.content.Context;
import android.graphics.Bitmap;

/**
 * This class is an adapter that provides images from a fixed set of resource
 * ids. Bitmaps and ImageViews are kept as weak references so that they can be
 * cleared by garbage collection when not needed.
 * 
 */
public class ResourceImageAdapter extends AbstractCoverFlowImageAdapter {

    /** The Constant TAG. */
    private static final String TAG = ResourceImageAdapter.class.getSimpleName();

    /** The Constant DEFAULT_LIST_SIZE. */
    private static final int DEFAULT_LIST_SIZE = 20;

    /** The Constant IMAGE_RESOURCE_IDS. */
    private static final List<Integer> IMAGE_RESOURCE_IDS = new ArrayList<Integer>(DEFAULT_LIST_SIZE);

    /** The Constant DEFAULT_RESOURCE_LIST. */
    private static final int[] DEFAULT_RESOURCE_LIST = { 
    
    	R.drawable.ic_launcher, 
    	R.drawable.ic_launcher, 
    	R.drawable.ic_launcher,
        R.drawable.ic_launcher 
        
    };

    /** The bitmap map. */
    private final Map<Integer, WeakReference<Bitmap>> bitmapMap = new HashMap<Integer, WeakReference<Bitmap>>();

    private final Context context;

    
    //Bitmap[]  bitm;
    List<MenuList> listMenuList = new ArrayList<MenuList>();   
    /**
     * Creates the adapter with default set of resource images.
     * 
     * @param context
     *            context
     */
    
    int allmenuhoro;
    int nullmenuhoro=0;
    int realmenuhoro;
    int khoro;
    
    
    public ResourceImageAdapter(final Context context,List<MenuList> listMenulist) {
        super();
        this.context = context;
        //bitm = new Bitmap[listconfig.size()+1];
        listMenuList = listMenulist;
        
        allmenuhoro = listMenulist.size();
        
        for(khoro=0;khoro<listMenulist.size();khoro++){
        	if(listMenulist.get(khoro).getMenuTypeImg() == null){
        		nullmenuhoro = nullmenuhoro+1;
        	}
        }
      
        realmenuhoro = allmenuhoro-nullmenuhoro;
        
        System.out.println(realmenuhoro+"RealllllllllllllllllllllMenu");
        
        setResources(DEFAULT_RESOURCE_LIST);
    }

    /**
     * Replaces resources with those specified.
     * 
     * @param resourceIds
     *            array of ids of resources.
     */
    public final synchronized void setResources(final int[] resourceIds) {
        IMAGE_RESOURCE_IDS.clear();
        for (final int resourceId : resourceIds) {
            IMAGE_RESOURCE_IDS.add(resourceId);
        }
        notifyDataSetChanged();
    }

    /*
     * (non-Javadoc)
     * 
     * @see android.widget.Adapter#getCount()
     */
    
    public synchronized int getCount() {
//        return IMAGE_RESOURCE_IDS.size();
    	return realmenuhoro;
    }

    /*
     * (non-Javadoc)
     * 
     * @see pl.polidea.coverflow.AbstractCoverFlowImageAdapter#createBitmap(int)
     */
    @Override
    protected Bitmap createBitmap(final int position) {
    	System.out.println("-----crateBitmap  " + position);
    	Bitmap bitmap;
    	int ihoro;
    	int jhoro;
    	String StoreMenu[];
    	StoreMenu = new String[listMenuList.size()];	
    	String kwonhoro;
    	int StorePosition[];
    	StorePosition = new int[listMenuList.size()];

    		
    	for(ihoro=0;ihoro<listMenuList.size();ihoro++){
    		StoreMenu[ihoro]=listMenuList.get(ihoro).getMenuTypeCallName();
    	}
    	
    	for(jhoro=0;jhoro<listMenuList.size();jhoro++){
    		kwonhoro = StoreMenu[jhoro];    		

    		if(kwonhoro.equals("Monthly Horo")){		
    			StorePosition[0] = jhoro;
    		}else if(kwonhoro.equals("Love Meter")){	
    			StorePosition[1] = jhoro;     			
    		}else if(kwonhoro.equals("Lucky Color")){
    			StorePosition[2] = jhoro;
    		}else if(kwonhoro.equals("Video Clips")){
    			StorePosition[3] = jhoro;   		
    		}else{
    			StorePosition[jhoro] = jhoro;  			
    		}
    	}
    	
    	bitmap = listMenuList.get(StorePosition[position])/*get(StoreMenu[position])*/.getMenuTypeImg();
    	
    	if(bitmap == null){
    		System.out.println("bitmap == null" +position); 
    		bitmap = HoroscopeActivity.bitmapCoverFlow;
    	}
    	
    	bitmapMap.put(position, new WeakReference<Bitmap>(bitmap));
    	
    	return bitmap;

    }
}