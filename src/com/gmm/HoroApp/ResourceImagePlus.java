package com.gmm.HoroApp;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.gmm.HoroApp.R;

import android.content.Context;
import android.graphics.Bitmap;

/**
 * This class is an adapter that provides images from a fixed set of resource
 * ids. Bitmaps and ImageViews are kept as weak references so that they can be
 * cleared by garbage collection when not needed.
 * 
 */
public class ResourceImagePlus extends AbstractCoverFlowImagePlus implements ImageReceivedCallback{

    /** The Constant TAG. */
    private static final String TAG = ResourceImagePlus.class.getSimpleName();

    /** The Constant DEFAULT_LIST_SIZE. */
    private static final int DEFAULT_LIST_SIZE = 20;

    /** The Constant IMAGE_RESOURCE_IDS. */
    private static final List<Integer> IMAGE_RESOURCE_IDS = new ArrayList<Integer>(DEFAULT_LIST_SIZE);

    /** The Constant DEFAULT_RESOURCE_LIST. */
    private static final int[] DEFAULT_RESOURCE_LIST = { 
    
    	R.drawable.ic_launcher, 
    	R.drawable.ic_launcher, 
    	R.drawable.ic_launcher,
        R.drawable.ic_launcher 
        
    };

    /** The bitmap map. */
    private final Map<Integer, WeakReference<Bitmap>> bitmapMap = new HashMap<Integer, WeakReference<Bitmap>>();

    private final Context context;

   
    List<MenuList> listMenuList = new ArrayList<MenuList>();
    
    /**
     * Creates the adapter with default set of resource images.
     * 
     * @param context
     *            context
     */
    
    int allmenuplus;
    int nullmenuplus=0;
    int realmenuplus;
    int kplus;
    
    private ImageReceivedCallback imageRenderCallback = ResourceImagePlus.this;
    
    public ResourceImagePlus(final Context context,List<MenuList> listMenulist) {
        super();
        this.context = context;
        
        listMenuList = listMenulist;
        
        allmenuplus = listMenulist.size();
        
//        for(kplus=0;kplus<listMenulist.size();kplus++){
//        	if(listMenulist.get(kplus).getMenuTypeImg() == null){
//        		nullmenuplus = nullmenuplus+1;
//        	}
//        }
      
        realmenuplus = allmenuplus-nullmenuplus;
        
//        System.out.println(realmenuplus+"RealllllllllllllllllllllMenu");
        
        setResources(DEFAULT_RESOURCE_LIST);
    }

    /**
     * Replaces resources with those specified.
     * 
     * @param resourceIds
     *            array of ids of resources.
     */
    public final synchronized void setResources(final int[] resourceIds) {
        IMAGE_RESOURCE_IDS.clear();
        for (final int resourceId : resourceIds) {
            IMAGE_RESOURCE_IDS.add(resourceId);
        }
        notifyDataSetChanged();
    }

    /*
     * (non-Javadoc)
     * 
     * @see android.widget.Adapter#getCount()
     */
    
    public synchronized int getCount() {
        return realmenuplus;
    }

    /*
     * (non-Javadoc)
     * 
     * @see pl.polidea.coverflow.AbstractCoverFlowImageAdapter#createBitmap(int)
     */
    @Override
    protected Bitmap createBitmap(final int position) {
//        Log.v(TAG, "creating item " + position);
//        final Bitmap bitmap = ((BitmapDrawable) context.getResources().getDrawable(IMAGE_RESOURCE_IDS.get(position)))
//                .getBitmap();
//        bitmapMap.put(position, new WeakReference<Bitmap>(bitmap));
//        return bitmap;
    	 
//    	System.out.println("-----crateBitmap  " + position);
    	Bitmap bitmap;

    	
    	int iplus;
    	int jplus;
    	String StoreMenuplus[];
    	StoreMenuplus = new String[listMenuList.size()];	
    	String kwonplus;
    	int StorePositionplus[];
    	StorePositionplus = new int[listMenuList.size()];
    		
    	for(iplus=0;iplus<listMenuList.size();iplus++){
    		StoreMenuplus[iplus]=listMenuList.get(iplus).getMenuTypeCallName();
    	}
    	
    	for(jplus=0;jplus<listMenuList.size();jplus++){
    		kwonplus = StoreMenuplus[jplus];    

    		if(kwonplus.equals("Weekly Horo")){		
    			StorePositionplus[0] = jplus;
    		}else if(kwonplus.equals("Auspicious Day")){	
    			StorePositionplus[1] = jplus;     			
    		}else if(kwonplus.equals("Lucky Number")){
    			StorePositionplus[2] = jplus;
    		}else if(kwonplus.equals("Special Video Clips")){
    			StorePositionplus[3] = jplus;   		
    		}else{
    			StorePositionplus[jplus] = jplus;    			
    		}
    	}
    	
    	bitmap = listMenuList.get(StorePositionplus[position]).getMenuTypeImg();
    	
    	if(bitmap == null){
    		System.out.println("bitmap == null" +position); 
    		bitmap = HoroscopeActivity.bitmapCoverFlow;
    	}
    	
//    	bitmap = listMenuList.get(3).getMenuTypeImg();
    	
    	bitmapMap.put(position, new WeakReference<Bitmap>(bitmap));

    	return bitmap;
    }


	public void onImageReceived(ImageDisplayer displayer) {
		// TODO Auto-generated method stub
		
	}
}