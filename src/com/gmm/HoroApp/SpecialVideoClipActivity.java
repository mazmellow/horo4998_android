package com.gmm.HoroApp;


import java.util.ArrayList;
import java.util.List;

import com.gmm.HoroApp.R;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.telephony.TelephonyManager;
import android.text.Html;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class SpecialVideoClipActivity extends Activity implements Runnable, ImageReceivedCallback{

	List<ContentHoro> videoList = new ArrayList<ContentHoro>();
	
	String MSISDN = null;
	String MENU_ID= null;
	String DEVICE = null;
	
	String LinkHoro = null;
	
	private ProgressDialog pd;

	private ListView listVideO;
	private VideoListAdapter adapter;
	private ImageReceivedCallback imageRenderCallback = SpecialVideoClipActivity.this;
	private ImageView imgbanner;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	
    	ProfileData msisdndata = new ProfileData(this);
    	List<Profile> storemsisdn = new ArrayList<Profile>();
    	
    	try{
        	storemsisdn =  msisdndata.query(null, null);
        }catch (Exception e) {
        	e.printStackTrace();
    	}
        
        MSISDN = storemsisdn.get(0).getMSISDN();
        
        MenuPlusListData menudata = new MenuPlusListData(this);
    	List<MenuList> storemenu = new ArrayList<MenuList>();
    	
    	try{
        	storemenu =  menudata.query(null, null);
        }catch (Exception e) {
        	e.printStackTrace();
    	}
        
    	// old
        //MENU_ID = storemenu.get(1).getMenuID();
    	MENU_ID = "5";
        
        DEVICE = getDeviceID();
    	
        super.onCreate(savedInstanceState);
        
        setContentView(R.layout.specialvideoclip);
        
        
		listVideO = (ListView)findViewById(R.id.list_video);
		imgbanner = (ImageView)findViewById(R.id.imgspecialvidiobanner);
		
		
		if(HomeActivity.bitmapBanner != null){
			imgbanner.setImageBitmap(HomeActivity.bitmapBanner);
		}
	
		pd = ProgressDialog.show(getParent(), "Loading..", "Please wait..", true, false);
		
        Thread thread = new Thread(this);
        thread.start();
	        
	        listVideO.setOnItemClickListener(new OnItemClickListener() {

				public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,long arg3) {
				 						
					if(videoList != null){
						Intent llo = new Intent(SpecialVideoClipActivity.this,PlayVideoActivity.class);
							llo.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
							llo.putExtra("link", videoList.get(arg2).getClip());
							startActivity(llo);

					}
					
				}
			});
		}

	public void onImageReceived(ImageDisplayer displayer) {
		if(displayer!=null){
    		this.runOnUiThread(displayer);
    	}
	}

	public void run() {
		try {
	        
			videoList = HttpPostContent.executeHttpPost(DEVICE,MSISDN,MENU_ID);
			
			pd.dismiss();
				
		} catch (Exception e) {
			pd.dismiss();
			e.printStackTrace();
		}
        
        handler.sendEmptyMessage(0);
	}
	
	private Handler handler = new Handler() {
	    @Override
	    public void handleMessage(Message msg) {
	    	
	    	if(videoList != null){
	    		try {

		            if(videoList.size()>0){
		            	adapter = new VideoListAdapter();
		            	listVideO.setAdapter(adapter);
		            	 	
		            	pd.dismiss();
		            }
		            
		    	}
		    	catch (Exception e) {
		    		
		    		pd.dismiss();
		    		
					e.printStackTrace();
				}
	    	}	
	    }
	};

	class VideoListAdapter extends ArrayAdapter<ContentHoro> {

		public VideoListAdapter() { 
			super(SpecialVideoClipActivity.this, R.layout.specialvideoclip_row, videoList);
		}
		
		@Override
		public View getView(int position, View convertView,ViewGroup parent){
			View row = convertView;
			VideoListWrapper wrapper = null;
			
			if(row == null){
				LayoutInflater inflater = getLayoutInflater();
				row = inflater.inflate(R.layout.specialvideoclip_row, null);
				wrapper = new VideoListWrapper(row);
				row.setTag(wrapper);
			}else{
				wrapper = (VideoListWrapper)row.getTag();
			}
			wrapper.populateFrom(videoList.get(position), position);
			return row;
		}
	}
	
	class VideoListWrapper {
		
		private View row=null;
		private ImageView imgVideo = null;
		private TextView videoName = null;
		private TextView webview = null;
		
		
		VideoListWrapper(View row) {
			this.row=row;
		}
		
		private void populateFrom(ContentHoro videoList, int pos) {
			try {
				new ImageReceiver(videoList.getThumbImg(),imageRenderCallback,getImageVideo());
			} catch (Exception e) {
				e.printStackTrace();
			
			}

				LinkHoro = videoList.getClip();
				
			
				getVideoName().setText(videoList.getTitle());

				getWebTextView().setText(Html.fromHtml(videoList.getDesc().toString()));
				
				if(getWebTextView().getText().toString().length() > 74){
					getWebTextView().setText(getWebTextView().getText().toString().substring(0, 74)+" . . .");	
				}else{
					getWebTextView().setText(getWebTextView().getText().toString().substring(0, getWebTextView().getText().toString().length())+" . . .");
				}
				
				
				
				
		}
		
		  private ImageView getImageVideo(){
		    	if(imgVideo == null){
		    		imgVideo  = (ImageView)row.findViewById(R.id.img_rowvideo);
		    	}
		    	
		    	return imgVideo;
		    }
		  
		  private TextView getVideoName(){
		    	if(videoName==null){
		    		videoName = (TextView)row.findViewById(R.id.videoname);
		    		System.out.println(videoName);
		    	}
		    	return videoName;
		    }
		  
		  private TextView getWebTextView(){
			  if(webview == null){
				  webview =(TextView)row.findViewById(R.id.videodetail);
			  }
			  return webview;
		  }
		 
	}
	
	public String getDeviceID(){
    	
  	  String str_device = null;
			try{
	        	TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
	        	str_device= telephonyManager.getDeviceId();
	        } catch (Exception e) {
				e.printStackTrace();
			}
	        try{
				if(str_device==null || str_device.equals("")){
					WifiManager wifiMan = (WifiManager)this.getSystemService(Context.WIFI_SERVICE);
		    		WifiInfo wifiInf = wifiMan.getConnectionInfo();
		    		str_device = wifiInf.getMacAddress();
				}
	    	} catch (Exception e) {
				e.printStackTrace();
			}
		return str_device;
	}
	
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)  {
	    if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
	    	
	    	
	    	 	
	    }

	    return super.onKeyDown(keyCode, event);
	}
       
}