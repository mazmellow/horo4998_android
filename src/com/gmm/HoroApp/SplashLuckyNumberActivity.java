package com.gmm.HoroApp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;

public class SplashLuckyNumberActivity extends Activity{
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splashluckynumber);
		
		ImageView img = (ImageView)findViewById(R.id.img_next);
		img.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				
//				Intent in = new Intent(SplashLuckyNumberActivity.this, TabPlusLuckyNumberActivity.class);
//				in.putExtra("value", "luckynumber");
//				in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//				startActivity(in);
				System.out.println("----- click ----");
				insertDecorView(TabPlusLuckyNumberActivity.mLocalActivityManager
						.startActivity("tabplusluckynumber", new Intent(SplashLuckyNumberActivity.this,LuckyNumberActivity.class)
						.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP))
						.getDecorView());
				System.out.println("---- end ---");
			}
		});
	}
	
	private void insertDecorView(View view) {
	   	 
    	getParent().setContentView(view, new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
    }
}
