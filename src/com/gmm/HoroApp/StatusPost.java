package com.gmm.HoroApp;

public class StatusPost {
	
	private String status="";
	private String status_code="";
	private String status_detail="";
	
	private String ZODIAC="";
	private String ZODIAC_ID="";
	private String BIRTHDATE="";
	
	
	public String getStatus() {
		return(status);
	}
	
	public void setStatus(String status) {
		this.status=status;
	}
	
	
	public String getStatusCode() {
		return(status_code);
	}
	
	public void setStatusCode(String status_code) {
		this.status_code=status_code;
	}
	
	
	public String getStatusDetail() {
		return(status_detail);
	}
	
	public void setStatusDetail(String status_detail) {
		this.status_detail=status_detail;
	}
	
	
	
	
	public String getZODIAC() {
		return(ZODIAC);
	}
	
	public void setZODIAC(String ZODIAC) {
		this.ZODIAC=ZODIAC;
	}
	
	
	public String getZODIAC_ID() {
		return(ZODIAC_ID);
	}
	
	public void setZODIAC_ID(String ZODIAC_ID) {
		this.ZODIAC_ID=ZODIAC_ID;
	}
	
	
	public String getBIRTHDATE() {
		return(BIRTHDATE);
	}
	
	public void setBIRTHDATE(String BIRTHDATE) {
		this.BIRTHDATE=BIRTHDATE;
	}
	
	

}
