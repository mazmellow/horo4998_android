package com.gmm.HoroApp;

import com.gmm.HoroApp.R;

import android.app.ActivityGroup;
import android.app.AlertDialog;
import android.app.LocalActivityManager;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;

public class TabHomeActivityGroup extends ActivityGroup {
	
	static String page_home;
	
	public static LocalActivityManager mLocalActivityManager; 
	
	 
	protected void onCreate(Bundle savedInstanceState) { 
	      super.onCreate(savedInstanceState); 
	      
	      System.out.println("__________ TabHomeActivityGroup __________");
	      
	      mLocalActivityManager = getLocalActivityManager();
	      View view = mLocalActivityManager 
	                                .startActivity("home", new Intent(this,HomeActivity.class) 
	                                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)) 
	                                .getDecorView(); 
	       this.setContentView(view); 
	       
	}
	
	
	
	
	public boolean onKeyDown(int keyCode, KeyEvent event)  {
    	
    	System.out.println("onKeyDown ________ TabHomeActivityGroup ___________");
    	
	    if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
	    	
	    	if(mLocalActivityManager.getCurrentId().equals("registerstep")){
	    		
	    		mLocalActivityManager = getLocalActivityManager();
	  	      View view = mLocalActivityManager 
	  	                                .startActivity("home", new Intent(this,HomeActivity.class) 
	  	                                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)) 
	  	                                .getDecorView(); 
	  	       this.setContentView(view); 
//	  	     mLocalActivityManager.destroyActivity("registerstep", true);
		        return true;
			}
	    	
	    	else if(mLocalActivityManager.getCurrentId().equals("usestep")){
	    		
	    		mLocalActivityManager = getLocalActivityManager();
		  	      View view = mLocalActivityManager 
		  	                                .startActivity("home", new Intent(this,HomeActivity.class) 
		  	                                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)) 
		  	                                .getDecorView(); 
		  	       this.setContentView(view); 
//		  	     mLocalActivityManager.destroyActivity("usestep", true);
	    		
	    		return true;
	    	}
	    	
	    	else{
	    		showYesNoExitDialogBox(getString(R.string.title_exit), getString(R.string.msg_exit));
	    	}
	    	
	    	/*
	    	if(page_home.equals("Howto")){
	    		
			      View view = mLocalActivityManager 
			                                .startActivity("home", new Intent(this,HomeActivity.class) 
			                                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)) 
			                                .getDecorView(); 
			       this.setContentView(view); 
				
		    	mLocalActivityManager.destroyActivity("howto", true);
		        return true;
		        
	    	}else if(page_home.equals("Home")){
	    		
	    		//-alert exit-//
	        	String title = getString(R.string.title_exit);
	            String message = getString(R.string.msg_exit);
	            showYesNoExitDialogBox(title, message);
	            
				
	            return true;
	            
	    	}
	    	
	    	*/
	    }

	    return super.onKeyDown(keyCode, event);
	}
	
	
	private void showYesNoExitDialogBox(final String title, final String message) {
    	
    	System.out.println("-- showYesNoExitDialogBox");
        Builder setupAlert;
        setupAlert = new AlertDialog.Builder(TabHomeActivityGroup.this).setTitle(title).setMessage(message).setPositiveButton( "Yes" ,
            new DialogInterface.OnClickListener() {
              public void onClick(DialogInterface dialog, int whichButton) {
            	  
            	  System.out.println("yes : ");
            	  
	      			finish();
	      			
	      			onDestroy();
	                android.os.Process.killProcess(android.os.Process.myPid());
	            
              }
            }).setNegativeButton( "No" , new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int whichButton) {
        	  
        	  	System.out.println("no : ");
          }
        });
        setupAlert.show();
      }

}
