package com.gmm.HoroApp;


import com.gmm.HoroApp.R;

import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TabHost;

public class TabHoroMenuActivity extends TabActivity {
	
	public static TabHost host;
	
	int tabID;
	
    public void onCreate(Bundle savedInstanceState) {
		
		System.out.println("__________ TabHoroMenuActivity __________");
		
		super.onCreate(savedInstanceState);
        
        this.setContentView(R.layout.customtab);
        
        
        Bundle bundle = getIntent().getExtras();

        if(bundle != null){
        	tabID = bundle.getInt("IDTAB");
        }
        
        host=getTabHost();
        
        
        host.getTabWidget().setBackgroundResource(R.drawable.bg_main_btn);
        
        
        host.addTab(host.newTabSpec("tabhorohome")
        		.setIndicator("", getResources().getDrawable(R.drawable.tabb_home))
        		.setContent(new Intent().setClass(this, TabHoroHomeActivity.class)));
        
        host.addTab(host.newTabSpec("tabhoromonthlyhoro")
        		.setIndicator("", getResources().getDrawable(R.drawable.tabb_monthlyhoro))
        		.setContent(new Intent().setClass(this, TabHoroMonthlyHoroActivity.class)));
        
        host.addTab(host.newTabSpec("tabhorolovemeter")
        		.setIndicator("", getResources().getDrawable(R.drawable.tabb_lovemeter))
        		.setContent(new Intent().setClass(this, TabHoroLoveMeterActivity.class)));
        
        
        host.addTab(host.newTabSpec("tabhoroluckycolor")
        		.setIndicator("", getResources().getDrawable(R.drawable.tabb_luckycolour))
        		.setContent(new Intent().setClass(this, TabHoroLuckyColorActivity.class)));
        
        host.addTab(host.newTabSpec("tabhorovedioclip")
        		.setIndicator("", getResources().getDrawable(R.drawable.tabb_videoclip))
        		.setContent(new Intent().setClass(this, TabHoroVideoClipActivity.class)));
        
		
        
        host.getTabWidget().getChildAt(0).setBackgroundResource(R.drawable.bg_main_btn);
//        
        host.getTabWidget().getChildAt(1).setBackgroundResource(R.drawable.bg_main_btn);
//        
        host.getTabWidget().getChildAt(2).setBackgroundResource(R.drawable.bg_main_btn);
//        
        host.getTabWidget().getChildAt(3).setBackgroundResource(R.drawable.bg_main_btn);
//        
        host.getTabWidget().getChildAt(4).setBackgroundResource(R.drawable.bg_main_btn);

        host.setCurrentTab(tabID);
        
      //  host.getTabWidget().setStripEnabled(false);
        
		
	}
	
	
	public static boolean isSdPresent() {  
	    return android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED);  
	}

}
