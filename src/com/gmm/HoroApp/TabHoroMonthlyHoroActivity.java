package com.gmm.HoroApp;

import android.app.ActivityGroup;
import android.app.LocalActivityManager;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;

public class TabHoroMonthlyHoroActivity extends ActivityGroup {
	
	static String page_home;
	
	public static LocalActivityManager mLocalActivityManager; 
	
	 
	protected void onCreate(Bundle savedInstanceState) { 
	      super.onCreate(savedInstanceState); 
	      
	      System.out.println("__________ TabHoroMonthlyHoroActivity __________");
	      
	      mLocalActivityManager = getLocalActivityManager();
	      View view = mLocalActivityManager 
	                                .startActivity("monthlyhoro", new Intent(this,NewMonthlyHoroActivity.class) 
	                                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
	                                ) 
	                                .getDecorView(); 
	       this.setContentView(view); 
	       
	}
	
	public boolean onKeyDown(int keyCode, KeyEvent event)  {
    	
    	System.out.println("onKeyDown ________ TabHoroMonthlyHoroActivity ___________");
    	
	    if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {

	        // do something on back.
	    	Log.log("Page curent  "+mLocalActivityManager.getCurrentId());
	    	
	    	
	    	if(mLocalActivityManager.getCurrentId().equals("monthlyhorodetail")){

	    		
	    		mLocalActivityManager = getLocalActivityManager();
	  	      View view = mLocalActivityManager 
	  	                                .startActivity("monthlyhoro", new Intent(this,NewMonthlyHoroActivity.class)
//	  	                                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
	  	                                )
	  	                                .getDecorView(); 
	  	       this.setContentView(view); 
	  	     mLocalActivityManager.destroyActivity("monthlyhorodetail", true);
		        return true;
			}
	    }

	    return super.onKeyDown(keyCode, event);
	}

}
