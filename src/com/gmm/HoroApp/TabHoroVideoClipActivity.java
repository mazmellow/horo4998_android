package com.gmm.HoroApp;

import android.app.ActivityGroup;
import android.app.LocalActivityManager;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;

public class TabHoroVideoClipActivity extends ActivityGroup {
	
	static String page_home;
	
	public static LocalActivityManager mLocalActivityManager; 
	
	 
	protected void onCreate(Bundle savedInstanceState) { 
	      super.onCreate(savedInstanceState); 
	      
	      System.out.println("__________ TabHoroVideoClipActivity __________");
	      
	      
	      mLocalActivityManager = getLocalActivityManager();
	      View view = mLocalActivityManager 
	                                .startActivity("videoclip", new Intent(this,VideoClipActivity.class) 
	                                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)) 
	                                .getDecorView(); 
	       this.setContentView(view); 
	       
	}
	
	
	
	
	
	
	
	public boolean onKeyDown(int keyCode, KeyEvent event)  {
    	
    	System.out.println("onKeyDown ________ TabHoroVideoClipActivity ___________");
    	
	    if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
	        // do something on back.
	    	
	    	finish();
	    	
	    	
	    	/*
	    	if(page_home.equals("Howto")){
	    		
			      View view = mLocalActivityManager 
			                                .startActivity("home", new Intent(this,HomeActivity.class) 
			                                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)) 
			                                .getDecorView(); 
			       this.setContentView(view); 
				
		    	mLocalActivityManager.destroyActivity("howto", true);
		        return true;
		        
	    	}else if(page_home.equals("Home")){
	    		
	    		//-alert exit-//
	        	String title = getString(R.string.title_exit);
	            String message = getString(R.string.msg_exit);
	            showYesNoExitDialogBox(title, message);
	            
				
	            return true;
	            
	    	}
	    	
	    	*/
	    }

	    return super.onKeyDown(keyCode, event);
	}
	
	

}
