package com.gmm.HoroApp;

import com.gmm.HoroApp.R;

import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TabHost;

public class TabMenuActivity extends TabActivity {
	
	public static TabHost host;
	
	
    public void onCreate(Bundle savedInstanceState) {
		
		System.out.println("----------- TabMenuActivity ----------");
		
		super.onCreate(savedInstanceState);
        
        this.setContentView(R.layout.customtab);
        host=getTabHost();
        
        
        host.getTabWidget().setBackgroundResource(R.drawable.bg_main_btn);
        
        
        host.addTab(host.newTabSpec("home")
        		.setIndicator("", getResources().getDrawable(R.drawable.empty))
        		.setContent(new Intent().setClass(this, TabHomeActivityGroup.class)));
        
        host.addTab(host.newTabSpec("profile")
        		.setIndicator("", getResources().getDrawable(R.drawable.empty))
        		.setContent(new Intent().setClass(this, TabProfileActivity.class)));
        
        
        host.addTab(host.newTabSpec("horo")
        		.setIndicator("", getResources().getDrawable(R.drawable.empty))
        		.setContent(new Intent().setClass(this, TabHoroActivityGroup.class)));
        
        host.addTab(host.newTabSpec("horoplus")
        		.setIndicator("", getResources().getDrawable(R.drawable.empty))
        		.setContent(new Intent().setClass(this, TabHoroPlusActivityGroup.class)));
        
		
        
        host.getTabWidget().getChildAt(0).setBackgroundResource(R.drawable.tabb_home);
        
        host.getTabWidget().getChildAt(1).setBackgroundResource(R.drawable.tabb_profile);
        
        host.getTabWidget().getChildAt(2).setBackgroundResource(R.drawable.tabb_horo);
        
        host.getTabWidget().getChildAt(3).setBackgroundResource(R.drawable.tabb_horoplus);
        
        
      //  host.getTabWidget().setStripEnabled(false);
        
		
	}
	
	
	public static boolean isSdPresent() {  
	    return android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED);  
	}

}
