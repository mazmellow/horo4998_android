package com.gmm.HoroApp;

import android.app.ActivityGroup;
import android.app.LocalActivityManager;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;

public class TabPlusAuspiciousDayActivity extends ActivityGroup {
	
	static String page_home;
	
	public static LocalActivityManager mLocalActivityManager; 
	
	 
	protected void onCreate(Bundle savedInstanceState) { 
	      super.onCreate(savedInstanceState); 
	      
	      System.out.println("__________ TabPlusAuspiciousDayActivity __________");
	      TabPlusMenuActivity.shaked = true;
	      
	      mLocalActivityManager = getLocalActivityManager();
	      View view = mLocalActivityManager 
	                                .startActivity("auspiciousday", new Intent(this,AuspiciousDayActivity.class) 
	                                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)) 
	                                .getDecorView(); 
	       this.setContentView(view); 
	       
	}
	
	
	
	
	public boolean onKeyDown(int keyCode, KeyEvent event)  {
    	
    	System.out.println("onKeyDown ________ TabPlusAuspiciousDayActivity ___________");
    	
	    if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
	        // do something on back.
	    	
	    	
	    	/*
	    	if(page_home.equals("Howto")){
	    		
			      View view = mLocalActivityManager 
			                                .startActivity("home", new Intent(this,HomeActivity.class) 
			                                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)) 
			                                .getDecorView(); 
			       this.setContentView(view); 
				
		    	mLocalActivityManager.destroyActivity("howto", true);
		        return true;
		        
	    	}else if(page_home.equals("Home")){
	    		
	    		//-alert exit-//
	        	String title = getString(R.string.title_exit);
	            String message = getString(R.string.msg_exit);
	            showYesNoExitDialogBox(title, message);
	            
				
	            return true;
	            
	    	}
	    	
	    	*/
	    }

	    return super.onKeyDown(keyCode, event);
	}
	
	

}
