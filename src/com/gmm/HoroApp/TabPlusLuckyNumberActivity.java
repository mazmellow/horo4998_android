package com.gmm.HoroApp;

import android.app.ActivityGroup;
import android.app.LocalActivityManager;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;

public class TabPlusLuckyNumberActivity extends ActivityGroup {
	
	static String page_home;
	
	public static LocalActivityManager mLocalActivityManager; 
	
	protected void onCreate(Bundle savedInstanceState) { 
		System.out.println("__________ TabPlusLuckyNumberActivity __________");  
		super.onDestroy();
		super.onCreate(savedInstanceState); 
	    
		mLocalActivityManager = getLocalActivityManager();
		TabPlusMenuActivity.shaked = true;
//		Bundle bundle = getIntent().getExtras();
//		
//		
//		mLocalActivityManager = getLocalActivityManager();
//    	View view = mLocalActivityManager
//    		.startActivity("luckynumber", new Intent(this,LuckyNumberActivity.class) 
//    		.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)) 
//    		.getDecorView(); 
//			this.setContentView(view);
		
	    if(TabPlusMenuActivity.luckynumberloadcomplete){
	    	
	    	View view = mLocalActivityManager
	    		.startActivity("luckynumber", new Intent(this,LuckyNumberActivity.class) 
	    		.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)) 
	    		.getDecorView(); 
  			this.setContentView(view);
	    	
	    }else{    
			View view = mLocalActivityManager 
				.startActivity("splashluckynumber", new Intent(this,SplashLuckyNumberActivity.class) 
				.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)) 
				.getDecorView(); 
			this.setContentView(view);
	    }
	}
	
	
	
	
	public boolean onKeyDown(int keyCode, KeyEvent event)  {
    	
    	System.out.println("onKeyDown ________ TabPlusLuckyNumberActivity ___________");
    	
	    if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
	        // do something on back.
	    	Log.log("getCurrentId : "+mLocalActivityManager.getCurrentId());
	    	if(mLocalActivityManager.getCurrentId().equals("splashluckynumber")){
	    		
	    		TabPlusMenuActivity.shaked = true;
		    		finish();
		    		onDestroy();
	    		}
	    	else if(mLocalActivityManager.getCurrentId().equals("luckynumber")){
	    		TabPlusMenuActivity.shaked = true;
	    		TabPlusMenuActivity.luckynumberloadcomplete = false;
	    		
		    		View view = mLocalActivityManager 
						.startActivity("splashluckynumber", new Intent(this,SplashLuckyNumberActivity.class) 
						.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)) 
						.getDecorView(); 
	    		
	    		this.setContentView(view);
	    	
	    	}
	    		
	    	return true;
	    }

	    return super.onKeyDown(keyCode, event);
	}

}
