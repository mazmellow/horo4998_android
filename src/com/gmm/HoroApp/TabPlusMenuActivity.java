package com.gmm.HoroApp;


import com.gmm.HoroApp.R;

import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TabHost;

public class TabPlusMenuActivity extends TabActivity {
	
	public static TabHost host;
	int tabID;
	public static boolean shaked;
	public static boolean luckynumberloadcomplete = false;
	
    public void onCreate(Bundle savedInstanceState) {
		
		System.out.println("__________ TabPlusMenuActivity __________");
		
		super.onCreate(savedInstanceState);
        
        this.setContentView(R.layout.customtab);
        
        Bundle bundle = getIntent().getExtras();

        if(bundle != null){
        	tabID = bundle.getInt("IDTAB");
        }
        
        System.out.println("----- Tab Id : " + tabID);
        
        host=getTabHost();
        
        
        host.addTab(host.newTabSpec("tabplushome")
        		.setIndicator("", getResources().getDrawable(R.drawable.tabb_home))
        		.setContent(new Intent().setClass(this, TabPlusHomeActivity.class)));
        
        host.addTab(host.newTabSpec("tabplusweeklyhoro")
        		.setIndicator("", getResources().getDrawable(R.drawable.tabb_weeklyhoro))
        		.setContent(new Intent().setClass(this, TabPlusWeeklyHoroActivity.class)));
        
        host.addTab(host.newTabSpec("tabplusauspiciousday")
        		.setIndicator("", getResources().getDrawable(R.drawable.tabb_auspiciousday))
        		.setContent(new Intent().setClass(this, TabPlusAuspiciousDayActivity.class)));
        
        host.addTab(host.newTabSpec("tabplusluckynumber")
        		.setIndicator("", getResources().getDrawable(R.drawable.tabb_luckynumber))
        		.setContent(new Intent().setClass(this, TabPlusLuckyNumberActivity.class)));
        
        host.addTab(host.newTabSpec("tabplusspecialvideo")
        		.setIndicator("", getResources().getDrawable(R.drawable.tabb_specialvdoclip))
        		.setContent(new Intent().setClass(this, TabPlusSpecialVideoActivity.class)));
        
        host.getTabWidget().setBackgroundResource(R.drawable.bg_main_btn);
        
        host.getTabWidget().getChildAt(0).setBackgroundResource(R.drawable.bg_main_btn);
        
        host.getTabWidget().getChildAt(1).setBackgroundResource(R.drawable.bg_main_btn);
        
        host.getTabWidget().getChildAt(2).setBackgroundResource(R.drawable.bg_main_btn);
        
        host.getTabWidget().getChildAt(3).setBackgroundResource(R.drawable.bg_main_btn);
        
        host.getTabWidget().getChildAt(4).setBackgroundResource(R.drawable.bg_main_btn);

        
        host.setCurrentTab(tabID);
        
      //  host.getTabWidget().setStripEnabled(false);
        
		
	}
	
	
	public static boolean isSdPresent() {  
	    return android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED);  
	}

}
