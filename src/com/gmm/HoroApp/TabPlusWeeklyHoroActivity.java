package com.gmm.HoroApp;

import android.app.ActivityGroup;
import android.app.LocalActivityManager;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;

public class TabPlusWeeklyHoroActivity extends ActivityGroup {
	
	static String page_home;
	
	public static LocalActivityManager mLocalActivityManager; 
	
	 
	protected void onCreate(Bundle savedInstanceState) { 
	      super.onCreate(savedInstanceState); 
	      
	      System.out.println("__________ TabPlusWeeklyHoroActivity __________");
	      TabPlusMenuActivity.shaked = true;
	      
	      mLocalActivityManager = getLocalActivityManager();
	      View view = mLocalActivityManager 
	                                .startActivity("weeklyhoro", new Intent(this,WeeklyHoroActivity.class) 
	                                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)) 
	                                .getDecorView(); 
	       this.setContentView(view); 
	       
	}
	
	
	
	
	public boolean onKeyDown(int keyCode, KeyEvent event)  {
    	
    	System.out.println("onKeyDown ________ TabPlusWeeklyHoroActivity ___________");
    	
	    if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
	        // do something on back.
	    	Log.log("Page curent  "+mLocalActivityManager.getCurrentId());
	    	
	    	
	    	if(mLocalActivityManager.getCurrentId().equals("weeklyhorodetail")){
	    		mLocalActivityManager = getLocalActivityManager();
	  	      View view = mLocalActivityManager 
	  	                                .startActivity("weeklyhoro", new Intent(this,NewWeeklyHoroActivity.class) 
//	  	                                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
	  	                                ) 
	  	                                .getDecorView(); 
	  	       this.setContentView(view); 
	  	     mLocalActivityManager.destroyActivity("weeklyhorodetail", true);
		        return true;
			}
	    }

	    return super.onKeyDown(keyCode, event);
	}

}
