package com.gmm.HoroApp;

import java.util.ArrayList;
import java.util.List;

import com.gmm.HoroApp.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.graphics.drawable.BitmapDrawable;
import android.hardware.Camera.Parameters;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class TabProfileActivity extends Activity implements Runnable {
	
	
	private ProgressDialog pd;
	
	static TextView label; 
	
	ImageView image_banner, imgProfile;
	
	
    public void onCreate(Bundle savedInstanceState) {
		
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile);
        
        
        image_banner = (ImageView)findViewById(R.id.imgprofilebanner);
        
        try{
        	
	 		if(HomeActivity.bitmapBanner != null){
	 			image_banner.setImageBitmap(HomeActivity.bitmapBanner);
	 		}
	 		
		} catch (Exception e) {
			e.printStackTrace();
		}
 		
		ConfigData configData = new ConfigData(this);
		List<Config> listconfig = new ArrayList<Config>();
		
		try {
			listconfig = configData.query(null, null);
			if(listconfig != null){	
				if(listconfig.size() > 0){
					
					ImageView bg_header = (ImageView)findViewById(R.id.img_profile);
	    	    		BitmapDrawable bitmapDrawable = new BitmapDrawable(listconfig.get(0).getProfileText()); 
//	    	    		bg_header.setBackgroundDrawable(bitmapDrawable);
	    	    		bg_header.setImageDrawable(bitmapDrawable);
					
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
        pd = ProgressDialog.show(getParent(), "Loading..", "Please wait..",true,false);
        
        Thread thread = new Thread(this);
        thread.start();
       
    }
	
	public void run() {
    	try {
    		
    		
		} catch (Exception e) {
			e.printStackTrace();
		}
        
        handler.sendEmptyMessage(0);
    }
	
	private Handler handler = new Handler() {
        
        public void handleMessage(Message msg) {
        	
        	try {
        		
            	pd.dismiss();
                	
        	}
        	catch (Exception e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
        }
    };
   
    private void showYesNoExitDialogBox(final String title, final String message) {
    	
    	System.out.println("-- showYesNoExitDialogBox");
        Builder setupAlert;
        setupAlert = new AlertDialog.Builder(TabProfileActivity.this).setTitle(title).setMessage(message).setPositiveButton( "Yes" ,
            new DialogInterface.OnClickListener() {
              public void onClick(DialogInterface dialog, int whichButton) {
            	  
            	  System.out.println("yes : ");
            	  
	      			finish();
	      			
	      			onDestroy();
	                android.os.Process.killProcess(android.os.Process.myPid());
	            
              }
            }).setNegativeButton( "No" , new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int whichButton) {
        	  
        	  	System.out.println("no : ");
          }
        });
        setupAlert.show();
      }
    
	public boolean onKeyDown(int keyCode, KeyEvent event)  {
    	
    	System.out.println("onKeyDown________ TabProfileActivity ___________");
    	
	    if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
	        // do something on back.
	    	
	    		//-alert exit-//
	        	String title = getString(R.string.title_exit);
	            String message = getString(R.string.msg_exit);
	            showYesNoExitDialogBox(title, message);
	            
				
	            return true;
	            
	    }

	    return super.onKeyDown(keyCode, event);
	}

}
