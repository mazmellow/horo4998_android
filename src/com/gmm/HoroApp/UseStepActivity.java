package com.gmm.HoroApp;

import com.gmm.HoroApp.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;

public class UseStepActivity extends Activity{

	private ImageView imgbanner, imghowtouse;
	
	protected void onCreate(Bundle savedInstanceState) {
		
		Log.log("------------ UseStepActivity -------");
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.usestep);
		
		imgbanner = (ImageView)findViewById(R.id.imgusestepbanner);
		imghowtouse = (ImageView)findViewById(R.id.imghowtouse);
		
		if(HomeActivity.bitmapBanner != null){
			imgbanner.setImageBitmap(HomeActivity.bitmapBanner);
		}
		if(HomeActivity.bitmapHowToUse != null){
			Log.log("bitmapHowtoToUse != null");
			imghowtouse.setImageBitmap(HomeActivity.bitmapHowToUse);
		}
	}

	
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {

	    	insertDecorView(TabHomeActivityGroup.mLocalActivityManager.startActivity("home", 
					new Intent(UseStepActivity.this,HomeActivity.class)).
					
					getDecorView());
	    	TabHomeActivityGroup.mLocalActivityManager.destroyActivity("usestep", true);
	    	
	        return true;
	    }
		return super.onKeyDown(keyCode, event);
	}
	
	private void insertDecorView(View view) {
    	
    	getParent().setContentView(view, new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
    }
}
