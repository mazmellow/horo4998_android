package com.gmm.HoroApp;

public class Version {
	
	public static String COL_config="config";
	public static String COL_menulist="menulist";
	public static String COL_menupluslist="menupluslist";
	
	public static String COL_monthlyhoro="monthlyhoro";	
	public static String COL_video="video";	
	public static String COL_luckynumber="luckynumber";	
	public static String COL_weeklyhoro="weeklyhoro";	
	public static String COL_specialvideo="specialvideo";	
	
	
	private String ConfigAPI="";
	private String MenuListAPI="";
	private String MenuPlusListAPI="";
	
	private String Content_1="";
	private String Content_2="";
	private String Content_3="";
	private String Content_4="";
	private String Content_5="";
	
	
	public String getConfigAPI() {
		return(ConfigAPI);
	}
	
	public void setConfigAPI(String ConfigAPI) {
		this.ConfigAPI=ConfigAPI;
	}
	
	
	public String getMenuListAPI() {
		return(MenuListAPI);
	}
	
	public void setMenuListAPI(String MenuListAPI) {
		this.MenuListAPI=MenuListAPI;
	}
	
	
	public String getMenuPlusListAPI() {
		return(MenuPlusListAPI);
	}
	
	public void setMenuPlusListAPI(String MenuPlusListAPI) {
		this.MenuPlusListAPI=MenuPlusListAPI;
	}
	
	
	
	public String getMonthlyHoro() {
		return(Content_1);
	}
	
	public void setMonthlyHoro(String Content_1) {
		this.Content_1=Content_1;
	}
	
	
	public String getVideo() {
		return(Content_2);
	}
	
	public void setVideo(String Content_2) {
		this.Content_2=Content_2;
	}
	
	
	public String getLuckyNumber() {
		return(Content_3);
	}
	
	public void setLuckyNumber(String Content_3) {
		this.Content_3=Content_3;
	}
	
	
	public String getWeeklyHoro() {
		return(Content_4);
	}
	
	public void setWeeklyHoro(String Content_4) {
		this.Content_4=Content_4;
	}
	
	
	public String getSpecialVideo() {
		return(Content_5);
	}
	
	public void setSpecialVideo(String Content_5) {
		this.Content_5=Content_5;
	}
	
	

}
