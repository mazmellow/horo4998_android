package com.gmm.HoroApp;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;

public class VersionDB extends ContentProvider{
	
	private static DatabaseHelper dbHelper;
	private static SQLiteDatabase sqliteDB;
	
	
	private static class DatabaseHelper extends SQLiteOpenHelper {

	    private static final String DATABASE_NAME = "GMM.versiondb";
	    private static final int DATABASE_VERSION = 1; // must begin with >= 1
	    private static final String TABLE_NAME = "version";
	    
	    private final String SQL_CREATE_TABLE = "create table if not exists "+TABLE_NAME+" ("
																		+"config"+" text, "
																		+"menulist"+" text, "
																		+"menupluslist"+" text, "
																		+"monthlyhoro"+" text, "
																		+"video"+" text, "
																		+"luckynumber"+" text, "
																		+"weeklyhoro"+" text, "
																		+"specialvideo"+" text);";		//8 COL
	    
	    //private final String SQL_DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;
	    
	    public DatabaseHelper(Context context) {
	      super(context, DATABASE_NAME, null, DATABASE_VERSION);
	      
	      //if check DATABASE_VERSION change >> onUpgrade called
	    }

	    
	    public void onCreate(SQLiteDatabase db) {
	      db.execSQL(SQL_CREATE_TABLE);

	    }

	    
	    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
	      //db.execSQL(SQL_DROP_TABLE);
	    }
	}
	
	
	
	
	public boolean onCreate() {
		// TODO Auto-generated method stub
		dbHelper = new DatabaseHelper(getContext());
		return false;
	}

	
	
	
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		// TODO Auto-generated method stub
		sqliteDB = dbHelper.getWritableDatabase();
	    try {
	    	
	    	if(selection==null){
	    		sqliteDB.delete(DatabaseHelper.TABLE_NAME, null, null);
	    	}else{
	    		sqliteDB.delete(DatabaseHelper.TABLE_NAME, selection, selectionArgs);
	    	}
	      
	    } catch (Exception e) {

	    } finally {
	      sqliteDB.close();
	    }
		return 0;
	}


	
	public String getType(Uri uri) {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	
	
	public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
		
		System.out.println(" VersionDB -- update");
		
		// TODO Auto-generated method stub
		sqliteDB = dbHelper.getWritableDatabase();
	    try {
	      sqliteDB.update(DatabaseHelper.TABLE_NAME, values, selection, selectionArgs);
	      
	      // notify to allow other threads known and utilize this update method of content provider
	      getContext().getContentResolver().notifyChange(uri, null);
	    } catch (Exception e) {

	    } finally {
	      sqliteDB.close();
	    }
		return 0;
	}


	
	
	public Uri insert(Uri uri, ContentValues values) {
		
		System.out.println(" VersionDB -- insert");
		
		// TODO Auto-generated method stub
		sqliteDB = dbHelper.getWritableDatabase();
	    Uri rowUri = null;
	    try {
	      
	      
	      long rowId = sqliteDB.insert(DatabaseHelper.TABLE_NAME, "", values);
	      if (rowId > 0) {
	        
	        /**
	         * ContentUris: Utility methods useful for working with content Uris, those with a "content" scheme. 
	         */
	        rowUri = ContentUris.appendId(UsersVersionDB.AuthenticationInfo.CONTENT_URI.buildUpon(), rowId).build();
	        
	        // notify to allow other threads known and utilize this insert method of content provider
	        getContext().getContentResolver().notifyChange(rowUri, null);
	      }
	    } catch (Exception e) {

	    } finally {
	      sqliteDB.close();
	    }
	    return rowUri;
	}


	
	
	public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
		
		System.out.println(" VersionDB -- query");
		
		// TODO Auto-generated method stub
		Cursor mCursor = null;
	    try {
	      sqliteDB = dbHelper.getReadableDatabase();
	      
	      if(selection==null){
	    	  
	    	  mCursor = sqliteDB.query(DatabaseHelper.TABLE_NAME, 
			          projection, 
			          null, 
			          null, 
			          null, 
			          null, 
			          null);
	      }else{
	    	  
	    	  mCursor = sqliteDB.query(DatabaseHelper.TABLE_NAME, 
			          projection, 
			          selection, 
			          selectionArgs, 
			          null, 
			          null, 
			          null);
	      }
	      
		      
	      
	    } catch (Exception e) {
	      System.out.println(e.getMessage());
	    }
	    return mCursor;
	}

	
}