package com.gmm.HoroApp;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

public class VersionData {
	
	private Context mContext;
	
	public VersionData(Context context){
		mContext = context;
	}


	public void insert(final Version version) {
		
		System.out.println(" VersionData -- insert");
		
		
		ContentValues values = new ContentValues();	
		
		values.put(Version.COL_config, version.getConfigAPI());	
		values.put(Version.COL_menulist, version.getMenuListAPI());	
		values.put(Version.COL_menupluslist, version.getMenuPlusListAPI());	
		
		values.put(Version.COL_monthlyhoro, version.getMonthlyHoro());	
		values.put(Version.COL_video, version.getVideo());	
		values.put(Version.COL_luckynumber, version.getLuckyNumber());	
		values.put(Version.COL_weeklyhoro, version.getWeeklyHoro());	
		values.put(Version.COL_specialvideo, version.getSpecialVideo());	//8
		
    	mContext.getContentResolver().insert(UsersVersionDB.AuthenticationInfo.CONTENT_URI, values);
    	
	}
	
	
	public void update(final Version version, String selection, final String selectionArgs) {
		
		System.out.println(" VersionData -- update");
		
		//String selection = Content.COL_id + "=?";
		ContentValues values = new ContentValues(); 
		
		values.put(Version.COL_config, version.getConfigAPI());	
		values.put(Version.COL_menulist, version.getMenuListAPI());	
		values.put(Version.COL_menupluslist, version.getMenuPlusListAPI());	
		
		values.put(Version.COL_monthlyhoro, version.getMonthlyHoro());	
		values.put(Version.COL_video, version.getVideo());	
		values.put(Version.COL_luckynumber, version.getLuckyNumber());	
		values.put(Version.COL_weeklyhoro, version.getWeeklyHoro());	
		values.put(Version.COL_specialvideo, version.getSpecialVideo());	//8
		
		mContext.getContentResolver().update(UsersVersionDB.AuthenticationInfo.CONTENT_URI, 
		    values, 
		    selection, 
		    new String[]{selectionArgs} 
			);
	}
	
	
	
	
	
	public final List<Version> query( String selection, final String contentCodee) {
		
		System.out.println(" VersionData -- query");
		
		// An array specifying which columns to return.
		String columns[] = new String[] { 
				Version.COL_config,
				Version.COL_menulist,
				Version.COL_menupluslist,
				Version.COL_monthlyhoro,
				Version.COL_video,
				Version.COL_luckynumber,
				Version.COL_weeklyhoro,
				Version.COL_specialvideo }; //8

		//selection = Content.COL_contentcode + "=?";

		Uri myUri = UsersVersionDB.AuthenticationInfo.CONTENT_URI;
		Cursor cur = mContext.getContentResolver().query(myUri, columns, selection, new String[]{contentCodee}, null);
		
		List<Version> Version_Listt = new ArrayList<Version>();

		// check if found any query result
		if (cur.getCount() > 0) {
			
			cur.moveToFirst();
			int numRows = cur.getCount();
			
			for (int i = 0; i < numRows; ++i) {
			
				Version version = new Version(); 
				
				version.setConfigAPI(cur.getString(cur.getColumnIndex(Version.COL_config)));
				version.setMenuListAPI(cur.getString(cur.getColumnIndex(Version.COL_menulist)));
				version.setMenuPlusListAPI(cur.getString(cur.getColumnIndex(Version.COL_menupluslist)));
				
				version.setMonthlyHoro(cur.getString(cur.getColumnIndex(Version.COL_monthlyhoro)));
				version.setVideo(cur.getString(cur.getColumnIndex(Version.COL_video)));
				version.setLuckyNumber(cur.getString(cur.getColumnIndex(Version.COL_luckynumber)));
				version.setWeeklyHoro(cur.getString(cur.getColumnIndex(Version.COL_weeklyhoro)));
				version.setSpecialVideo(cur.getString(cur.getColumnIndex(Version.COL_specialvideo)));
				
				/*
				System.out.println(i+"--"+version.getConfigAPI());
				System.out.println(i+"--"+version.getMenuListAPI());
				System.out.println(i+"--"+version.getMenuPlusListAPI());
				System.out.println(i+"--"+version.getMonthlyHoro());
				System.out.println(i+"--"+version.getLuckyNumber());
				System.out.println(i+"--"+version.getWeeklyHoro());
				*/
				
				Version_Listt.add(version);
				
				cur.moveToNext();
				
			}
			
			return Version_Listt;
		}
		
		// close cursor.
		cur.close();
		return null;
	}
	

	
	public void delete( final String selection, final String selectionArgs) {

		System.out.println(" VersionData -- delete");
		
		//String selection = Content.COL_contentcode +"=?";
		
		Uri myUri = UsersVersionDB.AuthenticationInfo.CONTENT_URI;
		mContext.getContentResolver().delete(myUri, selection, new String[]{selectionArgs});
		
	}
}