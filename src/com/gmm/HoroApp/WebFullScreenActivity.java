package com.gmm.HoroApp;

import com.gmm.HoroApp.R;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Display;
import android.view.KeyEvent;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class WebFullScreenActivity extends Activity implements Runnable{
	private ProgressDialog pd;

	WebView mWebView;
	String URL_webview; 
	
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
		
		System.out.println("--- WebFullScreenActivity");
		
        super.onCreate(savedInstanceState);
        setContentView(R.layout.webfullscreen);
        
        
        Bundle d = getIntent().getExtras();
        if (d != null) {
	        URL_webview = d.getString("URL_webview");
	        System.out.println(" URL_webview ::"+URL_webview);
        }
        
        
        mWebView = (WebView) findViewById(R.id.web_fullscreen);
        
        
        if(checkInternet()){
			
			pd = ProgressDialog.show( WebFullScreenActivity.this , "Loading..", "Please wait..",true,false);
	        
	        Thread thread = new Thread(this);
	        thread.start();
	        
		}else{
			
			/*
			//alert 
			String title = getString(R.string.title_internet_false);
            String message = getString(R.string.msg_internet_false);
            showNoticeDialogBox(title, message);
            */
		}
        
        
	}
	
	
	
	public void run() {
    	try {
    		
    		System.out.println("-run -- WebViewAcitivity");
    		
    		mWebView.getSettings().setJavaScriptEnabled(true);
	        
	        mWebView.clearCache(true);
	        mWebView.clearView();
	        mWebView.clearHistory();
	        mWebView.clearFormData();
	        
	        mWebView.loadUrl(URL_webview);
	        
	        mWebView.setWebViewClient(new HelloWebViewClient());
	        
	        // declare the original size of the iPad app
	        float ORIG_APP_W = 530;
	        
	        // get actual screen size
	        Display display = ((WindowManager) getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
	        int width = display.getWidth(); 
	        int height = display.getHeight(); 
	        
	        
	        // calculate target scale (only dealing with portrait orientation)
	        double globalScale = Math.ceil( ( width / ORIG_APP_W ) * 100 );
	        
			mWebView.getSettings().setSupportZoom(true);
			
			//mWebView.getSettings().setMinimumFontSize(16);
	        
	        
	        // set the scale
	        // mWebView.setInitialScale( (int)globalScale );
    		
    		
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        handler.sendEmptyMessage(0);
    }
	
	
	private Handler handler = new Handler() {
	    @Override
	    public void handleMessage(Message msg) {
	    	try {
	    		
	            pd.dismiss();
	            
	    	}
	    	catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    }
	};
	
	
	@Override
	protected void onResume() {
		
		super.onResume();
		
		System.out.println("---------- onResume() ---- WebFullScreenActivity ------");
		
		
	}
	
	
	private class HelloWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }
    
    
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK) && mWebView.canGoBack()) {
        	
        	System.out.println("======== onKeyDown");
        	
    		finish();
        	
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
    
    public boolean checkInternet() {
		
    	ConnectivityManager connec =  ( ConnectivityManager ) getSystemService( Context.CONNECTIVITY_SERVICE ); 
    	
    	if ( connec.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTED || connec.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTED )
    	{
    	     // connect 
    		return true;
    	}
    	else
    	{
    	     // not-connect 
    		return false;
    	}
    }
}
