package com.gmm.HoroApp;

import java.util.ArrayList;
import java.util.List;

import com.gmm.HoroApp.R;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

public class WebViewPlusActivity extends Activity{
	
	WebView www;
	String ChangGuYuNi;
//	String ChangGuYuNe;
//	String ulul ="http://www.google.com";
	ImageView image_banner;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
		
//		Bundle xxx = getIntent().getExtras();
//        ChangGuYuNi = xxx.getString("URLX");
        
        Bundle yyy = getIntent().getExtras();
        ChangGuYuNi = yyy.getString("URLY");
        
		super.onCreate(savedInstanceState);   
        setContentView(R.layout.webviewplus);
        
        
        
        image_banner = (ImageView)findViewById(R.id.imgmonlybanner);
        ConfigData cdcd = new ConfigData(this);
        List<Config> cfcf = new ArrayList<Config>();
        
        try{
	        //banner
        	cfcf = cdcd.query(null, null);
	     	
        	Bitmap bb=null;
        	
        	//get image banner from ConfigDB
        	bb = cfcf.get(0).getBannerImg();
	     	
	     	
	     	
	 		if(bb!=null){
	 			System.out.println("------- bb!=null");
	 			image_banner.setImageBitmap(bb);
	 		}
	 		
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        
        www = (WebView)findViewById(R.id.weblook);
        www.getSettings().setJavaScriptEnabled(true);     
        www.setWebViewClient(new WebViewClient() {
            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl)
            {
                // Handle the error
            	System.out.println("URL ERROR");
            }
 
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url)
            {
                view.loadUrl(url);
                return true;
            }
        });
        
        System.out.println(ChangGuYuNi+"TTTTTTTTTTTTTTVVVVVVVVVVVVVVVZZZZZZZZZZZETA");
        
        www.loadUrl(ChangGuYuNi);
        
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		
	    if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {

	    	insertDecorView(TabHoroPlusActivityGroup.mLocalActivityManager.startActivity("horoplusmenu", 
					new Intent(WebViewPlusActivity.this,HoroMenuActivity.class)).
					
					getDecorView());
	    	TabHoroActivityGroup.mLocalActivityManager.destroyActivity("webview", true);
	    	
	        return true;
	    }

	    return super.onKeyDown(keyCode, event);
		
	}

	private void insertDecorView(View view) {
    	
    	getParent().setContentView(view, new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
    }
	
}
