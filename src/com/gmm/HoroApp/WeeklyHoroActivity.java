package com.gmm.HoroApp;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.webkit.WebView;
import android.widget.ImageView;

public class WeeklyHoroActivity extends Activity{

	private ImageView imgHead;
	private WebView web;
	private int mMonth;
	
	private WeeklyHoroData weeklyHoroData;
	private List<ContentHoro> listContent;
	private String summary = "";
	
	private int[] image = {
				R.drawable.wh_1,
				R.drawable.wh_2,
				R.drawable.wh_3,
				R.drawable.wh_4,
				R.drawable.wh_5,
				R.drawable.wh_6,
				R.drawable.wh_7,
				R.drawable.wh_8,
				R.drawable.wh_9,
				R.drawable.wh_10,
				R.drawable.wh_11,
				R.drawable.wh_12
							
				};
	
	@SuppressLint({ "NewApi", "NewApi", "NewApi" })
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.weeklyhoro);
		
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.GINGERBREAD) {
            // only for android older than gingerbread
		}else{
			StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
			StrictMode.setThreadPolicy(policy);
		}
		
		imgHead = (ImageView)findViewById(R.id.img_head_weekly);
		web = (WebView)findViewById(R.id.webview_weekly);
		
		ProfileData proData = new ProfileData(this);
		List<Profile> listProfile = new ArrayList<Profile>();
		
		try {
			listProfile = proData.query(null, null);
			if(listProfile != null){
				if(listProfile.size() > 0){
					System.out.println(listProfile.get(0).getBirthdate());
					System.out.println(listProfile.get(0).getMSISDN());
					
					mMonth = Integer.parseInt(listProfile.get(0).getBirthdate().substring(5, 7));
					
					System.out.println("----------------- mMonth in DB : " + mMonth); 
					
					
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		if(mMonth == 4){
			mMonth = 1 ;
		}else if(mMonth > 4){
			mMonth -= 3;
		}else if(mMonth < 4){
			mMonth += 9;
		}
		
		System.out.println("------------- mMonth After process : " + mMonth);
		
		weeklyHoroData = new WeeklyHoroData(this);
		listContent = new ArrayList<ContentHoro>();
		
		String strDes = "";
		
		try{
			listContent =  weeklyHoroData.query(null, null);
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		if(listContent != null){
			if(listContent.size() > 0){
				
				
				//Log.log("listContent.size > 0 ");
				//Log.log("getDesc : " + listContent.get(mMonth).getDesc().toString());
				
				for(int i = 0; i < listContent.size(); i++){
					if(listContent.get(i).getID().equals(mMonth+"")){ 
						strDes = listContent.get(i).getDesc().toString();
					}
				}
				
				
				summary = "<html><head><meta http-equiv=Content-type content=text/html; charset=utf-8 />" +
				"<meta name = viewport content = width=device-width></head><body bgcolor='#e5e5e5'>"+
				strDes+"</body></html>";
			}else{
				summary = "<html><body></body></html>";
			}
			
		}else{
			summary = "<html><body></body></html>";
		}
		
		
		
		imgHead.setImageResource(image[mMonth-1]);
		//////web.loadData(summary, "text/html", null);
		
		web.loadData(summary, "text/html; charset=UTF-8", null);
		
		web.getSettings().setBuiltInZoomControls(false);
	}
	
	
}
