package com.gmm.HoroApp;


import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;
import android.widget.ImageView;

public class WeeklyHoroDetailActivity extends Activity{// implements OnTouchListener{
	
	private int header[] = {
			R.drawable.wh_1,
			R.drawable.wh_2,
			R.drawable.wh_3,
			R.drawable.wh_4,
			R.drawable.wh_5,
			R.drawable.wh_6,
			R.drawable.wh_7,
			R.drawable.wh_8,
			R.drawable.wh_9,
			R.drawable.wh_10,
			R.drawable.wh_11,
			R.drawable.wh_12
			
	};
	
	private ImageView imgHead;
	private WebView webview;
	
	private WeeklyHoroData weeklyHoroData;
	private List<ContentHoro> listContent;
	
	private int Rasri;
	private String summary;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		Log.log("-------- WeeklyHoroDetailActivity -----");
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.weeklyhorodetail);
		
		imgHead = (ImageView)findViewById(R.id.imgheadweekly);
		webview = (WebView)findViewById(R.id.webviewweekly);
		
		weeklyHoroData = new WeeklyHoroData(this);
		listContent = new ArrayList<ContentHoro>();
		
		try{
			listContent =  weeklyHoroData.query(null, null);
		}catch (Exception e) {
			e.printStackTrace();
		}
		
        Bundle rara = getIntent().getExtras();
		if(rara != null){
			Rasri = rara.getInt("RaSri");
			Log.log("Rasri : " + Rasri);
			
			imgHead.setBackgroundResource(header[Rasri]);
			if(listContent != null){
				summary = "<html><head><meta http-equiv=Content-type content=text/html; charset=utf-8 />" +
							"<meta name = viewport content = width=device-width></head><body>"+
							listContent.get(Rasri).getDesc().toString()+"</body></html>";
			}else{
				summary = "<html><body></body></html>";
				Log.log("listContent.Desc == null");
			}

		}
		
		webview.loadData(summary, "text/html", null);
		webview.getSettings().setBuiltInZoomControls(true);
	}
	
}
