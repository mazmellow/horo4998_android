package com.gmm.HoroApp;

import java.util.ArrayList;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;


public class XMLConfigHandler extends DefaultHandler{
	
	private List<Config> configList;
    
    private Config config;
    private String tempString;
    private static final String Config_String = "Config";
    
    private static final String BannerImg_String = "BannerImg";
    private static final String HomePageImg_String = "HomePageImg";
    private static final String HowToRegisterImg_String = "HowToRegisterImg";
    private static final String HowToUseImg_String = "HowToUseImg";
    private static final String ProfileText_String = "ProfileText";
    
    public List<Config> getConfigLists() {
            return configList;
    }
    
    
/**
*  To read the XML file when calling the method  
*  Initialization  persons
*/
    
    public void startDocument() throws SAXException {
            // What this initialization list  
    	configList = new ArrayList<Config>();
    	
    }

/**
* sax  Read the text node when calling this method  
*/
    
    public void characters(char[] ch, int start, int length)
                    throws SAXException {
            
            if (config != null) {
                    String valueString = new String(ch, start, length);
                    
                    if (BannerImg_String.equals(tempString)) {
                        //  If the current node is resolved to a name would be  name In the text node element of worth to the  
                    	config.setBannerImg_URL(valueString);
                    }  
                    else if (HomePageImg_String.equals(tempString)) {
                    	config.setHomePageImg_URL(valueString);
                    } 
                    else if (HowToRegisterImg_String.equals(tempString)) {
                    	config.setHowToRegister_URL(valueString);
                    }
                    
		            else if (HowToUseImg_String.equals(tempString)) {
		            	config.setHowToUseImg_URL(valueString);
		            }
		            else if (ProfileText_String.equals(tempString)) {
		            	config.setProfileText_URL(valueString);
		            }
                    
            }
            
    }
/**
* sax  Read to the element node with this method  :
*/
    
    public void startElement(String uri, String localName, String name,
                    Attributes attributes) throws SAXException {
            //  To determine whether the element is  content
    		if (Config_String.equals(localName)) {
                    
    			config = new Config();
                 
            } 
    		
    		
            tempString = localName;

    }
/**
*  This method is the end of each label are encountered, it is not a only encountered last call at the end of  
* 
*  Read the complete experience to the end of the person '    It encapsulates the good saves to a personbean  list And empty  person Object  
* 
*/
    
    
    public void endElement(String uri, String localName, String name)
                    throws SAXException {
    	
            if(Config_String.equals(localName)&&config!=null)
            {
            	configList.add(config);
            	config = null;
            }
            
            tempString = null;
            
    }
    
    /*
    <?xml version="1.0" encoding="UTF-8"?>
	<Config>
        <BannerImg><![CDATA[ http://xxx.xxx.xxx]]></BannerImg>
        <HomePageImg><![CDATA[http://xxx.xxx.xxx]]> </HomePageImg>
        <HowToRegisterImg><![CDATA[http://xxx.xxx.xxx]]> </HowToRegisterImg>
        <HowToUseImg><![CDATA[http://xxx.xxx.xxx]]></HowToUseImg>
        <ProfileText><![CDATA[ xxxxxxxx ]]> </ProfileText>
	</Config>

     */
    


}
