package com.gmm.HoroApp;

import java.util.ArrayList;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;


public class XMLContentHandler extends DefaultHandler{
	
	private List<ContentHoro> MonthlyHoro_List;
    
    private ContentHoro monthlyHoro;
    private String tempString;
    private static final String Data_String = "Data";
    
    private static final String ID_String = "ID";
    private static final String Title_String = "Title";
    private static final String Desc_String = "Desc";
    
    private static final String ThumbImg_String = "ThumbImg";
    private static final String Img_String = "Img";
    private static final String Clip_String = "Clip";
    private static final String Date_String = "Date";
    
    private String buffer;
    
    
    public List<ContentHoro> getMonthlyHoroLists() {
            return MonthlyHoro_List;
    }
    
    
/**
*  To read the XML file when calling the method  
*  Initialization  persons
*/
    
    public void startDocument() throws SAXException {
            // What this initialization list  
    	MonthlyHoro_List = new ArrayList<ContentHoro>();
    	
    }

/**
* sax  Read the text node when calling this method  
*/
    
    public void characters(char[] ch, int start, int length)
                    throws SAXException {
    	
    	String valueString = new String(ch, start, length);
    	
    	if(buffer == null){
    		buffer = valueString;
    	}else{
    		buffer += valueString;
    	}
            
            if (monthlyHoro != null) {
            	
            	if (ID_String.equals(tempString)) {
                    //  If the current node is resolved to a name would be  name In the text node element of worth to the  
                	monthlyHoro.setID(buffer);
                }  
                else if (Title_String.equals(tempString)) {
                	monthlyHoro.setTitle(buffer);
                } 
                else if (Desc_String.equals(tempString)) {
                	buffer = buffer.trim();
                	monthlyHoro.setDesc(buffer);
                }
                
	            else if (ThumbImg_String.equals(tempString)) {
	            	monthlyHoro.setThumbImg(buffer);
	            }
	            else if (Img_String.equals(tempString)) {
	            	monthlyHoro.setImg(buffer);
	            }
	            else if (Clip_String.equals(tempString)) {
	            	monthlyHoro.setClip(buffer);
	            }
	            else if (Date_String.equals(tempString)) {
	            	monthlyHoro.setDate(buffer);
	            }
                    
                    /*if (ID_String.equals(tempString)) {
                        //  If the current node is resolved to a name would be  name In the text node element of worth to the  
                    	monthlyHoro.setID(valueString);
                    }  
                    else if (Title_String.equals(tempString)) {
                    	monthlyHoro.setTitle(valueString);
                    } 
                    else if (Desc_String.equals(tempString)) {
                    	monthlyHoro.setDesc(valueString);
                    }
                    
		            else if (ThumbImg_String.equals(tempString)) {
		            	monthlyHoro.setThumbImg(valueString);
		            }
		            else if (Img_String.equals(tempString)) {
		            	monthlyHoro.setImg(valueString);
		            }
		            else if (Clip_String.equals(tempString)) {
		            	monthlyHoro.setClip(valueString);
		            }
		            else if (Date_String.equals(tempString)) {
		            	monthlyHoro.setDate(valueString);
		            }*/
                    
            }
            
    }
/**
* sax  Read to the element node with this method  :
*/
    
    public void startElement(String uri, String localName, String name,
                    Attributes attributes) throws SAXException {
            //  To determine whether the element is  content
    		if (Data_String.equals(localName)) {
                    
    			monthlyHoro = new ContentHoro();
                 
            } 
    		
    		
            tempString = localName;

    }
/**
*  This method is the end of each label are encountered, it is not a only encountered last call at the end of  
* 
*  Read the complete experience to the end of the person '    It encapsulates the good saves to a personbean  list And empty  person Object  
* 
*/
    
    
    public void endElement(String uri, String localName, String name)
                    throws SAXException {
    	
            if(Data_String.equals(localName)&&monthlyHoro!=null)
            {
            	MonthlyHoro_List.add(monthlyHoro);
            	monthlyHoro = null;
            }
            
            tempString = null;
            buffer = null;
    }
    
    /*
    <?xml version="1.0" encoding="UTF-8"?>
	<XML>
	   <Data>
			<ID><![CDATA[ 1 ]]></ID>
			<Title><![CDATA[ xxx ]]></Title>
			<Desc><![CDATA[ xxx]]></Desc>
			<ThumbImg><![CDATA[ http://xxx.xxx.xxx/xxx.jpg ]]></ThumbImg>
			<Img><![CDATA[]]></Img>
		    <Clip><![CDATA[http://xxx.xxx.xxx/xxx.flv]]></Clip>
		    <Date><![CDATA[2011-01-07 16:57:33]]></Date>
	   </Data>
	   <Data>
			<ID><![CDATA[ 2 ]]></ID>
			<Title><![CDATA[ xxx ]]></Title>
			<Desc><![CDATA[ xxx]]></Desc>
			<ThumbImg><![CDATA[ http://xxx.xxx.xxx/xxx.jpg ]]></ThumbImg>
			<Img><![CDATA[]]></Img>
	        <Clip><![CDATA[http://xxx.xxx.xxx/xxx.flv]]></Clip>
	        <Date><![CDATA[2011-01-07 16:57:33]]></Date>
	   </Data>
	</XML>


     */
    


}
