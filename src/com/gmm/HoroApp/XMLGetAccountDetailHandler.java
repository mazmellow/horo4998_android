package com.gmm.HoroApp;

import java.util.ArrayList;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;


public class XMLGetAccountDetailHandler extends DefaultHandler{
	
	private List<StatusPost> statusList;
    
    private StatusPost status;
    private String tempString;
    private static final String xml_String = "XML";
    
    private static final String status_String = "STATUS";
    private static final String st_code_String = "STATUS_CODE";
    private static final String detail_String = "STATUS_DETAIL";
    
    private static final String ZODIAC_String = "ZODIAC";
    private static final String ZODIAC_ID_String = "ZODIAC_ID";
    private static final String BIRTHDATE_String = "BIRTHDATE";
    
    public List<StatusPost> getStatusPostLists() {
            return statusList;
    }
    
    
/**
*  To read the XML file when calling the method  
*  Initialization  persons
*/
    
    public void startDocument() throws SAXException {
            // What this initialization list  
    	statusList = new ArrayList<StatusPost>();
    	
    }

/**
* sax  Read the text node when calling this method  
*/
    
    public void characters(char[] ch, int start, int length)
                    throws SAXException {
            
            if (status != null) {
                    String valueString = new String(ch, start, length);
                    if (status_String.equals(tempString)) {
                        //  If the current node is resolved to a name would be  name In the text node element of worth to the  
                    	status.setStatus(valueString);
                    }  
                    else if (st_code_String.equals(tempString)) {
                    	status.setStatusCode(valueString);
                    } else if (detail_String.equals(tempString)) {
                    	status.setStatusDetail(valueString);
                    }
                    
		            else if (ZODIAC_String.equals(tempString)) {
		            	status.setZODIAC(valueString);
		            }
		            else if (ZODIAC_ID_String.equals(tempString)) {
		            	status.setZODIAC_ID(valueString);
		            }
                    else if (BIRTHDATE_String.equals(tempString)) {
                    	status.setBIRTHDATE(valueString);
                    }
            }
            
    }
/**
* sax  Read to the element node with this method  :
*/
    
    public void startElement(String uri, String localName, String name,
                    Attributes attributes) throws SAXException {
            //  To determine whether the element is  content
    		if (xml_String.equals(localName)) {
                    
    			status = new StatusPost();
                 
            } 
    		
    		
            tempString = localName;

    }
/**
*  This method is the end of each label are encountered, it is not a only encountered last call at the end of  
* 
*  Read the complete experience to the end of the person '    It encapsulates the good saves to a personbean  list And empty  person Object  
* 
*/
    
    
    public void endElement(String uri, String localName, String name)
                    throws SAXException {
    	
            if(xml_String.equals(localName)&&status!=null)
            {
            	statusList.add(status);
            	status = null;
            }
            
            tempString = null;
            
    }
    
    /*
    <XML>
		<STATUS><![CDATA[ ERR ]]></STATUS>
		<STATUS_CODE><![CDATA[ 801 ]]></STATUS_CODE>
		<STATUS_DETAIL><![CDATA[ Invalid application ID. ]]></STATUS_DETAIL>
		<ZODIAC><![CDATA[ ]]></ZODIAC>
		<ZODIAC_ID><![CDATA[ 0 ]]></ZODIAC_ID>
		<BIRTHDATE><![CDATA[ ]]></BIRTHDATE>
	</XML>
     */
    


}
