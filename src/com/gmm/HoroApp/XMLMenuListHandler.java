package com.gmm.HoroApp;

import java.util.ArrayList;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;


public class XMLMenuListHandler extends DefaultHandler{
	
	private List<MenuList> menuList_List;
    
    private MenuList menu;
    private String tempString;
    private static final String Menu_String = "Menu";
    
    private static final String MenuID_String = "MenuID";
    private static final String MenuTypeID_String = "MenuTypeID";
    private static final String MenuTypeCallName_String = "MenuTypeCallName";
    
    private static final String MenuTypeImg_String = "MenuTypeImg";
    private static final String MenuTypeURL_String = "MenuTypeURL";
    
    public List<MenuList> getMenuListLists() {
            return menuList_List;
    }
    
    
/**
*  To read the XML file when calling the method  
*  Initialization  persons
*/
    
    public void startDocument() throws SAXException {
            // What this initialization list  
    	menuList_List = new ArrayList<MenuList>();
    	
    }

/**
* sax  Read the text node when calling this method  
*/
    
    public void characters(char[] ch, int start, int length)
                    throws SAXException {
            
            if (menu != null) {
                    String valueString = new String(ch, start, length);
                    if (MenuID_String.equals(tempString)) {
                        //  If the current node is resolved to a name would be  name In the text node element of worth to the  
                    	menu.setMenuID(valueString);
                    }  
                    else if (MenuTypeID_String.equals(tempString)) {
                    	menu.setMenuTypeID(valueString);
                    } 
                    else if (MenuTypeCallName_String.equals(tempString)) {
                    	menu.setMenuTypeCallName(valueString);
                    }
                    
		            else if (MenuTypeImg_String.equals(tempString)) {
		            	menu.setMenuTypeImg_URL(valueString);
		            }
		            else if (MenuTypeURL_String.equals(tempString)) {
		            	menu.setMenuTypeUrl(valueString);
		            }
                    
            }
            
    }
/**
* sax  Read to the element node with this method  :
*/
    
    public void startElement(String uri, String localName, String name,
                    Attributes attributes) throws SAXException {
            //  To determine whether the element is  content
    		if (Menu_String.equals(localName)) {
                    
    			menu = new MenuList();
                 
            } 
    		
    		
            tempString = localName;

    }
/**
*  This method is the end of each label are encountered, it is not a only encountered last call at the end of  
* 
*  Read the complete experience to the end of the person '    It encapsulates the good saves to a personbean  list And empty  person Object  
* 
*/
    
    
    public void endElement(String uri, String localName, String name)
                    throws SAXException {
    	
            if(Menu_String.equals(localName)&&menu!=null)
            {
            	menuList_List.add(menu);
            	menu = null;
            }
            
            tempString = null;
            
    }
    
    /*
    <?xml version="1.0" encoding="UTF-8"?>
	<MenuList ALLITEM="2">
	   <Menu>
	        <MenuID><![CDATA[1]]></MenuID>
	        <MenuTypeID><![CDATA[1]]></MenuTypeID>
	        <MenuTypeCallName><![CDATA[xxx]]></MenuTypeCallName>
	        <MenuTypeImg><![CDATA[http://xxx.xxx.xxx]]></MenuTypeImg>
	        <MenuTypeURL><![CDATA[xxx]]></MenuTypeURL>
	   </Menu>
	   <Menu>
	        <MenuID><![CDATA[2]]></MenuID>
	        <MenuTypeID><![CDATA[1]]></MenuTypeID>
	        <MenuTypeCallName><![CDATA[xxx]]></MenuTypeCallName>
	        <MenuTypeImg><![CDATA[http://xxx.xxx.xxx]]></MenuTypeImg>
	        <MenuTypeURL><![CDATA[xxx]]></MenuTypeURL>
	   </Menu>
	</MenuList>

     */
    


}
