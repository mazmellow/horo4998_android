package com.gmm.HoroApp;

import java.util.ArrayList;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;


public class XMLVersionHandler extends DefaultHandler{
	
	private List<Version> Version_List;
    
    private Version version;
    private String tempString;
    private static final String Version_String = "Version";
    
    private static final String ConfigAPI_String = "ConfigAPI";
    private static final String MenuListAPI_String = "MenuListAPI";
    private static final String MenuPlusListAPI_String = "MenuPlusListAPI";
    
    private static final String Content_1_String = "Content_1";
    private static final String Content_2_String = "Content_2";
    private static final String Content_3_String = "Content_3";
    private static final String Content_4_String = "Content_4";
    private static final String Content_5_String = "Content_5";
    
    public List<Version> getVersionLists() {
            return Version_List;
    }
    
    
/**
*  To read the XML file when calling the method  
*  Initialization  persons
*/
    
    public void startDocument() throws SAXException {
            // What this initialization list  
    	Version_List = new ArrayList<Version>();
    	
    }

/**
* sax  Read the text node when calling this method  
*/
    
    public void characters(char[] ch, int start, int length)
                    throws SAXException {
            
            if (version != null) {
                    String valueString = new String(ch, start, length);
                    if (ConfigAPI_String.equals(tempString)) {
                        //  If the current node is resolved to a name would be  name In the text node element of worth to the  
                    	version.setConfigAPI(valueString);
                    }  
                    else if (MenuListAPI_String.equals(tempString)) {
                    	version.setMenuListAPI(valueString);
                    } 
                    else if (MenuPlusListAPI_String.equals(tempString)) {
                    	version.setMenuPlusListAPI(valueString);
                    }
                    
		            else if (Content_1_String.equals(tempString)) {
		            	version.setMonthlyHoro(valueString);
		            }
		            else if (Content_2_String.equals(tempString)) {
		            	version.setVideo(valueString);
		            }
		            else if (Content_3_String.equals(tempString)) {
		            	version.setLuckyNumber(valueString);
		            }
		            else if (Content_4_String.equals(tempString)) {
		            	version.setWeeklyHoro(valueString);
		            }
		            else if (Content_5_String.equals(tempString)) {
		            	version.setSpecialVideo(valueString);
		            }
                    
            }
            
    }
/**
* sax  Read to the element node with this method  :
*/
    
    public void startElement(String uri, String localName, String name,
                    Attributes attributes) throws SAXException {
            //  To determine whether the element is  content
    		if (Version_String.equals(localName)) {
                    
    			version = new Version();
                 
            } 
    		
    		
            tempString = localName;

    }
/**
*  This method is the end of each label are encountered, it is not a only encountered last call at the end of  
* 
*  Read the complete experience to the end of the person '    It encapsulates the good saves to a personbean  list And empty  person Object  
* 
*/
    
    
    public void endElement(String uri, String localName, String name)
                    throws SAXException {
    	
            if(Version_String.equals(localName)&&version!=null)
            {
            	Version_List.add(version);
            	version = null;
            }
            
            tempString = null;
            
    }
    
    /*
    <?xml version="1.0" encoding="UTF-8"?>
	<Version>
        <ConfigAPI><![CDATA[ 201201124578421]]></ConfigAPI>
        <MenuListAPI><![CDATA[ 201201124578421]]></MenuListAPI>
        <MenuPlusListAPI><![CDATA[ 201201124578421]]></MenuPlusListAPI>
        <Content_1><![CDATA[ 201201124578421]]></Content_1>
        <Content_2><![CDATA[ 201201124578421]]></Content_2>
        <Content_3><![CDATA[ 201201124578421]]></Content_3>
        <Content_4><![CDATA[ 201201124578421]]></Content_4>
        <Content_5><![CDATA[ 201201124578421]]></Content_5>
	</Version>
     */
    


}
